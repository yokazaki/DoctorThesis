\section{Detailed Setup of SM Background MC Simulation in the  SUSY Search}
\label{app:SM_sample}

In this section, the setup of SM background samples is introduced.
The generator setup is summarized in Table~\ref{tab:dataMC:bkg_process}.

\input{DataAndSimulations/tables/MCConfigureation}

\paragraph*{\Vjets}
\Wjets and \Zjets events with leptonically-decaying bosons are simulated using \SHERPAV{2.2.1}~\cite{sherpa}.
In this setup, matrix elements for up to two partons are calculated using next-to-leading-order (NLO) accuracy, and for up to four partons are calculated using leading-order (LO) accuracy by Comix~\cite{Gleisberg:2008fv} and \textsc{OpenLoops}~\cite{Buccioni_2019,Cascioli:2011va,Denner:2016kdg} libraries.
The default \SHERPA parton shower model~\cite{Schumann:2007mg}, which is based on Catani–Seymour dipole factorization and the cluster hadronization model~\cite{Winter:2003tt}, is employed.
The NNPDF$3.0_{\textrm{NNLO}}$ PDF set~\cite{Ball:2014uwa}, which parameters are tuned by the \SHERPA, is used.
Their cross-sections are normalized to an NLO prediction~\cite{Anastasiou:2003ds}.
Additionally, prompt single photon samples ($\gammajets$) are simulated by \SHERPAV{2.2.2}.
Photons are required to satisfy a smooth-cone isolation criterion~\cite{Frixione:1998jh}.

\paragraph*{Tops}
\ttbar and $t+W$ event generations are simulated by \POWHEGBOX~\cite{Frixione:2007nw,JHEP0411.2004.040,Frixione:2007vw,Alioli:2010xd} v2 at NLO with the NNPDF$3.0_{\textrm{NNLO}}$ PDF set.
The interference for the parton shower and hadronization is simulated by \PYTHIAV{8.230}~\cite{Comp.Phys.Comm.191.159} using A14 set of tuned parameters~\cite{ATL-PHYS-PUB-2014-021} and the NNPDF$2.3_{\textrm{LO}}$ PDF set~\cite{Ball:2012cx}.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.6.0~\cite{Lange:2001uf}.
In this simulation, we use the $h_{\textrm{damp}}$ parameter to control the matching matrix elements to the parton shower in \POWHEG and regulate the high-\pt radiation against which the \ttbar system recoils~\cite{ATL-PHYS-PUB-2016-020}, and $h_{\textrm{damp}}$ is set to be 1.5 times top mass.
The \ttbar sample is normalized to the cross-section prediction of \textsc{Top++} 2.0~\cite{Beneke:2011mq,Cacciari:2011hy,Baernreuther:2012ws,Czakon:2012zr,Czakon:2012pz,Czakon:2013goa,Czakon:2011xx} using the calculation at NLO in QCD, including the resummation of next-to-next-to-leading logarithmic (NNLL) soft-gluon terms.
Similarly, the cross-section of $t+W$ sample is normalized to the theoretical prediction using the calculation at NLO in QCD with NNLL soft-gluon corrections~\cite{Kidonakis:2010ux,Kidonakis:2013zqa}.
However, $t$-channel $t+W$ samples are normalized to the theoretical prediction of the calculation at NLO in QCD with \textsc{Hathor} 2.1~\cite{Aliev:2010zk,Kant:2014oha}.
Additionally, we employ the diagram removal scheme~\cite{Frixione:2008yi} to consider the interference between \ttbar and $t+W$ process.

$tWZ$ and $tZ$ events are simulated by \MADGRAPHV{5}\_\AMCatNLOV{2.3.3}~\cite{Alwall:2014hca} with the NNPDF$3.0_{\textrm{NNLO}}$ PDF set.
The interference is simulated by \PYTHIAV{8.212}~\cite{Comp.Phys.Comm.191.159} using A14 set of tuned parameters and the NNPDF$2.3_{\textrm{LO}}$ PDF set.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.2.0.

$\ttbar+h$ event generations are simulated with \POWHEGBOX v2.2 and and the NNPDF$3.0_{\textrm{NNLO}}$ PDF set.
The cross-section is calculated at NLO QCD and NLO EW accuracy by \MADGRAPHV{5}\_\AMCatNLO~\cite{yellowReport4}.
3/4 tops, $\ttbar+WW$ events are simulated by \MADGRAPHV{5}\_\AMCatNLOV{2.2.2}~\cite{Alwall:2014hca} with \PYTHIAV{8.186} A14 set of tuned parameters and the NNPDF$2.3_{\textrm{LO}}$ PDF set.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.2.0.
$\ttbar+V$ production events are simulated by \MADGRAPHV{5}\_\AMCatNLOV{2.3.3} with the NNPDF$3.0_{\textrm{NNLO}}$ PDF set.
The interference is simulated by \PYTHIAV{8.210} using A14 set of tuned parameters and the NNPDF$2.3_{\textrm{LO}}$ PDF set.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.2.0.
The cross-sections are calculated at NLO QCD and NLO EW accuracy by \MADGRAPHV{5}\_\AMCatNLO~\cite{yellowReport4}.
Other top backgrounds, such as $t\bar{t}t$, $tt\bar{t}\bar{t}$, $\ttbar+Wll$, $\ttbar+\gamma$, and $\ttbar+VV$, are simulated by \MADGRAPHV{5}\_\AMCatNLOV{2.2.3-2.6.7}.
The interferences are simulated by \PYTHIAV{8.186-8.240} using A14 set of tuned parameters and the NNPDF$2.3_{\textrm{LO}}$ PDF set.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.2.0-1.7.0.
Events with two tops and at least one boson are referred to as \ttbarX samples.

\paragraph*{Multi-bosons}
Diboson processes, such as $WW, WZ, ZZ$, are simulated by \SHERPA.
Fully leptonic decaying events are simulated with \SHERPAV{2.2.1} ($ZZ\ra\nu\nu\nu\nu$) or \SHERPAV{2.2.2} ($llll$, $lll\nu$, $ll\nu\nu$, and $l\nu\nu\nu$).
Additionally, semi-leptonic decaying events, where one boson decays leptonically and the other decays hadronically, are simulated with \SHERPAV{2.2.1}.
Besides, gluon-initiated diboson events are simulated by \SHERPAV{2.2.2}.
In this setup, off-shell effects and Higgs boson contributions are included.
Up to one additional parton is generated using matrix elements at NLO accuracy in QCD, and up to three additional parton emissions are generated at LO accuracy.
Diboson associated with additional two jets events ($VVjj$) is simulated with \SHERPAV{2.2.2}.
The NNPDF$3.0_{\textrm{NNLO}}$ PDF set, which parameters are tuned by the \SHERPA, is used.

Triboson ($WWW$, $WWZ$, $WZZ$, and $ZZZ$ denoted as $VVV$) events are simulated with \MADGRAPHV{5}\_\AMCatNLOV{2.6.6} at LO.
The interference is simulated by \PYTHIAV{8.243} with the A14 tune and the NNPDF$3.0_{\textrm{NNLO}}$ PDF set is used.
The cross-sections are normalized to the values calculated at LO from the generator.

$V\gamma$ events are simulated with \SHERPAV{2.1.1}.
The \SHERPA default PDF and tune are used.
The cross-sections are normalized to the values calculated at LO from the generator.

\paragraph*{Higgs}
Events with leptonically decaying $W/Z$ boson and hadronically decaying Higgs boson are simulated with \POWHEGBOXV{2.2} interfaced with \PYTHIAV{8.186}.
The NNPDF$3$ PDF set and AZNLO tune~\cite{Aad:2014xaa} are used.

\paragraph*{Multi-jets}
Multi-jet backgrounds are simulated with \SHERPAV{2.1.1} and the \SHERPA default PDF and tune are used.
The contribution of multi-jet backgrounds is negligible in the search for electroweakinos.

\paragraph*{Alternative samples}
\ttbar samples are used for the boson tagging efficiency measurement described in Sec.\ref{sec:bjt_sf}.
One of the alternative \ttbar samples is simulated with the same settings as a nominal \ttbar sample, except for the $h_{\textrm{damp}}$, $h_{\textrm{damp}}$ is set to be 3 times top mass.
The alternative \ttbar samples are simulated with \POWHEG interfaced with \HERWIG 7.04~\cite{EPJC.76.196,B_hr_2008} and \MADGRAPHV{5}\_\AMCatNLOV{2.6.0} interfaced with Pythia 8.230.
Additionally, $W/Z(\ra qq)$+jets samples are also used for the boson tagging efficiency measurement.
$W/Z(\ra qq)$+jets samples are simulated with \SHERPAV{2.1.1} and the \SHERPA default PDF and tune are used.
Alternative $W/Z(\ra qq)$+jets samples are simulated with \HERWIGpp v2.7.1.% with the parton shower, UE-EE-5-CTEQ6L1 tune~\cite{Pumplin:2002vw}.
Besides, multi-jet and \gammajets samples are used for the boson tagging background rejection factor measurements.
Alternative multi-jet samples are simulated with \PYTHIAV{8.186} using A14 set of tuned parameters and the NNPDF$2.3_{\textrm{LO}}$ PDF set.
The bottom and charm hadron decays are simulated by the \textsc{EvtGen} 1.2.0.
Besides, alternative \gammajets samples are simulated with \PYTHIAV{8.186} using the NNPDF$2.3_{\textrm{LO}}$ PDF set and \SHERPAV{2.1} using the CT10 PDF set~\cite{Lai:2010vv}.

\section{Detailed Setup of SUSY Signal MC Simulation}
\label{app:Signal_sample}

All of the signal event generations are simulated with the LO matrix elements with up to two extra partons using \MADGRAPH v2.6.2~\cite{Alwall:2014hca}.
For simulating of parton showering and hadronization, \PYTHIAV{8.230} using A14 set of tuned parameters, and the NNPDF$2.3_{\textrm{LO}}$ PDF set are used as the interface.
The bottom and charm hadron decays are simulated by \textsc{EvtGen} 1.2.0.
In the simulation setup, SM-like Higgs boson mass is set to $125\GeV$.
The signal cross-sections are calculated at NLO in the strong coupling constant.
Additionally, the resummation of soft gluon emission is taken into account at NLL accuracy.
In this calculation, we use the PDF4LHC15\_mc PDF set~\cite{PDF4LHC}.

In the \winoBinoSim samples, it is assumed that $m(\Cone) = m(\Ntwo)$.
In the \hinoBinoSim samples, it is assumed that $m(\Cone/\Ntwo) = m(\None)+1\GeV$.
In the \hinoGravitino samples, $m(\gravitino)$ is asuumed to be 0.5\GeV and it is assumed that $m(\Cone/\Ntwo) = m(\None)+1\GeV$.

In this analysis, our target signatures have the mass difference $(m(\heavyino)-m(\lightino)) > 400\GeV$, then $W/Z$ bosons in the models are typically fully longitudinally polarized.
We use the boson tagging technique~\cite{ATL-PHYS-PUB-2020-017} to identify jets originating from hadronically decaying bosons.
The performance of the boson tagging is sensitive to the boson polarization~\cite{JETM-2018-03}.
Thus, the polarization of $W/Z$ bosons from the electroweakinos is modeled by \textsc{MadSpin} v2.7.3~\cite{Artoisenet:2012st,Frixione:2007zp}, and re-weighting the truth-level helicity angle distribution.
In the re-weighting method, the overall cross-section is not changed.
