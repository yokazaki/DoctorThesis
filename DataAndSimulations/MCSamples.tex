Monte Carlo simulation samples are used to estimate the yields of the signal and the SM backgrounds.
The first phase of the event simulation is the hard scattering process.
In the $pp$-collisions, multiple proton-proton collisions are occurring in the same and neighboring bunch crossings; other particles from collisions other than a collision of interest contribute to the mis-reconstruction and mis-measurement.
These contributions are called ``pile-up effect.''
To simulate this pile-up effect, we use the samples simulated with \PYTHIAV{8.186}~\cite{Comp.Phys.Comm.191.159}, the NNPDF$2.3_{\textrm{LO}}$ PDF set~\cite{Ball:2012cx}, and A3 set of tuned parameters~\cite{ATL-PHYS-PUB-2016-017} to overlay to other hard scatter simulation samples.
%These samples are overlaid.
The response of the ATLAS detector is simulated with the ATLAS simulation software~\cite{SOFT-2010-01} based on the \GEANT 4~\cite{GEANT4}.

\clearpage

\subsection{Hard-scatter Event Generation}
\label{sec:datamc:hard}

In the Monte Carlo event generations, the hadron interactions are described with sub-divided calculation, such as the hard short-distance physics, the nonperturbative transition from partons (``hadronization''), and hadron decays.
The differential cross-section with an initial state ($a,~b$) and a final state ($F$) in the hard-scattering process for an observable $\mathcal{O}$ is written by Ref.\cite{skands2012qcd},
\begin{equation}
\frac{d\sigma}{d\mathcal{O}} = \sum_{a,b}\int^{1}_{0}dx_{a}dx_{b}\sum_{F}\int d\Phi_{F}f_{a}^{h1}(x_{a},\mu_{F})f_{b}^{h2}(x_{b},\mu_{F})\frac{d\hat{\sigma}_{ab\ra F}}{d\hat{\mathcal{O}}}D_{F}(\hat{\mathcal{O}}\ra \mathcal{O},\mu_{F}) \label{eq:dataMC:factorize},
\end{equation}
where $h_{1,2}$ represent the colliding hadrons in the initial state $a,b$, respectively, and $d\Phi_{F}$ is  the standard final-state phase-space differential.
$f_{a,b}$ is the parton distribution function (PDF), $d\hat{\sigma}$ is the perturbatively calculable short-distance cross-section and $D_{F}$ is a fragmentation function (FF).
%As described in Eq.\ref{eq:dataMC:factorize}, the complicated calculation is divided into independent calculations approximately.
%The QCD interaction calculation in high-energy can be applied to the perturbative quantum field theory since the hadron-scattering is factorized.
%Additionally, the PDF and hadronization terms ($f_{a,b}$ and $D_{F}$) can be constrained from the measurement of experiments.
$\mu_{F}$ is an arbitrary energy scale to separate between long and short distance physics, i.e. emissions with energy scales above $\mu_{F}$ are included in the short-distance calculation (term relating $d\hat{\sigma}$) and emissions with energy scales below $\mu_{F}$ are included in the long-distance calculation (terms relating $f_{a,b}$ and $D_{F}$).
Therefore, $\mu_{F}$ is referred to as a ``factorization scale.''

The PDF ($f_{i}^{h}(x_{i}, \mu_{F})$) represents the probability where a parton $i$ carry a fraction $x_{i}$ of the parton's momentum at a hard interaction of the energy scale $\mu_{F}$.
%Since PDFs are non-perturbative and have difficulty to be calculated using lattice QCD, they are extracted from experimental results.
The PDFs are obtained from the calculations using the results of collider physics experiments, deep-inelastic scattering measurements, and fixed-target Drell-Yan experiments as inputs.
Several groups provide the sets of PDFs with different calculations and inputs.
To estimate the uncertainty of PDF, we consider that it relates to the choice of PDF sets, as described in Section~\ref{sec:sys:theoretical}.


The partonic scattering cross-section $d\hat{\sigma}_{ab\ra F}$ is calculable in perturbative QCD with fixed-order and written as,
\begin{equation}
d\hat{\sigma}_{ab\ra F} = \frac{1}{2\hat{s}_{ab}}|\mathcal{M}_{ab\ra F}|^{2}(\Phi;\mu_{F},\mu_{R}),
\end{equation}
where $|\mathcal{M}|^{2}$ represents the matrix element squared for the process and $\frac{1}{2\hat{s}_{ab}}$ is the parton flux factor.
The matrix element (ME) is the sum of the transition amplitude with an initial state and a final state, regardless of intermediate states.
%$\mu_{F}$ and $\mu_{R}$ are unphysical parameters to lead the matrix element calculation to be simple.
$\mu_{R}$ is referred to as a ``renormalization scale'' and an unphysical term to get rid of UV divergence.
The parton shower approximation, which corrects higher-order real-emission in the hard scattering, is applied.
The fragmentation function is a universal function that describes the hadronic observable in the final state at the partonic level.
They are used in the non-perturbative calculation and parameterized with the $\mu_{F}$ in the event generation.

%reference
%https://arxiv.org/pdf/1706.00428.pdf
%https://arxiv.org/pdf/0810.2281.pdf
%https://physics.stackexchange.com/questions/365124/what-is-meant-by-factorization-scale-factor-in-qcd-calculations
%https://indico.cern.ch/event/680421/contributions/3096162/attachments/1697092/2731944/J.Huston_Introduction_to_QCD_from_an_LHC_perspective-02.pdf
%https://pdg.lbl.gov/2012/reviews/rpp2012-rev-qcd.pdf
%https://arxiv.org/pdf/1104.2863.pdf
%https://cds.cern.ch/record/1461470/files/arXiv:1207.2389.pdf
%https://cds.cern.ch/record/1600005/files/thesis.pdf
%https://indico.cern.ch/event/507575/contributions/2164831/attachments/1276861/1894847/IntroPqcd.pdf
%https://arxiv.org/pdf/1411.4085.pdf

\subsection{SM Background Simulation}

As introduced in Chapter~\ref{ch:Introduction}, two particular experimental signatures in this thesis, large missing transverse energy (\met) and large-radius jets, are used.
The background sources are vector bosons associated with jets, for example,  \Vjets, Tops, Multi-bosons production, and Higgs production.
Since large \met is required, the contribution of multi-jet events is negligible in this analysis.
More detail is described in Appendix~\ref{sec:app:multijet}.


%In this section, configurations of simulation samples are introduced.
%Thus, there are jets from boson decaying hadronically and large missing transverse energy (\met) from the LSPs, as shown in Figure~\ref{fig:intro:diagram_fullhad}.
%We select the events with jets and large \met, without leptons, and the selected kinematic regions are called ``0-lepton'' region.
%The dominant SM background in the 0-lepton region is $Z\ra\nu\nu +$jets.
%To estimate the SM backgrounds, we use the control samples normalized in selected regions with high background purity and the extrapolation to the analysis regions, described in Chapter~\ref{ch:Backgroundestimation}.
%To validate the background estimation, we use the events, where the number of leptons is one or the number of photons is one.
%These events are called ``1-lepton'' and ``1-photon'' region, respectively.
%%Firstly, the SM background samples used for the analysis are introduced.
%%After that, SUSY signal simulation samples are introduced in Sec.\ref{sec:dataMC:signal}.

The detail (including the generator setup) of the SM background samples is described in Appendix~\ref{app:SM_sample}.

\paragraph*{\Vjets}
\Znunu process can have large \met and jets from initial state radiation or final state radiation.
$W\ra e\nu~(\mu\nu) +$jets processes can be backgrounds when $e$ or $\mu$ is not reconstructed.
In $W\ra\tau\nu +$jets process, leptonically decaying $\tau$ process is similar to $W\ra e\nu (\mu\nu) +$jets processes.
Hadronically decaying $\tau$ process remains since the hadronically decaying $\tau$ is reconstructed as a jet.
\gammajets process is also considered because \gammajets events can be backgrounds when \met is mis-measured.

\paragraph*{Tops}
Leptonically decaying top quarks ($t\ra bl\nu$) remain if lepton is not reconstructed, as same as \Wjets.
\ttbar, $t+W/Z$, $tWZ$, and $\ttbar+X~(X=t,~\ttbar,~WW\cdots)$ samples are considered.

\paragraph*{Multi-bosons}
Diboson processes denoted as $VV~(V=W,~Z)$, such as $WW, WZ, ZZ$, and triboson processes denoted as $VVV$, such as $WWW$, $WWZ$, $WZZ$, and $ZZZ$, are considered.
Since one of the bosons decays leptonically, these processes remain.
% in the 0-lepton and 1-lepton regions.
$V\gamma$ process is also considered.
%$V\gamma$ samples mainly remain in the 1-photon region.

\paragraph*{Higgs}
Due to the large branching ratio of $h\ra b\bar{b}$, $Vh$ samples with leptonic decaying $W/Z$ can remain.

%\paragraph*{Multi-jets}


\subsection{Signal Simulation Samples}
\label{sec:dataMC:signal}

As introduced in Section~\ref{sec:intro:target_signals}, the \winoBino, \hinoBino, \winoHino, \hinoWino, \hinoGravitino, and \hinoAxino models are considered.
However, it is difficult to create signal samples with all production, decay processes, and SUSY parameters to scan.
Thus, benchmark signal samples are created and interpreted using normalized them, as summarized in Table~\ref{tab:dataMC:sig_process_intp}.
%Simulations of benchmark signal generation are introduced in Section~\ref{sec:dataMC:signal_gen}.
%Additionally, the method of interpretations for general signal models is introduced in Section~\ref{sec:dataMC:interpretation}.

\input{DataAndSimulations/tables/SignalProcess}

\subsubsection{SUSY signal generation}
\label{sec:dataMC:signal_gen}

Seven benchmark signal samples, \SMWW, \SMWZ, \SMWh, \SMZZ, \SMZh, \SMhh, and the \hinoGravitino model with $\mathcal{B}(\None\ra Z\gravitino) = 50\%$, are created.
\SMWW, \SMWZ, and \SMWh are assumed to be the \winoBinoSim.
\SMZZ, \SMZh, and \SMhh are assumed to be \hino pair production and only one decay process with $ZZ$, $Zh$, $hh$, respectively.
These samples are called the ``\hinoBino-SIM.''
Since \gravitino has spin-3/2, unlike \bino/\wino/\hino, the \hinoGravitino samples are generated separately.

The setup of MC samples is summarized in Appendix~\ref{app:Signal_sample}.
For the chargino-neutralino production mode, the cross-section of \wino (\hino) is 4.76 (1.12) fb, assuming their masses are $800\GeV$.
 
\subsubsection{SUSY Signal Interpretation}
\label{sec:dataMC:interpretation}

%As introduced in Section~\ref{sec:dataMC:signal_gen}, only seven simplified signal simulation samples are generated.
%However, all production and decay modes of not simplified models are covered by the samples.
For the simulations of general models, re-weighted samples of simplified models by the theoretical prediction values of the production cross-sections of \heavyino and the branching ratios are used.
It is confirmed that the event kinematics do not depend on \heavyino and \lightino in the phase space of this analysis using the generator-level samples.

\paragraph*{\winoBinoSim}
In the \winoBinoSim model, the branching ratios are assumed to be 100\%, and only one production mode is considered.
Thus, the benchmark signal simulation samples are used directly, without re-weighting.


\paragraph*{\winoBino}
Considering the chargino pair production mode, \SMWW samples are used directly since only one decay process, $\Cone\Conemp\ra WW\None\None$, is allowed in this setup.
On the other hand, in the chargino-neutralino production case, two decay processes via $WZ$ or $Wh$ boson pairs can be taken into account.
Since the branching ratios of two decay processes are variable, the re-weighted samples of \SMWZ and \SMWh to correspond to the assumed branching ratio with the constraint, $\mathcal{B}(\Ntwo\ra Z\None) = 1-\mathcal{B}(\Ntwo\ra h\None)$, are used.

\paragraph*{\hinoBino}
Four production modes (\Cone\Conemp, \Cone\Ntwo, \Cone\Nthr, \Ntwo\Nthr) and corresponding decay process, i.e., six final states, as summarized in Table~\ref{tab:dataMC:sig_process_intp}, are considered.
Re-weighting \SMWW, \SMWZ, and \SMWh samples to the production cross-section of higgsino are used as \Cone\Conemp, \Cone\Ntwo, and \Cone\Nthr production samples.
\SMZZ, \SMZh, and \SMhh samples are used without re-reweighting.
However, the branching ratios are variable.
In this analysis, it is assumed that $\mathcal{B}(\Ntwo\ra Z\None) = 1-\mathcal{B}(\Ntwo\ra h\None) = 1-\mathcal{B}(\Nthr\ra Z\None) = \mathcal{B}(\Nthr\ra h\None)$, and these re-weighted samples corresponding to the branching ratio are used.
%In summary,
%\begin{align}
%\text{\SMWW:} \quad\quad  & \text{re-weighted to the production cross-section,} \nonumber \\
%\text{\SMWZ, \SMWh:} \quad\quad  & \text{re-weighted to the production cross-section and the branching fraction,} \nonumber \\
%\text{\SMZZ, \SMZh, \SMhh:} \quad\quad  & \text{re-weighted to the branching fraction.} \nonumber
%\end{align}

\paragraph*{\winoHino and \hinoWino}
%In \winoHino or \hinoWino cases, the production and decay modes summarized in Table~\ref{tab:dataMC:sig_process} are considered.
Like the \hinoBino model, signal samples of the simplified models are re-weighted to proper cross-sections.
However, the branching ratio of \heavyino depends on MSSM parameters, such as $M_{2},~\mu,~\tan{\beta}$.
Thus, the ($M_{2},~\mu,~\tan{\beta}$) is scanned over and is used the re-weighted samples corresponding to the branching ratios.
The branching ratios of \winoHino and \hinoWino with different ($M_{2},~\mu,~\tan{\beta}$) are shown in Appendix~\ref{sec:app:branch}.

\paragraph*{\hinoGravitino}
In the \hinoGravitino model, the higgsino-gravitino coupling is assumed to be small, and only two decay modes, $\None \ra Z/h+\gravitino$, are considered.
Since $\mathcal{B}(\None \ra Z+\gravitino)$ is variable, re-weighted \hinoGravitino simulation samples corresponding to the assumed branching ratio are used.

\paragraph*{\hinoAxino}
Like the \hinoGravitino model, the higgsino-axino coupling is assumed to be small.
Therefore, \Cone and \Ntwo decay into \None, and then \None decays into $\tilde{a}$ with $Z/h$ bosons.
\hinoBinoSim simulation samples are re-weighted to the total cross-section ( = the sum of \Cone\Conemp, \Cone\None, \Cone\Ntwo, \None\Ntwo) and the assumed branching ratio of $\mathcal{B}(\None \ra Z+\tilde{a})$.
