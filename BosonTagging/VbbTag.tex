%We define a new tagging technique to identify large-$R$ jets as $Z/h\ra bb$ candidates.
For $Z/h\ra bb$ tagging, two $b$-tagged track jets inside a large-$R$ jet ($\Delta R(J, b\textrm{-jet}) < 1.0$) and the large-$R$ jet mass to be consistent with the mass of $Z/h$ are required correspondingly~\cite{ATL-PHYS-PUB-2017-010}.
The mass resolution of 2$b$-tagged large-$R$ jets ($J_{bb}$) is relatively poor compared to the two light flavor jets ($V\ra qq$) due to muons generating from semi-leptonic decaying $b/c$ hadrons.
Muons pass through the calorimeters giving only a few $\GeV$ and taking away the rest of the energy from the reconstructed $V\ra bb$ candidates.
To correct jet mass and improve the resolution, the four-momentum of a spatially matched muon ($\Delta R(J, \mu) < 0.8$) is added, and the jet mass is calculated by including the matched muons, denoted as $m(J_{bb})$.
If multiple muons pass the spatial requirement, only the highest \pT muon is used for the correction.
The corrected mass distribution has a narrow peak near the Higgs boson mass, as shown in Figure~\ref{fig:bjt:nom_cor_mbb}.
%The $\sigma$ value of the gaussian distribution is improved by 2.5\% from 13.9 to 13.6, and the mean value is changed from 117~\GeV to 118~GeV.
2.5\% improves the $\sigma$ value of the Gaussian distribution from 13.9 to 13.6, and the mean value is changed from 117~\GeV to 118~GeV.
As shown in Figure~\ref{fig:bjt:mbb_process}, the $m(J_{bb})$ distribution originating from $Z\ra bb$ has a peak near $Z$ boson mass.
A broad peak is seen in \ttbar MC samples because one of $t\ra qqb$ decay products is outside the large-$R$ jet.
To summarize, the requirements for the large-$R$ jet mass are:
\begin{itemize}
\item $m_J \in [70,100]~\GeV$~for $Z \ra bb$ tagging,
\item $m_J \in [100,135]~\GeV$~for $h \ra bb$ tagging.
\end{itemize}

%%%%%%%%%%%%% mbb distribution %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[Comparison of large-$R$ jet mass distribution \label{fig:bjt:nom_cor_mbb}]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/Jbb_correct.pdf}}
  \subfigure[$m(J_{bb})$ distribution \label{fig:bjt:mbb_process}]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/mbb_distribution.pdf}}
  \caption[$m(J_{bb})$ distribution of bosons and top quarks]{(a) Large-$R$ jet mass distribution containg 2$b$-tagged track jets in $\chinoonepm\ninotwo (m(\Cone/\Ntwo) = 900\GeV, m(\None) = 0\GeV) \ra Wh\ninoone\ninoone \ra qqbb\ninoone\ninoone$ events.
The blue line represents the default mass distribution, and the red line represents the corrected distribution by a muon.
(b) Distribution of $m(J_{bb})$ in signal and \ttbar (green) events.
Signal samples are $\chinoonepm\ninotwo (m(\Cone/\Ntwo) = 900\GeV, m(\None) = 0\GeV)$ with $Z\ra bb$ (blue) and $h\ra bb$ (red).
}
  \label{fig:bjt:mbb_distribution}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The performance of $Z/h\ra bb$ tagging is shown in Figure~\ref{fig:bjt:Vbb_performance}.
At the low \pT bin, the efficiency is slightly low because the $b$ quarks are not contained in one large-$R$ jet, for example, $\frac{2m_{h}}{p_{\textrm{T}h}} \sim 1.25$ where $p_{\textrm{T}h}=200$~\GeV\footnote{$dR(b,b)$ of Higgs is about 40\% larger than one of $Z$ because Higgs mass is about 40\% larger than the $Z$ mass.}.
In the high jet \pT region, the low efficiency is caused by overlapping the two track jets originating from $b$-quarks.
The rejection factor for the large-$R$ jets with $c$-quark initiated sub-jets included is smaller than those with the included light flavor initiated sub-jets.
It is because that the inability of falsely tagged as $b$-jet (mis-$b$-tag) is large for $c$-quark initiated sub-jets.
The background estimation, as described later in Chapter~\ref{ch:Backgroundestimation}, is designed to be not sensitive to the $Z/h\ra bb$ tagging efficiency.
Hence its efficiency in the simulation is not corrected in this main analysis.

%%%%%%%%%%%%% mbb distribution %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[Efficiency]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/VbbTag_efficiency_log.pdf}}
  \subfigure[Rejection factor]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/VbbTag_bkg_rej_log.pdf}}
  %\subfigure[Efficiency]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/VbbTag_efficiency.pdf}}
  %\subfigure[Rejection factor]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/VbbTag_bkg_rej_v4.1.pdf}}
  \caption[Efficiency and background rejection of $Z/h\ra bb$ tagging]{(a) Efficiency of $h\ra bb$ (red) and $Z\ra bb$ (blue) tagging efficiency by $\chinoonepm\ninotwo$ signals~\cite{ATLAS_EW_FullHad}.
(b) Rejection factor (defined as the inverse of efficiency).
The $x$-axis represents the number of $b$- or $c$-quark initiated sub-jets.
The hashed bands represent the uncertainty, including the MC statistical uncertainty and the systematic uncertainties, such as the jet mass scale, the jet mass resolution, and the $b$-tagging uncertainties.
}
  \label{fig:bjt:Vbb_performance}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%% mbb distribution %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
 \centering
\subfigure[C1N2-WZ]{\includegraphics[width=0.75\columnwidth, height=0.35\textheight]{BosonTagging/figure/C1N2_WZ_jetmass_sys.pdf}}
\subfigure[C1N2-Wh]{\includegraphics[width=0.75\columnwidth, height=0.35\textheight]{BosonTagging/figure/C1N2_Wh_jetmass_sys.pdf}}
  \caption[$m(J_{bb})$ distributions with systematic variations.]{
$m(J_{bb})$ distributions of \SMWZ (a) and \SMWh (b) with systematic variations.
The jet mass scale (JMS), jet mass resolution (JMR), and $b$-tagging uncertainties are shown.
The details are described in Section~\ref{sec:sys:largejets}.
}
  \label{fig:bjt:mbb_distribution_sys}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The outline of the impact on systematic uncertainties is introduced below, while more detail is discussed in Section~\ref{sec:sys:largejets}.
The dominant systematic sources are the jet mass scale (JMS), jet mass resolution (JMR), and $b$-tagging uncertainties.
As shown in Figure~\ref{fig:bjt:mbb_distribution_sys}, the largest difference in the acceptance between nominal and systematic MC samples is about 10\% and derived from the smearing scheme for the truth jet mass.
The differences in the acceptance with systematic variations are assigned as systematic uncertainties for the $Z/h \ra bb$ tagging.


