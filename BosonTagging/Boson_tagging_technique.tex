%\subsubsection{Jet Substructure Variables}

As shown in Figure~\ref{fig:reco:jet_prong}, the $W/Z$ boson jets have 2-prongness, and the quark- or gluon-initiated jets have 1-prongness.
Then, an important difference in substructure within jets originating from $W/Z$ bosons and quarks/gluons is the prongness of jets.
To describe the prongness, the energy correlation ratios~\cite{Larkoski:2013eya,Larkoski:2014gra,larkoski2016analytic}, the splitting scale~\cite{WW_scatter_2002}, and the subjettiness~\cite{N_subjet_2011,N_subjet_2012} which represent the relative positions and momenta of jet constituents, the clustering history, the substructure templates of the constituents by fit, respectively, are studied~\cite{PERF-2015-03}.
In this thesis, the energy correlation ratio is used for the boson tagging.
The other variables representing the jet mass and the number of charged particles are also found to be important because the $W/Z$ boson jets have a peak in the jet mass distribution, and $W/Z$ bosons are color-singlet.

The three variables described below are used to identify jets originating from hadronic decaying bosons.
One is the combined mass of large-$R$ jets introduced in Section~\ref{sec:reco:large_tech}.
%As shown in Figure~\ref{fig:bjt:mass}, the peak of jet mass distribution originating from $W$ bosons (light gray and light blue) can be seen near the $W$ boson mass.
As shown in Figure~\ref{fig:bjt:mass}, there is a peak near the $W$ boson mass in the jet mass distribution originating from $W$ bosons (light gray and light blue).
Other jets are distributed like the tail from the low mass region.
Their labeling represents the flavor of jets requiring spatial matching between truth particles and reconstructed jets (called ``jet truth labeling '' as described in Appendix~\ref{sec:reco_large_label}).
In this chapter, the jets originating from $W/Z$ bosons and labeled as $W/Z$ are called ``signal jets.''
The other jets are called ``background jets.''

The second is $D_{2}^{\beta}$, which is a key of a jet substructure variable to separate the hadronic decays of $W/Z$ bosons from massive QCD jets.
%The QCD jets, such as quark- or gluon-initiated jets, have 1-prongness, and the jets of hadronic decaying $W/Z$ bosons have 2-prongness, as shown in Figure~\ref{fig:reco:jet_prong}.
To describe the prongness of jets, one-, two- and three-point energy correlation functions ($N$-point correlation functions denoted as ECF($N$, $\beta$)) are used.
Their definitions are,
\begin{eqnarray}
\textrm{ECF}(1, \beta) &=& \sum_{i \in J}p_{\textrm{T}i}, \\
\textrm{ECF}(2, \beta) &=& \sum_{i<j \in J}p_{\textrm{T}i}p_{\textrm{T}j}\left(\Delta R_{ij}\right)^{\beta}, \\
\textrm{ECF}(3, \beta) &=& \sum_{i<j<k \in J}p_{\textrm{T}i}p_{\textrm{T}j}p_{\textrm{T}k}\left(\Delta R_{ij}\Delta R_{ik}\Delta R_{jk}\right)^{\beta},
\end{eqnarray}
where $p_{\textrm{T}i}$ is the transverse momentum of the cluster $i$, $J$ is the large-$R$ jet, and $\Delta R_{ij}$ is the Euclidean distance between $i$ and $j$ in the rapidity-azimuth angle plane, $R_{ij}^{2} = (y_{i}-y_{j})^{2}+(\phi_{i}-\phi_{j})^{2}$.
For infrared and collinear (IRC) safety, all $\beta > 0$.
$D_{2}^{\beta}$ is defined using ECF($N$, $\beta$),
\begin{eqnarray}
D_{2}^{\beta} &=& \frac{\textrm{ECF}(3, \beta)\left(\textrm{ECF}(1, \beta)\right)^{3}}{\left(\textrm{ECF}(2, \beta)\right)^{2}}.
\end{eqnarray}
In this analysis, $\beta = 1$ is employed and $D_{2}$ represents $D_{2}^{(\beta=1)}$.
As shown in Figure~\ref{fig:bjt:D2}, jets originating from $W/Z$ bosons have a small $D_{2}$ value, while quark- or gluon-initiated jets have large $D_{2}$.

%-------------------------
\begin{figure}[t]
 \centering
  \subfigure[$\mcomb$ \label{fig:bjt:mass}]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/WSel_rljet_m_comb_fullrange.eps}}
  \subfigure[$D_{2}$ \label{fig:bjt:D2}]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/WSel_mCut_rljet_D2.eps}}
 \centering
  \subfigure[$n_{trk}$ \label{fig:bjt:ntrk}]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/WSel_mCut_rljet_Ntrk500.eps}}
  \caption[Distributions of jet substructure variables in the $W$-enhanced regions]{
Distributions of jet substructure variables in the $W$-enhanced regions (described in Section~\ref{sec:bjt:event_sel}).
Light gray and light blue represent jets labeled as $W$ bosons.
(a) The combined mass distribution of large-$R$ jets from $W$ boson has a peak near about the $W$ boson mass.
The discrepancies of $D_{2}$ (b) and \ntrk (c) are seen after a mass selection ($> 50\GeV$).
}
  \label{fig:bjt:ttbar_CR_plots}
\end{figure}
%-------------------------

The last one is the multiplicity of tracks inside the jet (\ntrk).
The tracks that ghost-associate~\cite{GhostAssociation} with an ungroomed jet and have larger \pt than $500$~MeV are used.
The distribution of \ntrk is shown in Figure~\ref{fig:bjt:ntrk}.
As shown in Figure~\ref{fig:bjt:ntrk_vs_pT}, $W/Z$ bosons are color-singlet and the particle multiplicity is nearly independent of \pT.
In contrast to $W/Z$ bosons, the multiplicity of quark- or gluon-initiated jets increases to the jet \pT.
Thus, the upper cut is effective for high \pT jets.

However, \ntrk value depends on the kinematics of charged particles alone (for example $\pi^{\pm}$), not including neutral particles (for example $\pi^{0}$).
Therefore, \ntrk is not an IRC safe variable, and the \ntrk distribution in simulation is greatly influenced by hadronization effects such as soft emissions and arbitrary collinear parton splittings.
The data/MC discrepancy in Figure~\ref{fig:bjt:ntrk} is caused by mis-modeling of low energy scale QCD dynamics.
The discrepancies of jet substructure variables can cause an efficiency difference between the data and MC.
We correct the efficiency difference separately for each flavor of jets.
How the correction is obtained is discussed in Sections~\ref{sec:bjt_sf}, and~\ref{sec:bjt:background_sf}.

%-------------------------
\begin{figure}[t]
 \centering
  \subfigure[$W$ boson jets]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/pT_Ntrk_W.pdf}}
  \subfigure[quark- or gluon-initiated jets]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/pT_Ntrk_qg.pdf}}
\caption[\ntrk dependency on the jet \pt.]
{\ntrk dependency on the jet \pt originating from $W$ bosons decayed from electroweakinos (a) and quarks/gluons in \Znunu MC samples (b).
}
  \label{fig:bjt:ntrk_vs_pT}
\end{figure}
%-------------------------


