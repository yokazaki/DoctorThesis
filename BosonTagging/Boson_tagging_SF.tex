%\subsubsection{Signal Efficiency Measurement}
%\label{sec:bjt:signal_sf}

The signal efficiency is measured using hadronic decaying $W$ bosons in semi-leptonic \ttbar events.
This measurement is independent of the SUSY analysis because the measurement region and the analysis region of the search for electroweakinos are not overlapped.
The data sample in 2015-2017, not including 2018, is used.
The difference between the data periods is small enough with respect to the statistical uncertainty of the data in this analysis\footnote{The difference in the efficiency for signal jets between 2015-2016 and 2017 is typically less than 5\% in both the data and MC samples. The ratio as data/MC is consistent within the statistical error.}.

For the signal efficiency measurement in the data, the large-$R$ jet mass distribution is used as a template, and the template in the data is fitted with the MC template to minimize the $\chi^2$.
In this fit, shape variations are not included.
The observed difference between the data and MC samples is used for correcting the boson tagging efficiency.
However, this method is limited to the jet \pt range.
%To extend the higher \pt range, an MC-based study is conducted using $W(\ra qq)+$jets samples.
To extend to a higher \pt range, the other MC-based study to measure the efficiency in the high \pt region is conducted using $W(\ra qq)+$jets samples.
It is difficult to define a $Z$ boson enriched region because no phase space includes the $Z\ra qq$ process with high purity.
Since it can be well expected that there will be no significant difference in jet substructure variables between $W$ and $Z$ bosons except for jet mass, the study to extrapolate from the W tagging efficiency correction to the Z tagging is conducted using MC samples.


\subsubsection{Event Selection for the Measurement Region}
\label{sec:bjt:event_sel}
%For the signal efficiency measurement, the hadronic decays of $W$ bosons originating from top quarks in semi-leptonic \ttbar control samples are used.
%We mainly use $W$ bosons in semi-leptonic \ttbar control samples.
%of the data and MC to set $W$ boson enriched region.
%The event selections are summarized and the topology in \ttbar control samples are shown in Figure~\ref{fig:bjt:ttbar_topology} :
%\begin{itemize}
%\item Lowest unprescaled single-muon trigger is fired (described in Section~\ref{sec:emutrigger}).
%\item Event cleaning (described in Section~\ref{sec:sel:clean_detector}).
%\item Exactly one muon which passes the ``Medium'' identification criteria, ``TightTrackOnly'' isolation criteria (introduced in Section~\ref{sec:reco:muon}), and $|d_{0}|/\sigma(d_{0})<3$, $|z_{0}\sin{\theta}| < 0.5$~mm, $\pT > 30$~\GeV.
%\item Vetoing events with electron identified by the ``Tight'' working points based on likelihood variables~\cite{PERF-2017-01} with $\pT > 25$~\GeV.
%\item $\met > 20$~\GeV~and $\met + m_{\textrm{T}}^{W} > 60$~\GeV where $m_{\textrm{T}}^{W} = \sqrt{2\pt^{l}\met(1-\cos{\Delta\phi})}$.
%\item At least one small-$R$ jet with $\pT > 25$~\GeV, $|\eta| < 2.5$ and the ``Tight'' identification criteria,
%\item At least one large-$R$ jet with $\pT > 200$~\GeV and $|\eta| < 2$
%\item At least one $b$-tagged VR (variable radius) track jet ($R_{\textrm{min}} = 0.02, R_{\textrm{max}} = 0.4$ and $\rho = 30$~\GeV) passing the MV2c10 77\% working point.
%\item $\Delta R(\text{small-}R~\text{jet},\mu) < 1.5$.
%\item $\Delta R(\text{leading large-}R~\text{jet},\mu) > 2.3$.
%\item $\Delta R(\text{leading large-}R~\text{jet},\text{small-}R~\text{jet}) > 1.5$.
%\item $\Delta R(\text{leading large-}R~\text{jet},b\text{-tagged track jets}) > 1.0$.
%\end{itemize}
%Each identification and isolation criteria are defined in Section~\ref{ch:Reconstruction}.
``$W$ boson enriched region'' is defined by requiring a single-muon trigger~\cite{TRIG-2018-01}, an event cleaning (details are described in Appendix~\ref{sec:sel:clean_detector}), one muon, electron veto, and kinematic selections.
These selections are illustarted in Figure~\ref{fig:bjt:ttbar_topology}.
In this topology, \met is due to neutrino from leptonic decaying top quark.
Additionally, at least one large-$R$ jet from hadronic decaying $W$ boson, one $b$-tagged track jet from hadronic decaying top quark, and one small-$R$ jet from leptonic decaying top quark are required.
The requirement of the distance between large-$R$ jet and $b$-tagged track jet ($\Delta R(J, b\textrm{-tagged track jet}) > 1.0$) is used to reject large-$R$ jets containing $b$ quark.
Mass, $D_{2}$ and \ntrk distributions of selected jets in the $W$ boson enriched region are shown in Figure~\ref{fig:bjt:ttbar_CR_plots}.
\ttbar events account for 75\% in the $W$ boson enriched region, and the jets labeled as $W\ra qq$ account for 28\% of \ttbar events in the MC.
With simple jet mass requirements $\in [50\GeV,~110\GeV]$, the jets labeled as $W\ra qq$ in \ttbar and single-top events account for 50\%.

%-------------------------
\begin{figure}[ht]
 \centering
  \includegraphics[width=0.95\columnwidth]{BosonTagging/figure/ttbar_topology_dt.pdf}
  \caption[Selections to define the $W$ boson enriched region in semi-leptonic \ttbar topology.]{
Selections to define the $W$ boson enriched region in semi-leptonic \ttbar topology.
$\nu$ is measured as \met.
$W$ bosons are reconstructed by combining $\mu$ and $\nu$.
The small-$R$ jet near the reconstructed $W$ boson is not required $b$-tagging because $W$ bosons are used in $tW$ events.
To identify large-$R$ jets originating from $W$ bosons, a $b$-tagged track jet is required to be outside large-$R$ jets.
}
  \label{fig:bjt:ttbar_topology}
\end{figure}
%-------------------------

\subsubsection{Efficiency measurement}
\label{sec:bjt:signal_sf_ttbar}

The signal efficiency in MC samples is calculated as :
\begin{eqnarray}
  \epsilon_{\textrm{MC}}(\pT) &=& \frac{N^{\textrm{tagged}}_{\textrm{signal}}}{N^{\textrm{tagged}}_{\textrm{signal}}+N^{\textrm{not tagged}}_{\textrm{signal}}},  \nonumber \\
\end{eqnarray}
where $N^{\textrm{tagged}}_{\textrm{signal}}$ and $N^{\textrm{not tagged}}_{\textrm{signal}}$ are the numbers of events containing ``signal jets'', which pass (``tagged'') or fail (``not tagged'') the tagger requirements, respectively.
The signal efficiency is calculated in each \pT bin of large-$R$ jets, [200, 250, 300, 350, 600]\GeV.
The efficiency in the region where \pt is greater than $600\GeV$ no longer derived using the method due to the insufficient data statistics.

The signal efficiency for the data sample is measured as :
\begin{eqnarray}
  \epsilon_{\textrm{data}}(\pT) &=& \frac{N^{\textrm{tagged}}_{\textrm{fitted signal}}}{N^{\textrm{tagged}}_{\textrm{fitted signal}}+N^{\textrm{not tagged}}_{\textrm{fitted signal}}}, \nonumber \\
\end{eqnarray}
where $N^{\textrm{tagged}}_{\textrm{fitted signal}}$ and $N^{\textrm{not tagged}}_{\textrm{fitted signal}}$ are the estimated numbers of signal jets which pass (tagged) or fail (not tagged) the tagging, respectively.
%This estimation is performed by fitting the large-$R$ jet mass distribution in the data with the MC distributions (templates).
The estimation method is described below.

The large-$R$ jet mass distributions (templates) are made separately for
\begin{itemize}
\item \ttbar signal : jets of \ttbar and single-top samples labeled as $W$,
\item background : jets of \ttbar and single-top samples not labeled as $W$ or other background samples.
\end{itemize}
These templates are fitted simultaneously to minimize $\chi^2$ in each \pt bin and the normalization factors for tagged and not tagged signals, and backgrounds.
Consequently, $N^{\textrm{tagged}}_{\textrm{fitted signal}}$ and $N^{\textrm{not tagged}}_{\textrm{fitted signal}}$ are obtained.
Figure~\ref{fig:bjt:mcomb_postfit} shows examples of the jet mass distribution before/after fitting.
Other distributions are shown in Appendix~\ref{appsec:jet_mass_pre_post}.



%%%%%%%%%%%%% pre-/post- fit results %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[\mcomb in $W$-tag pass region before fitting]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/dataMC_preFit_w_3var_50eff_cont_JSSMassCut_nominal_pt250_300_pass.pdf}}
  \subfigure[\mcomb in $W$-tag fail region before fitting]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/dataMC_preFit_w_3var_50eff_cont_JSSMassCut_nominal_pt250_300_fail.pdf}}
  \subfigure[\mcomb in $W$-tag pass region after fitting]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/dataMC_postFit_w_3var_50eff_cont_JSSMassCut_nominal_pt250_300_pass.pdf}}
  \subfigure[\mcomb in $W$-tag fail region after fitting]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/dataMC_postFit_w_3var_50eff_cont_JSSMassCut_nominal_pt250_300_fail.pdf}}
  \caption[\mcomb distribution of pre-/post-fit]{(a)(b) Pre-fit or (c)(d) post-fit distribution of \mcomb in the (250\GeV < \pT < 300\GeV) bin.
    The regions passing ((a)(c)) or failing ((b)(d)) are shown.}
  \label{fig:bjt:mcomb_postfit}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{Results of Efficiency Measurement and Systematic uncertainties}
\label{sec:bjt_result_vqq}
The results of the signal efficiency measurement are shown in Figure~\ref{fig:bjt:signal_sf_result}.
The signal efficiency of the MC sample is almost constant.
However, it turns out that the data/MC is weakly dependent on jet \pT.

%-------------------------
\begin{figure}[ht]
 \centering
  \includegraphics[width=0.6\columnwidth]{BosonTagging/figure/effComp_w_3var_50eff_cont_JSSMassCut_rljet_m_comb_pt_Total_zoom.pdf}
  \caption[Measured efficiency of \Wqq-tagging.]{
Measured efficiency of \Wqq-tagging.
$\epsilon_{\textrm{MC}}(\pT)$ (red) and $\epsilon_{\textrm{data}}(\pT)$ (black) in each \pT bin.
The values are almost constant within statistical errors.
}
  \label{fig:bjt:signal_sf_result}
\end{figure}
%-------------------------

As shown in Figure~\ref{fig:bjt:signal_sf_result}, the difference in the efficiency between the data and MC sample is large.
To correct the difference, a signal efficiency scale factor (SF) is defined as;
\begin{eqnarray}
  \textrm{SF}(\pT) &=& \frac{\epsilon_{\textrm{data}}(\pT)}{\epsilon_{\textrm{MC}}(\pT)}. \label{eq:object:effDef}
\end{eqnarray}
In this thesis, the jets failing the boson tagging are used for a background estimation discussed in Chapter~\ref{ch:Backgroundestimation}. 
To conserve the sum of the efficiency and inefficiency to 1, an inefficiency SF is defined as;
\begin{eqnarray}
  \textrm{SF}_{\textrm{ineff}}(\pT) &=& \frac{1-\epsilon_{\textrm{MC}}(\pT)\times\textrm{SF}(\pT)}{1-\epsilon_{\textrm{MC}}(\pT)}.
\label{eq:bjt:ineff}
\end{eqnarray}

The systematic uncertainties in the efficiency measurement are evaluated for the following systematic sources;
\begin{itemize}
\item \ttbar modeling: Uncertainties calculated as the difference between nominal (\POWPYTHIA 8~\cite{Frixione:2007nw,JHEP0411.2004.040,Frixione:2007vw,Alioli:2010xd,Comp.Phys.Comm.191.159}) and alternative samples. The uncertainty from the choice of parton shower algorithm is estimated by the difference between nominal and \POWHEG+\HERWIG~\cite{EPJC.76.196,B_hr_2008} samples. To estimate the matching of next-to-leading-order (NLO) matrix-elements and parton shower, nominal, and \MGMCatNLO~\cite{Alwall:2014hca} generated samples with shower modeling of \PYTHIAV{8} used. For QCD radiation uncertainty estimation, a different parameter to control the \pt of the first additional emission beyond the leading-order Feynman diagram in the parton shower and regulate the high-\pt emission against which the \ttbar system recoils~\cite{Alioli:2010xd}. 
\item Theory: Uncertainties on \ttbar, single-top and $W+$jets cross-sections.
\item Large-$R$ jet: Uncertainties on the large-$R$ jet energy scale (JES) and resolution (JER).
\item $b$-tagging: Uncertainties of the $b$-tagging efficiency measurement.
\item Other experimental sources: A relative luminosity measurement uncertainty, detector response to muons, \met, and small-$R$ jets are considered.
\end{itemize}
Each systematic uncertainty is evaluated as a difference in the SF using each template of nominal and alternative MC samples.
Large-$R$ jet uncertainties correlate with uncertainties of final results in the statistical analysis to search for electroweakinos.
The $\eta$ dependencies for large-$R$ jets are corrected by the calibration described in Section~\ref{sec:reco:largeR_calib}, and the uncertainties are included in the large-$R$ jet uncertainties.
$b$-tagging uncertainties and other experimental uncertainties do not correlate since their effect is small, and these uncertainties are assigned conservatively.
The dependency between 10 and 40 in the average number of interactions per bunch crossing is smaller than 4\% in the MC and data efficiency measurements.
The ratios of the data to MC are consistent within the statistical uncertainty.
Since the pile-up dependencies are small enough to be ignored to the statistical uncertainty of the analysis, no additional uncertainty is assigned.

The systematic uncertainties are summarized in Table~\ref{tab:bjt:signal_sf_systematics}.
The statistical uncertainty due to the limited data sample size is substantially small compared with the total uncertainty.
The total systematic uncertainty is dominated by \ttbar modeling uncertainties that arise from parton shower modeling.
As shown in Figure~\ref{fig:bjt:ttbar_comp_m}, there is a small difference in the mass distribution of jets labeled as $W$ bosons in \ttbar samples, between different MC for jets with $250\GeV\leq\pt\leq 300\GeV$ (other distributions in different \pt range are shown in Appendix~\ref{appsec:ttbarcomp}).
However, $D_{2}$ and \ntrk have a large difference among different MCs, as shown in Figure~\ref{fig:bjt:ttbar_comp_D2}, and Figure~\ref{fig:bjt:ttbar_comp_ntrk}.
These differences between MC samples lead to large \ttbar modeling uncertainty, parton shower variation sample has a large contribution.
In contrast, it has good agreement with the data distributions as shown in Figure~\ref{fig:bjt:comp_all}.
For the QCD radiation sample, the data/MC value is small.
Since the efficiency is measured, the agreement of the distribution is important, not the normalization.
Thus, the modeling uncertainty derived from the QCD radiation is small.
%Since we measure the efficiency, the small average data/MC value in the QCD radiation sample is no problem.
%To estimate systematic uncertainties conservatively, \ttbar modeling uncertainties are symmetrized.
Since jet substructure variables are not modeled well, the differences in the efficiency SF are symmetrized and assigned as uncertainties.
%Other sources of uncertainty are muons, the jet energy scale of small-$R$ jets and large-$R$ jets, \met and flavor tagging, described in Sec~\ref{sec:sys:experiment}, they are negligible.

\input{BosonTagging/table/Signal_SF_Systematics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
\subfigure[$\mcomb$ \label{fig:bjt:ttbar_comp_m}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_rljet_m_comb_Pt250_300_ttbar_w.pdf}}
\subfigure[$D_{2}$ \label{fig:bjt:ttbar_comp_D2}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_rljet_D2_Pt250_300_ttbar_w.pdf}}
\subfigure[\ntrk \label{fig:bjt:ttbar_comp_ntrk}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_rljet_Ntrk500_Pt250_300_ttbar_w.pdf}}
\caption[Distributions of jet substructure variables in \ttbar MC samples]{
Distributions of $\mcomb$ (a), $D_{2}$ (b) and \ntrk (c) in \ttbar MC samples ($250\GeV < \pt < 300\GeV$).
Nominal MC represents black line.
Alternative MCs for the variation of matrix elements (red), parton shower (blue), and QCD radiation (green) are shown.
}
\label{fig:bjt:ttbar_comp_jss}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
\subfigure[$\mcomb$ \label{fig:bjt:ttbar_comp_m_all}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_rljet_m_comb_ttbar_w_all_add.pdf}}
\subfigure[$D_{2}$ \label{fig:bjt:ttbar_comp_D2_all}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_mCut_rljet_D2_ttbar_w_all_add.pdf}}
\subfigure[\ntrk \label{fig:bjt:ttbar_comp_ntrk_all}]{\includegraphics[width=0.48\textwidth]{BosonTagging/figure/ttbar_comp_WSel_mCut_rljet_Ntrk500_ttbar_w_all_add.pdf}}
\caption[Data/MC distributions of jet substructure variables with different \ttbar MC samples]{
Distributions of $\mcomb$ (a), $D_{2}$ (b), and \ntrk (c) in data and MC samples.
The black line represents data samples.
Except for the black line, each color represents MC expected distributions with different \ttbar MC samples (other samples are common).
Each line represents nominal MC sample (violet), alternative MCs for the variation of matrix elements (red), parton shower (blue), and QCD radiation (green) are shown.
}
\label{fig:bjt:comp_all}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

In order to estimate the uncertainty on the SF derived from the choice of the \pt binning in the measurement, the uncertainty is evaluated as the difference between the original and alternative binning, [200, 275, 350, 425, 600]\GeV.
A linear function is used to fit the SF to reduce the effect of the difference at the binning boundary.
As shown in Figure~\ref{fig:bjt:SF_2_options}, the difference in SF ($\Delta$SF) between the two binnings is less than 1\% and small enough to be compared to the statistical uncertainty ($\sim 10\%$).
The smoothed signal efficiency SF estimated from the original \pT binning is applied to large-$R$ jets, and the absolute value of $\Delta$SF is assigned as the systematic uncertainty.

%%%%%%%%%%%%% pre-/post- fit results %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
 \centering
  \subfigure[SFs with original \pT binning]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/Original_pT_SF.pdf}}
  \subfigure[SFs with alternative \pT binning]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/Shift_pT_SF.pdf}}
  \subfigure[Absolute value of $\Delta$SF ]{\includegraphics[width=0.45\columnwidth, height=0.2\textheight]{BosonTagging/figure/absolute_difference_SF.pdf}}
  \caption[Smoothing the signal efficiency SF with two \pt binnings.]{The efficiency SFs are estimated in the original binning (a) and the alternative binning (b).
The error bar represents the statistical error.
The difference between the two options (c) is assigned as a systematic uncertainty.
}
  \label{fig:bjt:SF_2_options}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage


\subsubsection{High \pT Extrapolation}
\label{sec:bjt:high_pT}

As discussed in Section~\ref{sec:bjt:signal_sf_ttbar}, a \pT range where the signal efficiency SF can be used is up to $600\GeV$ due to requiring the separation between $b$-quark and $W$ boson, such as $dR(b\textrm{-jet},~J) > 1.0$.
However, large-$R$ jets with higher \pT than $600\GeV$ are used in the main analysis, for example, such high \pt jet account for 30\% in the \winoBinoSim with $m(\heavyino) = 900\GeV,~m(\lightino) = 100\GeV$.
%Thus, we introduce how to extend the \pt range for applying SF.
%an approximation and an additional uncertainty to extend the \pt range for applying SF.

If there are sufficient statistics in the data at high \pt, the SF can be defined as:
\begin{eqnarray}
\textrm{SF}(\pT) &=& R(\pT) \times\textrm{SF}(\pT^{\textrm{ref}}), \\
R(\pT) &=& \frac{\epsilon^{\textrm{Data}}(\pT)/\epsilon^{\textrm{Data}}(\pT^{\textrm{ref}})}{\epsilon^{\textrm{MC}}(\pT)/\epsilon^{\textrm{MC}}(\pT^{\textrm{ref}})}, \label{eq:bjt:RpT}
\end{eqnarray}
where $\pT^{\textrm{ref}}$ is the highest \pT ($= 600\GeV$) of the measurement in the semi-leptonic \ttbar topology.
However, the high purity region of $W$ bosons in high \pt region cannot be defined.
Therefore, an approximation where the SF value in $\pt > 600\GeV$ is the same as the one at $\pt = 600\GeV$, i.e. $R(\pT) = 1$, is introduced, and then, an additional uncertainty on this assumption for the $W\ra qq$ tagging is assigned.
This uncertainty is evaluated as the deviation of $R(\pT)$ from unity using different hadronic shower schemes and ATLAS detector geometry in the \GEANT4 simulation.
%In this study, the double ratio of $R(\pT)$ with nominal MC and alternative MC is measured.
This study measures the double ratio of $R(\pT)$ with nominal MC and alternative MC samples.
In order to evaluate the systematic uncertainty, \Wjets MC samples containing $W\ra qq$ are used, and the calculation is performed in each jet \pt bin, [600, 700, 1000, 3000]\GeV.
The double ratio is evaluated as $a\log(\pT)^{b}$ where $a$ and $b$ are free parameters.
The function parameters of systematic uncertainties are summarized in Table~\ref{tab:bjt:RpT_fit}.
The impact of each systematic variation is not large.
The total uncertainty of high \pT extrapolation is assigned as a quadruple sum of all systematic variations and approximately 5\%.
The uncertainty has a small effect on the main analysis because the statistical error is sufficiently larger than high \pt extrapolation uncertainty, described in Section~\ref{sec:sys:total_unc}.





%%%%%%%%%%%%% high pT extrapolation each var %%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[t]
% \centering
%  \subfigure[{\tiny noDiffraction}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3295.pdf}}
%  \subfigure[{\tiny QGS}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3296.pdf}}
%  \subfigure[{\tiny Re-scattering}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3297.pdf}}
%  \subfigure[{\tiny ChipsXS}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3298.pdf}}
%  \subfigure[{\tiny HP}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3299.pdf}}
%  \subfigure[{\tiny AML}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3373.pdf}}
%  \subfigure[{\tiny PLD}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3374.pdf}}
%  \subfigure[{\tiny AMS}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3375.pdf}}
%  \subfigure[{\tiny AMI}]{\includegraphics[width=0.32\columnwidth, height=0.22\textheight]{BosonTagging/figure/HighpT_extrapolation_s3376.pdf}}
%  \caption{The variation of the double ratio $\Delta R$ with respect to that in the reference \pT bin for high \pt extrapolation uncertainty.}
%  \label{fig:bjt:RpT}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{BosonTagging/table/RpT_fit}

%%%%%%%%%%%%% high pT extrapolation %%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[t]
%\begin{center}
%\includegraphics[width=0.8\hsize]{BosonTagging/figure/highpT_extrapolation.pdf}
%\caption{The total high-\pT extrapolation uncertainties for the \Wqq-tagging.}
%\label{fig:bjt:highpT_total}
%\end{center}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsubsection{$W$-to-$Z$ Extrapolation}
\label{sec:bjt:WZ_unc}

%The signal efficiency and systematic uncertainties for the $W\ra qq$ tagging can be measured in the previous schemes.
%However, it is difficult to perform such measurement in the data for $Z\ra qq$ tagging because it is difficult to define $Z$ boson enhanced sample with high purity.
%It can be well expected that there will be no significant difference in jet substructure variables between $W$ and $Z$ bosons except for jet mass.
%Therefore, we add one assumption that there is no significant difference in efficiency SF between them.
The correction for the Z tagging can not be estimated because it is difficult to define a $Z$ boson enriched region.
Then, it is assumed that there is no significant difference in the efficiency SF between the W and Z tagging.
Thus, the signal efficiency SF of the Z tagging is assumed to be equal to the W tagging.
However, the uncertainty derived from the assumption is needed.
It is called ``$W$-to-$Z$ extrapolation uncertainty.''


$W$-to-$Z$ extrapolation uncertainty is estimated in a similar way to evaluate high \pT extrapolation uncertainty.
The systematic uncertainty is defined as the double ratio with the generator and process differences between $W(\ra qq)+$jets and $Z(\ra qq)+$jets.
The double ratio definition is :
\begin{eqnarray}
  R^{\textrm{model}}(\pT) &=& \frac{\epsilon^{\HERWIGpp}_{\Zjets}(\pT)/\epsilon^{\SHERPA}_{\Zjets}(\pT)}{\epsilon^{\HERWIGpp}_{\Wjets}(\pT)/\epsilon^{\SHERPA}_{\Wjets}(\pT)}, \\
  \Delta R^{\textrm{model}}(\pT) &=& R^{\textrm{model}}(\pT)-1.
\end{eqnarray}
%As shown in Figure~\ref{fig:bjt:WZdiff}, $\Delta R^{\textrm{model}}(\pT)$ has a small dependency on \pT because the cut values for each boson tagging are optimized to reduce the dependency of performance on \pt.
%Thus, the uncertainty is assigned  as a constant value ($\sim 4.4$\%).
$\Delta R^{\textrm{model}}(\pT)$ has a small dependency on \pT because the cut values for each boson tagging are optimized to reduce the dependency of performance on \pt.
The $\Delta R^{\textrm{model}}(\pT)$ is $\sim 4.4$\%, and it is assigned as the uncertainty without the dependency on \pt.


%%%%%%%%%%%%% Z extrapolation %%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[t]
%\begin{center}
%\includegraphics[width=0.8\hsize]{BosonTagging/figure/WZdifference.pdf}
%\caption[$W$-to-$Z$ extrapolation uncertainty for the \Zqq-tagging]{The $W$-to-$Z$ extrapolation uncertainty for the \Zqq-tagging. A constant value of 4.4\% by a fit.}
%\label{fig:bjt:WZdiff}
%\end{center}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\clearpage

\subsection{Efficiency for Background Jets}
\label{sec:bjt:background_sf}

\subsubsection{Event Selection for the Measurement Regions}
Like the signal efficiency measurement, the background jet efficiency is measured in \gammajets and multi-jets samples.
The \gammajets sample is used in the low \pT region ($\pT \leq 500\GeV$) and multi-jets sample is used in the high \pT region ($500\GeV \leq \pT \leq 3000\GeV$).
To define ``\gammajets enhanced region,'' the events collected by a single-photon trigger~\cite{TRIG-2018-05} are used, and at least one photon and one large-$R$ jet are required.
For ``multi-jets enhanced region,'' the events collected by a single-jet trigger are used, and at least two large-$R$ jets and a lepton veto are required.
One of the large-$R$ jets is required to have $\pt > 500\GeV$.
%To define the quark and gluon enhanced region, the following event selections are required,
%\begin{itemize}
%\item Multi-jet control region
%  \begin{itemize}
%  \item at least one large-$R$ jet with $\pT > 500$~\GeV
%  \item at least one additional large-$R$ jet with $\pT > 200$~\GeV
%  \item lepton veto
%  \item pass a single-jet trigger
%  \end{itemize}
%\item $\gamma$ + jets control region
%  \begin{itemize}
%  \item at least one photon with $\pT > 155$~\GeV
%  \item at least one large-$R$ jet with $\pT > 200$~\GeV~and $\Delta\phi (J,\gamma ) > \pi / 2$
%  \item pass a single-photon trigger and select a photon passes ``loose'' quality criteria
%  \end{itemize}
%\end{itemize}

The distributions of jet substructure variables in each region are shown in Figures~\ref{fig:bjt:ctrl_plots_gammajet}, and Figure~\ref{fig:bjt:ctrl_plots_dijet} in the \gammajets and multi-jets enhanced regions, respectively.
In the multi-jet enhanced region, multi-jet events account for more than 99\%.
In the \gammajets enhanced region, \gammajets events also account for more than 98\%.
\mcomb and $D_{2}$ distributions of \PYTHIAV{8}~\cite{Comp.Phys.Comm.191.159} and \SHERPA~\cite{sherpa} samples are good agreements with data in the multi-jet and \gammajets enhanced regions.
However, \ntrk distributions of \PYTHIAV{8} are different from \SHERPA samples and data.
\ntrk distributions of \SHERPA samples have similar mis-modeling in both the multi-jet and \gammajets enhanced regions.
A similar trend can be seen in the $W\ra qq$ signal efficiency measurement as shown in Figure~\ref{fig:bjt:ntrk}, and \ntrk is mis-modeled because it is not an IRC safe variable.

%%%%%%%%%%%%% enhanced plots gammajet %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[\mcomb]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_m_comb_data_mc_gammajet_log.pdf}}
  \subfigure[$D_{2}$]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_D2_combMgt40GeV_data_mc_gammajet.pdf}}
  \subfigure[\ntrk]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_ungroomed_ntrk500_combMgt40GeV_data_mc_gammajet.pdf}}
  \caption[Distribution of jet substructure variables in \gammajets enhanced region]{
Distributions of \mcomb (a), $D_{2}$ (b) and \ntrk (c) in \gammajets enhanced region.
\SHERPA samples are nominal MC and the difference between \PYTHIAV{8} and \SHERPA samples is assigned as a systematic uncertainty from generator difference.
}
   \label{fig:bjt:ctrl_plots_gammajet}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% control plots dijet %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[\mcomb]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_m_comb_data_mc_dijet_log.pdf}}
  \subfigure[$D_{2}$]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_D2_combMgt40GeV_data_mc_dijet.pdf}}
  \subfigure[\ntrk]{\includegraphics[width=0.45\columnwidth]{BosonTagging/figure/h_rljet0_ungroomed_ntrk500_combMgt40GeV_data_mc_dijet.pdf}}
  \caption[Distribution of jet substructure variables in multi-jet enhanced region]{
Distributions of \mcomb (a), $D_{2}$ (b) and \ntrk (c) in multi-jet enhanced region.
\PYTHIAV{8} samples are nominal MC and the difference between \PYTHIAV{8} and \SHERPA samples is assigned as a systematic uncertainty from generator difference.
}
   \label{fig:bjt:ctrl_plots_dijet}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Efficiency Measurement and Systematic Uncertainties}

The efficiency of the data and MC is measured as,
\begin{eqnarray}
  \epsilon_{\textrm{data}}(\pT, \log (m/\pT)) &=& \frac{N^{\textrm{tagged}}_{\textrm{data}} - N^{\textrm{tagged}}_{\textrm{MC background}}}{N^{\textrm{total}}_{\textrm{data}} - N^{\textrm{total}}_{\textrm{MC background}}} \label{eq:objects:bgEffDef}, \\
  \epsilon_{\textrm{MC}}(\pT, \log (m/\pT)) &=& \frac{N^{\textrm{tagged}}_{\textrm{MC} \gamma +\textrm{jets/multi-jets}}}{N^{\textrm{tagged}}_{\textrm{MC} \gamma +\textrm{jets/multi-jets}}+N^{\textrm{not tagged}}_{\textrm{MC} \gamma +\textrm{jets/multi-jets}}},
\end{eqnarray}
where $N^{\textrm{tagged,not tagged}}_{\textrm{MC}}$ represents the number of events estimated with jets which pass or fail the boson tagging of \gammajets or multi-jets MC samples, $N^{\textrm{total,tagged}}_{\textrm{data}}$ represents the number of events of the data in total or with jets which pass boson tagging requiremnets, and $N^{\textrm{total,tagged}}_{\textrm{MC background}}$ represents the number of the other background events, such as $W/Z+{\textrm{jets}}, \ttbar, W/Z+\gamma, \ttbar+\gamma$, as estimated with MC samples.
Unlike the signal efficiency measurement, fits need not be performed thanks to the high purity of quark- or gluon-initiated jets.

\PYTHIAV{8} samples are treated as nominal MC in the multi-jet enhanced region and alternative MC in \gammajets enhanced region.
In contrast, \SHERPA samples are treated as the opposite of \PYTHIAV{8} samples.


%%%%%%%%%%%%% 1-D bkg rejection %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[multijet]{\includegraphics[width=0.45\columnwidth, height=0.3\textheight]{BosonTagging/figure/smooth19WTag_50eff_PD2PNtrkPMassCut_rej_dijet.pdf}\label{fig:Object:multijets_1D}}
  \subfigure[$\gamma$+jets]{\includegraphics[width=0.45\columnwidth, height=0.3\textheight]{BosonTagging/figure/smooth19WTag_50eff_PD2PNtrkPMassCut_rej_gammajet.pdf}\label{fig:Object:gammajets_1D}}
  \caption[Background rejection of \Wqq-tagging as a function of \pt]{Measured background rejections for the $W$-tagging on the top panel, and the inverse of the log($m$/\pT)-inclusive SFs on the bottom panel.
    The results from (a) the multi-jet measurement and (b) the \gammajets are shown respectively.}
   \label{fig:bjt:bkg_rej}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The background rejection (defined as the inverse of the efficiency) is shown in Figure~\ref{fig:bjt:bkg_rej}.
The low background rejection at the low \pt region is caused by loose cut values of jet mass and \ntrk.
Similarly, in the high \pt region ($\pt > 1300\GeV$), loose cut values of the mass window and $D_{2}$ lead to the low background rejection.
%Thus, the higher jet \pt is, and the smaller the background rejection is.

In order to correct the difference in the measured efficiency between the data and MC, the SF for background jets is defined as the ratio of the data to MC.
The difference between the signal and background efficiency SF is that background SF is evaluated as a function of a jet \pT and mass because the statistics of data samples are sufficient.

To check the difference of quark- and gluon-initiated jets, the background rejections in the multi-jet and \gammajets enhanced regions are compared.
In the \gammajets enhanced regions, quark-initiated jets account for much.
The difference in the rejection factor ($500\GeV \leq \pt \leq 600\GeV$) between the multi-jets and \gammajets enhanced regions is about 10\% and smaller than the statistical uncertainty.
The difference between the two measurements is larger in the higher \pt region, while the statistical uncertainty is also larger, so they agree within the statistical uncertainty.
Furthermore, the difference in terms of the SF is canceled by the data and MC samples and reduced to 2\%.
Therefore, the two measurements are consistent, and thus, the difference in the SF derived from the two measurements is ignored.

Total systematic uncertainty is discussed in Section~\ref{sec:sys:bosontagging}.
In the background efficiency measurements, systematic uncertainties derived from the jet energy scale and resolution of large-$R$ jets and these generator differences are considered.
The generator difference between \PYTHIAV{8} and \SHERPA is assigned as a modeling uncertainty, and it is a dominant source of uncertainty.
%The results of the background rejection using \gammajets samples in the \pt regime above $500$~\GeV are not used because a statistical error is larger than multi-jet samples.


%%%%%%%%%%%%% 2-D SF results %%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[t]
% \centering
%  %\subfigure[W-tag SF for background]{\includegraphics[width=0.45\columnwidth, height=0.3\textheight]{BosonTagging/figure/QCD_jet_pass_SF.pdf}}
%  %\subfigure[W-tag efficiency for background]{\includegraphics[width=0.45\columnwidth, height=0.3\textheight]{BosonTagging/figure/QCD_jet_pass_eff.pdf}}
%  \includegraphics[width=0.45\columnwidth, height=0.3\textheight]{BosonTagging/figure/QCD_jet_pass_SF.pdf}
%  \caption[Background efficiency SF and efficiency]{Measured background SFs for jets passing the $W$-tagging WP. 
%}
%  \label{fig:bjt:bkg_sf_sys}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Background efficiency SF for $Z$ boson tagging}

Unlike the signal efficiency SF, the background SF depends on the jet mass.
Thus, the $Z$ tagging SF is defined by shifting the W tagging,
\begin{eqnarray}
\textrm{SF}_{Z}(\pT, \log (m/\pT)) &=& \textrm{SF}_{W}(\pT, \log ((m-10.803)/\pT)).
\end{eqnarray}
The difference in the SF values between the measurement using the $Z$ boson tagging and evaluation with shifted mass method from the $W$ boson tagging is typically 1\%, at maximum 4\%.
It is small enough compared with total uncertainty ($10$-$12$\%).

\subsection{Physics Process Dependency}
In order to compare the efficiency between the efficiency measurement regions and the main analysis regions to search for electroweakinos (described in Section~\ref{sec:sel:signalregion}), the signal and background efficiency using MC samples of the SUSY signals and \Znunu backgrounds are evaluated and compared with \ttbar, \gammajets, and multi-jets, respectively.
The comparisons are shown in Figure~\ref{fig:sys:jetcomp_eff}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[Signal efficiency \label{fig:sys:signal_jetcomp_eff}]{\includegraphics[width=0.48\textwidth]{Systematics/figure/SignalEffUnc}}
\subfigure[Background efficiency \label{fig:sys:bkg_jetcomp_eff}]{\includegraphics[width=0.48\textwidth]{Systematics/figure/BkgEffUnc}}
\caption[Process dependency of boson tagging performance between the measurement regions and the SUSY analysis regions]{
(a) The boson tagging signal efficiency of \ttbar control samples in the measured region and SUSY signals in the analysis regions.
(b) The boson tagging background rejection of \gammajets and multi-jets control samples in the measured region and \Znunu backgrounds in the analysis regions.
The signal efficiency is measured using large-$R$ jets labeled as $W$ bosons, and the polarization causes the difference derived from the origins of $W$ bosons.
In the background efficiency, the number of sub-jet (spatially matched small-$R$ jets to large-$R$ jets with $dR(\textrm{small-}R~\textrm{jet}, \textrm{large-}R~\textrm{jet})<1.0$) is sensitive to jet substructure variables because it correlates $D_{2}$ value.
Background jets are categorized with the hardest associated truth particle of spatially matched small-R jets.
}
\label{fig:sys:jetcomp_eff}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

Figure~\ref{fig:sys:signal_jetcomp_eff} shows that the difference in the efficiency of the signal jets is about $10\%$.
The difference can be explained as due to the polarization of $W$ bosons, i.e., the fraction of $W$ bosons originating from top quarks with longitudinal polarization is $\sim 69\%$~\cite{TOPQ-2018-02}, and $W$ bosons from SUSY particles have $\sim 100\%$ longitudinal polarization.
Since MC modeling of top quarks agrees with the data, the difference in the data/MC between the parents of $W$ bosons is assumed to be negligible.
Thus, the measured signal efficiency in the $W$ boson enhanced region is used without additional uncertainties in the analysis.
However, since the inefficiency SF depends on the efficiency and efficiency SF, the inefficiency SF may be incorrectly estimated due to the difference.
Therefore, additional uncertainty (10\%) on the inefficiency SF derived from the difference in the efficiency is applied.

For the backgrounds, the difference in the background rejection derived from the jet origin can be seen in Figure~\ref{fig:sys:bkg_jetcomp_eff}.
As discussed in Section~\ref{sec:bjt:background_sf}, while there is the difference in the background rejection factor, the difference in the data/MC is canceled, i.e., the difference of the SF is small.
Thus, no additional uncertainty for the background efficiency SF is applied.
However, additional uncertainty on the background inefficiency SF derived from the difference in the measured efficiency is assigned.


