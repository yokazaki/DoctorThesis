
BASENAME = Thesis

PDFLATEX = pdflatex
PDFLATEXOPTIONS = "-interaction=nonstopmode"
PDFLATEXOPTIONS = ""
PDFLATEX_STDIN = "H\\nH\\nX\\n"
BIBTEX = bibtex
PYTHON = /usr/bin/python

VERSION=$(shell grep newcommand src/Thesis.tex | grep DraftVersion | grep -v % | cut -d { -f 3 | cut -d } -f 1)

default: $(BASENAME)_v$(VERSION).pdf
	@echo "Made:"
	@echo "===================================================================="
	@echo ""
	@echo "    $(PWD)/$<"
	@echo ""
	@echo "===================================================================="

%_v$(VERSION).pdf: %.pdf
	cp $< $@

%.pdf: src/%.tex bib/*.bib %.bbl */*.tex */tables/*.tex */tables/*/*.tex atlaslatex/latex/* src/*.sty src/*/*.sty
	@echo "$(PDFLATEX) $<"
	echo -e "$(PDFLATEX_STDIN)" > stdin.tmp.txt
	$(PDFLATEX) $(PDFLATEXOPTIONS) $< < stdin.tmp.txt
	$(PDFLATEX) $(PDFLATEXOPTIONS) $< < stdin.tmp.txt


%.bbl: %.aux
	@echo "$(BIBTEX) $< $@"
	-$(BIBTEX) $<

%.aux: src/%.tex bib/*.bib */*.tex */tables/*.tex atlaslatex/latex/* src/*.sty src/*/*.sty
	$(PYTHON) PrepareForGrammarly.py
	@echo "$(PDFLATEX) $<"
	echo -e "$(PDFLATEX_STDIN)" > stdin.tmp.txt
	$(PDFLATEX) $(PDFLATEXOPTIONS) $< < stdin.tmp.txt
	$(PYTHON) ProcessFeynmanDiagrams.py --dir metapost --pat *.mp




clean:
	-rm -r *.dvi *.toc *.aux *.log *.out \
		*.bbl *.blg *.brf *.bcf *-blx.bib *.run.xml \
		*.cb *.ind *.idx *.ilg *.inx \
		*.synctex.gz *~ *.fls *.fdb_latexmk .*.lb spellTmp stdin.tmp.txt ._* \
		metapost/* grammarly

cleanpdf:
	-rm $(BASENAME)*.pdf

cleanps:
	-rm $(BASENAME)*.ps

cleanversioning:
	-rm $(BASENAME)_v* localversion.sty

cleanall: clean cleanpdf cleanps cleanversioning

# Clean the PDF files created automatically from EPS files
