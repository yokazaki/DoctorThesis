

Since the extrapolation from the CRs to the SRs relies on MC simulation, the modeling of MC simulation is important.
$W(\ra l\nu)+\textrm{jets}$ and \gammajets have larger cross-sections and similar diagrams with \Zjets, which is the dominant background process in the SRs.
Then, $W(\ra l\nu)+\textrm{jets}$ and \gammajets are used to validate the background estimation.

In the \Znunu process, \met represents \pT of a $Z$ boson.
As discussed later in Section~\ref{sec:sel:comp_presel}, the \pT of $W$ boson for \Wjets in the 1L category and photon for $\gamma+$jets in the 1Y category can be used as good proxies of $Z$ bosons.
Additional CRs and validation regions (denoted as ``VRs'') like the SRs for $W+\textrm{jets}$/\gammajets in the 1L/1Y categories are defined.
Similarly, the normalization factors are defined in the CRs and are applied in the VRs.

Like the 0L category, multi-jet backgrounds are also negligible in the 1L and 1Y categories since an isolated lepton or a high \pt photon in validation regions is required.
More detail is described in Appendix~\ref{sec:app:multijet}.
%Thus, the number of leptons is required to define CRs/VRs for \Wjets, and the 1-lepton category is denoted as ``1L.''
%Besides, the number of photons is also required to define CRs/VRs for \gammajets, and the 1-photon category is denoted as ``1Y.''

\subsection{Preselection in 1L/1Y Categories}

Selections to define CRs and VRs in the 1L and 1Y categories are introduced.
The definitions are similar to the CRs and SRs in the 0L category.
The difference between target backgrounds in each category is the species of bosons.
In the 0L category, \Znunu backgrounds are targeted, and \met is used to define the SRs.
In \Znunu events, \met represents \pt of $Z$ bosons.
Thus, we need to use variables that are a substitute for \met.
In the 1Y category, \gammajets backgrounds are targeted, and \pt of photons (\ptY) is a good proxy of \met.
Then, we can use \ptY instead of \met to calculate substitute variables of \meffJ, \mindphimet, and \mTtwoJJ.
Considering \Wjets in the 1L category, we can use \pt of reconstructed $W$ (\ptW) as a vector sum of transverse momentum of a lepton and $\vec{p}_{\textrm{T}}^{\textrm{miss}}$.
We define \meffJV, \mindphiV, and \mTtwoJJV as common variables in the 0L, 1L, and 1Y categories.
They are extended variables for \meffJ, \mindphimet, and \mTtwoJJ in the 1L/1Y categories where \ptW and \ptY are used instead of \met.

In the 1L category, a new preselection is defined.
Two types of single-lepton triggers are required: a single-electron trigger and a single-muon trigger.
The thresholds of these triggers are selected to correspond to the data period~\cite{TRIG-2018-05,TRIG-2018-01}.
As described above, kinematic selections are also defined using \ptW instead of \met.
Similarly, a new preselection is also defined in the 1Y category, such as single-photon triggers, \ptY, and \mindphiV.
The preselections are summarized in Table~\ref{tab:sel:presel}.

\input{EventSelection/table/Preselections}

The data and MC distributions in preselected regions of 1L/1Y categories are shown in Figure~\ref{fig:sel:pre_1l_var},~\ref{fig:sel:pre_1y_var}
The normalization of the 1Y4Q region is caused by the overestimated cross-section of \gammajets in \SHERPA samples.
The cross-section is calculated at the NLO, and a similar tendency is also observed in the SM \gammajets measurement~\cite{STDM-2017-01}.
However, the disagreement is not significant because it is constrained by using the normalization factor.

The general trends of 2B2Q/4Q are seen in each 0L, 1L, 1Y category.
Each double ratio of the data/MC in 2B2Q/4Q is $1.27\pm 0.05$ in 0L, $1.17\pm 0.05$ in 1L, and $1.23\pm 0.06$ in 1Y.
The double ratios have good agreement between the 0L, 1L, and 1Y categories.
Then, they imply that the $2b$-tagged fraction (such as a gluon splitting into a $bb$ pair) is underpredicted by MC samples.
Since the difference in the normalization between the regions is large, the normalization factor in each region is evaluated individually.
Consequently, the difference is not sensitive to the background estimation strategy in the analysis because the data/MC value is canceled by the ratio of VRs/CRs or SRs/CRs.
What is more important in the background estimation is the shape similarity of backgrounds between 0L/1L/1Y categories, as discussed below.
%The comparison of kinematic distributions in MC samples is discussed in Section~\ref{sec:sel:comp_presel}.


%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
   \subfigure[\mindphimet in Precut1L4Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/dphi_Precut1L4Q_dataMC.pdf}}
   \subfigure[leading large-$R$ jet \pt in Precut1L4Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/J1Pt_Precut1L4Q_dataMC.pdf}}
   \subfigure[\mindphimet in Precut1L2B2Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/dphi_Precut1L2B2Q_dataMC.pdf}}
   \subfigure[$m(J_{bb})$ in Precut1L2B2Q \label{fig:sel:jbbm_pre_1L2B2Q}]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/J2bM_Precut1L2B2Q_dataMC.pdf}}
  \caption[\mindphiV and leading large-$R$ jet variables with Precut1L4Q/Precut1L2B2Q.]{
Kinematic distributions of \mindphimet in Precut1L4Q (a) and Precut1L2B2Q (c).
Leading large-$R$ jet \pt in Precut1L4Q (b) and $m(J_{bb})$ in Precut1L2B2Q (d) are shown.
}
  \label{fig:sel:pre_1l_var}
\end{figure}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
   \subfigure[\mindphimet in Precut1Y4Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/dphi_Precut1Y4Q_dataMC.pdf}}
   \subfigure[leading large-$R$ jet \pt in Precut1Y4Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/J1Pt_Precut1Y4Q_dataMC.pdf}}
   \subfigure[\mindphimet in Precut1Y2B2Q]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/dphi_Precut1Y2B2Q_dataMC.pdf}}
   \subfigure[$m(J_{bb})$ in Precut1Y2B2Q \label{fig:sel:jbbm_pre_1Y2B2Q}]{\includegraphics[width=0.45\textwidth]{EventSelection/figure/J2bM_Precut1Y2B2Q_dataMC.pdf}}
  \caption[\mindphiV and leading large-$R$ jet variables with Precut1Y4Q/Precut1Y2B2Q.]{
Kinematic distributions of \mindphimet in Precut1Y4Q (a) and Precut1Y2B2Q (c).
Leading large-$R$ jet \pt in Precut1Y4Q (b) and $m(J_{bb})$ in Precut1Y2B2Q (d) are shown.
}
  \label{fig:sel:pre_1y_var}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

\subsection{Definition of CRs and VRs in the 1L/1Y Categories}

New CRs in 1L/1Y categories are defined and denoted as CR1L-4Q, CR1L-2B2Q, CR1Y-4Q, and CR1Y-2B2Q, respectively.
Additionally, new VRs are defined and denoted as VR1L-4Q, VR1L-2B2Q, VR1Y-4Q, and VR1Y-2B2Q, respectively.
The selections of the CRs and VRs are summarized in Table~\ref{tab:bg:vr_1l} and Table~\ref{tab:bg:vr_1y}.
However, looser kinematic selections are applied to maintain sufficient data statistics than similar ones in the 0L category.
For the 2B2Q regions in the 1L/1Y categories, $m(J_{bb})$ selections in CR1L-2B2Q and CR1Y-2B2Q are consistent with CR0L-2B2Q, and inclusive $m(J_{bb})$ selections in VR1L-2B2Q and VR1Y-2B2Q are applied.

Background compositions in each region are shown in Figure~\ref{fig:bg:vr_comp}.
The composition of minor backgrounds in the 1L and 1Y categories are similar to the 0L category, and it is similar between SRs and VRs and corresponding CRs.



\input{BackgroundEstimation/table/VR_Selection}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.98\hsize]{BackgroundEstimation/figure/VR_BGcomp.pdf}
\caption{Background composition of physics processes in CRs, VRs, and SRs. }
\label{fig:bg:vr_comp}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to guarantee that $W+\textrm{jets}$ and \gammajets can be used for validation, the kinematic distributions of $W+\textrm{jets}$ and \gammajets need to be confirmed to have similar shapes of \Zjets and whether reconstructed $W$ bosons in the 1L category and photons in the 1Y category are good proxies as $Z$ bosons in the 0L category, or not.
%The comparison of kinematics is discussed in Section~\ref{sec:sel:comp_presel} using $V+$jets MC.
The jet origins of \Zjets, \Wjets, and \gammajets are similar in each CR, SR, and VR since $W$, $Z$, and $\gamma$ are not the origin of jets.
More details of jet origins are shown in Appendix~\ref{sec:app:jetcomp}.
In the 1L/1Y VRs, signal contaminations (shown in Appendix~\ref{sec:app:signalcontami_vr}) are negligible since signals are required to decay semi-leptonically and need a hard initial state radiation jet ($\pT > 200\GeV$, $m > 40\GeV$) to pass $\nJ \geq 2$ requirement in the 1L category, or emit a hard initial state radiation photon in the 1Y category.

%Like 0L regions, each normalization factor (denoted as NF) evaluated in CR applies to corresponding VR respectively.
%Thus, four NFs are evaluated for 1L-4Q, 1L-2B2Q, 1Y-4Q, and 1Y-2B2Q.
%The NF is defined,
%\begin{equation}
%\textrm{NF} = \frac{N_{\textrm{data}}-N_{\textrm{irreducible BG}}}{N_{\textrm{reducible BG}}}.
%\end{equation}
MC modeling in each region should be compared to verify the reducible background estimation strategy.
A transfer factor (TF) as a ratio of the event yields from region A to region B, i.e. $r(A \ra B) := y(B)/y(A)$, is defined.
The MC modelings as the TFs between $r(\text{CR0L} \ra \text{SR})$ and $r(\text{CR1L/1Y} \ra \text{VR1L/1Y})$ need to be checked and confirmed that they are similar.
Two following assumptions will be checked,
\begin{itemize}
\item The extrapolations from CRs to SRs/VRs depend on the $W/Z \ra qq$ tagging when kinematic selections are matched in all regions
\item The difference between the data and MC samples in actual CRs/SRs/VRs is small enough 
\end{itemize}
For checking the first item, the distributions of jet substructure variables and the TFs between 0L, 1L, and 1Y categories are compared in Section~\ref{sec:bg:jetvarcomp}.
Additionally, the TF trends in the data and MC samples are compared by the level of the agreement for the second item in Section~\ref{sec:bg:TFcomp}.

\clearpage

\subsection{Comparison between 0L/1L/1Y for $V+$jets MC}
\label{sec:sel:comp_presel}
\input{EventSelection/Comparison_Presel}

\subsection{Jet variable comparison in the CRs and SRs/VRs}
\label{sec:bg:jetvarcomp}
The MC-to-MC distributions of $J^{\text{fail V-tag}}$ variables in CR0L-4Q, CR1L-4Q, and CR1Y-4Q are shown in Figure~\ref{fig:bg:MC_jet_4Q}.
This figure shows the sum of the reducible backgrounds, containing other than \Vjets MC samples too.
Selections in CR0L-4Q are loosened so that they are consistent with the same as selections of CR1L-4Q/CR1Y-4Q, i.e., \meffJV (\ptV) cut is loosened from $1300~(300)\GeV$ to $1000~(200)\GeV$.
Reasonable agreements of jet variables between the three regions are found in MC samples.
Figure~\ref{fig:bg:MC_jet_2B2Q} shows a similar MC-to-MC comparison for 2B2Q regions.
Like the 4Q categories, reasonable consistencies within the limited MC statistics are seen in all samples.
The data-to-data distributions in the data are shown in Appendix~\ref{app:jfailvar_data}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[htb!p]
\centering
\subfigure[\pT]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFPt_4Q_fail_diff_MC.pdf}}
\subfigure[mass]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFM_4Q_fail_diff_MC.pdf}}
\centering
\subfigure[$D_{2}$]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFD2_4Q_fail_diff_MC.pdf}}
\subfigure[\ntrk]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFNtrk_4Q_fail_diff_MC.pdf}}
\caption[MC-to-MC comparison ofjet substructure variables in CR-4Q bins.]{
MC-to-MC comparison of et substructure variables of the failed jet for boson tagging requirements in CR0L-4Q, CR1L-4Q, and CR1Y-4Q.
CR0L-4Q selections are loosened to $\meffJV > 1000\GeV$, $\ptV > 200\GeV$, respectively, to align with CR1L-4Q and CR1Y-4Q.
}
\label{fig:bg:MC_jet_4Q}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[htb!p]
\centering
\subfigure[\pT]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFPt_2B2Q_fail_diff_MC.pdf}}
\subfigure[mass]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFM_2B2Q_fail_diff_MC.pdf}}
\centering
\subfigure[$D_{2}$]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFD2_2B2Q_fail_diff_MC.pdf}}
\subfigure[\ntrk]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JFNtrk_2B2Q_fail_diff_MC.pdf}}
\caption[MC-to-MC comparison of jet substructure variables in CR-2B2Q bins.]{
MC-to-MC comparison of jet substructure variables of the fail jet for boson tagging requirements in CR0L-2B2Q, CR1L-2B2Q, and CR1Y-2B2Q.
}
\label{fig:bg:MC_jet_2B2Q}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

Similarly, MC-to-MC comparisons are checked in the SR-like regions where both jets are satisfied with boson tagging requirements, as shown in Figure~\ref{fig:bg:MC_leading_jet_pt_SR}.
Other variables are shown in Appendix~\ref{app:jfailvar_data}.
To maintain MC statistics, \meffJV and \ptV cuts are loosened to $800\GeV$ and $200\GeV$.
For the 2B2Q categories, $m(J_{bb})$ cut in SR-2B2Q is changed to the logical union of $Z\ra bb$ and $h\ra bb$ cuts to align with VR1L-2B2Q and VR1Y-2B2Q.
Like the CRs, reasonable consistencies within the limited MC statistical uncertainty are seen in all regions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[htb!p]
\centering
\subfigure[leading large-R jet \pT in SR-4Q and VR-4Q bins]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/J1Pt_4Q_fail_diff_MC.pdf}}
\subfigure[sub-leading large-R jet \pT in SR-4Q and VR-4Q bins]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/J2Pt_4Q_fail_diff_MC.pdf}}
\subfigure[$J_{qq}$ \pT in SR-2B2Q and VR-2B2Q bins]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/JqqPt_2B2Q_fail_diff_MC.pdf}}
\caption[MC-to-MC comparison of jet substructure variables of the leading jet in SR(VR)-4Q bins/SR(VR)-2B2Q bins.]{
MC-to-MC comparison of leading large-$R$ jet \pt distributions (a) and sub-leading large-$R$ jet \pt distributions (b) in SR-4Q-VV, VR1L-4Q, and VR1Y-4Q with loose kinematic selections, $\meffJV > 800\GeV$, $\ptV > 200$~\GeV in all regions to maintain MC statistics.
MC-to-MC comparison of $\pt(J_{qq})$ distributions (c) in SR-2B2Q-VZh, VR1L-2B2Q, and VR1Y-2B2Q with loose kinematic selections, $\meffJV > 800\GeV$.
}
\label{fig:bg:MC_leading_jet_pt_SR}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\clearpage

\subsection{Transfer factor comparison}
\label{sec:bg:TFcomp}

To avoid the fluctuation of the data and MC samples, the transfer factor (TF) comparisons with loose kinematic selections ($\ptV>200~\GeV,~\meffJVs>0.8$~TeV in the 4Q and 2B2Q categories, additionally $\mTtwoJJV>200~\GeV$ for the 2B2Q categories) are shown in Figure~\ref{fig:Background_boosted:ratio_2Tover1T1F_loose}.
The largest disagreement between the data and MC samples is observed in the 1Y-2B2Q region, but the difference is consistent within the statistical uncertainty.
The transfer factors of the data and MC samples in the 0L-2B2Q region are in good agreement with the 1L-2B2Q and 1Y-2B2Q regions.
Additionally, the transfer factors are good agreements in the 4Q regions of the 0L, 1L, and 1Y categories.
The difference in the TFs between the 4Q and 2B2Q categories is a factor of 2 and this value is reasonable because
\begin{itemize}
\item The ratios in the 4Q categories are defined as ($J_{1}$ and $J_{2}$ pass)/(($J_{1}$ pass and $J_{2}$ fail) $||$ ($J_{1}$ fail and $J_{2}$ pass)) $\sim 1/2\times$(pass/fail).
\item In 2B2Q categories, the ratios defined as ($J_{qq}$ pass)/($J_{qq}$ fail) $\sim$ (pass/fail).
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[h]
% \centering
%  \subfigure[2T/1T1F (SR-like kinematics) \label{fig:Background_boosted:ratio_2Tover1T1F_nomKine}]{\includegraphics[width=0.7\columnwidth]{BackgroundEstimation/figure/TransferFactor.pdf}}
%  \subfigure[2T/1T1F (loosened kinematics) \label{fig:Background_boosted:ratio_2Tover1T1F_loose}]{\includegraphics[width=0.7\columnwidth]{BackgroundEstimation/figure/TransferFactor_loose.pdf}}
%  \caption[Data/MC comparison of the transfer factors from 1T1F (CR-like) to the 2T region (SR-like).]{
%  Data/MC comparison of the transfer factors (the ratios in the yields) from 1T1F(CR-like)  to the 2T region (SR-like) with the kinematics matched between the 0L, 1L and the 1Y regions.
%  In figure (a) the kinematic cuts in $\meffJVs(:=\meffJV)$, $\ptV$ and $\mTtwoJJV$ in the 1L and 1Y regions are changed to match with that in the SRs: 
%  (4Q) $\ptV>300~\GeV,~\meffJVs>1.3~\TeV$ and (2B2Q) $\ptV>200~\GeV,~\mTtwoJJV>250~\GeV,~\meffJVs>1~\TeV$.
%  In figure (b) all the regions are loosened commonly to $\ptV>200~\GeV,~\meffJVs>0.8~\TeV$ (4Q), or $\ptV>200~\GeV,~\mTtwoJJV>200~\GeV,~\meffJVs>0.8~\TeV$ (2B2Q).
%  }
%  \label{fig:Background_boosted:ratio_1T1Fover2T_sameKine}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
\subfigure[loose selected region \label{fig:Background_boosted:ratio_2Tover1T1F_loose}]{\includegraphics[width=0.45\columnwidth]{BackgroundEstimation/figure/TransferFactor_loose.pdf}}
\subfigure[original SR/VR selection \label{fig:Background_boosted:ratio_2Tover1T1F_nomKine}]{\includegraphics[width=0.45\columnwidth]{BackgroundEstimation/figure/TransferFactor.pdf}}
\caption[Data/MC comparison of the transfer factors from CR-like regions to the SR/VR-like regions with kinematic selections.]{
Data/MC comparison of the transfer factors (the ratios in the yields) from CR-like regions to the SR-like regions with the kinematics matched between the 0L, 1L and the 1Y categories.
(a) All the regions are loosened commonly to $\ptV>200\GeV,~\meffJVs>0.8~\TeV$ for the 4Q category, or $\ptV>200\GeV,~\mTtwoJJV>200\GeV,~\meffJVs>0.8~\TeV$ for the 2B2Q category.
(b) The kinematic selections in $\meffJVs(:=\meffJV)$, $\ptV$ and $\mTtwoJJV$ in the 1L and 1Y categories are changed to match with that in the SRs:
$\ptV>300\GeV,~\meffJVs>1.3~\TeV$ for the 4Q category and $\ptV>200\GeV,~\mTtwoJJV>250\GeV,~\meffJVs>1~\TeV$ for the 2B2Q category.
}
\label{fig:Background_boosted:ratio_2Tover1T1F_all}
\end{figure}

A similar check is also performed using kinematic selections aligned to SRs and shown in Figure~\ref{fig:Background_boosted:ratio_2Tover1T1F_nomKine}.
In the 2B2Q categories, the TFs of MC samples are good agreements between the 0L, 1L, and 1Y categories.
However, the TF in VR1L-4Q is larger than SR-4Q bins and VR1Y-4Q.
The difference is mainly caused by the limitation of the MC statistics.


%\begin{figure}[t]
%\centering
%\includegraphics[width=0.7\columnwidth]{BackgroundEstimation/figure/TransferFactor.pdf}
%\caption[Data/MC comparison of the transfer factors from 1T1F (CR-like) to the 2T region (SR-like).]{
%Data/MC comparison of the transfer factors (the ratios in the yields) from 1T1F(CR-like)  to the 2T region (SR-like) with the kinematics matched between the 0L, 1L and the 1Y regions.
%the kinematic cuts in $\meffJVs(:=\meffJV)$, $\ptV$ and $\mTtwoJJV$ in the 1L and 1Y regions are changed to match with that in the SRs:
%(4Q) $\ptV>300~\GeV,~\meffJVs>1.3~\TeV$ and (2B2Q) $\ptV>200~\GeV,~\mTtwoJJV>250~\GeV,~\meffJVs>1~\TeV$.
%}
%\label{fig:Background_boosted:ratio_2Tover1T1F_nomKine}
%\end{figure}


The TFs with various kinematic selections are considered to check trends of TFs in the 4Q categories.
Figure~\ref{fig:Background_boosted:TF_vs_meffJ_loose} shows the TFs as a function of varying \meffJV thresholds with constant \ptV selection ($\ptV > 200\GeV$).
There is no significant difference between the 0L and 1Y categories.
However, some up-trends with respect to the 0L above 1.3~TeV in \meffJV threshold are seen in the 1L category.
This trend is caused by the MC stat fluctuation.
The TFs between the data and MC samples in the 1Y category are consistent within the statistical error.
However, some down-trend of data/MC above 0.9~TeV \meffJV threshold is seen in the 1L category.
As shown in Figure~\ref{fig:bg:vr1l4q_meff}, the difference is caused by the data down fluctuation because a good agreement is found in $\meffJV < 1$~TeV.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  %\subfigure[\label{fig:Background_boosted:TF_vs_meffJ_nominal}]{\includegraphics[width=0.45\columnwidth]{BackgroundEstimation/figure/Meff_cutvalues_nom.pdf}}
  %\subfigure[\label{fig:Background_boosted:TF_vs_meffJ_loose}]{\includegraphics[width=0.45\columnwidth]{BackgroundEstimation/figure/Meff_cutvalues_loose.pdf}}
  \includegraphics[width=0.6\columnwidth]{BackgroundEstimation/figure/Meff_cutvalues_loose.pdf}
  \caption[Transfer factor as a function of the \meffJV cut value.]{
   Top panel: TF~(CR-like $\ra$ SR/VR-like) for the 0L-4Q, 1L-4Q, and 1Y-4Q regions, as a function of the \meffJV cut value. 
   The MC TFs are shown in dashed lines and the data TFs (only for the 1L-4Q and 1Y-4Q) are illustrated in the solid ones.
   Middle panel: Data/MC of the TFs from the top panel. 
   %Bottom panel: MC-to-MC ratio of 1L/1Y MC TF to the 0L MC TF, with the (a) \ptV cut aligned with the SRs ($\ptV>300~\GeV$), or (b) with the VR1L(1Y)-4Q ($\ptV>200~\GeV$).
   Bottom panel: MC-to-MC ratio of 1L/1Y MC TF to the 0L MC TF, with the \ptV cut aligned with the VR1L(1Y)-4Q ($\ptV>200~\GeV$).
  }
  %\label{fig:Background_boosted:TF_vs_meffJ}
  \label{fig:Background_boosted:TF_vs_meffJ_loose}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.7\hsize]{BackgroundEstimation/figure/N1_plot_Meff_VR1L4Q_2T_100GeV.pdf}
\caption{\meffJV distribution in VR1L-4Q region (after fit).}
\label{fig:bg:vr1l4q_meff}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Additionally, Figure~\ref{fig:Background_boosted:TF_vs_pTV_loose} shows the trend of the 4Q TFs as a function of \ptV with loose \meffJV cut ($\meffJV > 1$~TeV).
For the MC TFs, there is no significant trend of \ptV cut values in the 0L/1L/1Y regions and good agreement between the 0L and 1L regions.
There is no significant difference in the MC TFs between the 0L and 1Y because the MC fluctuation causes the 1Y/0L ratio deviation from unity.

For the data/MC of the TFs in the 1Y region, there is no dependency on \ptV cut values and a good agreement between the data and MC samples.
In the 1L region, due to the data down fluctuation at $\meffJV \sim 1$~TeV, there is no trend for the \ptV cut values while the ratio is $\sim 0.7$



In conclusion, no significant trend in TF disagreement between the 0L, 1L, and 1Y categories is seen.
As well as between the data and the MC samples, no disagreement is seen in the 1L/1Y categories.
The results in VRs with full systematics are discussed in Section~\ref{sec:result:bgfig}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  %\subfigure[\label{fig:Background_boosted:TF_vs_pTV_nominal}]{\includegraphics[width=0.6\columnwidth]{BackgroundEstimation/figure/pTV_cutvalues_nom.pdf}}
  %\subfigure[\label{fig:Background_boosted:TF_vs_pTV_loose}]{\includegraphics[width=0.6\columnwidth]{BackgroundEstimation/figure/pTV_cutvalues_loose.pdf}}
  \includegraphics[width=0.6\columnwidth]{BackgroundEstimation/figure/pTV_cutvalues_loose.pdf}
  \caption[Transfer factor as a function of the \ptV cut value.]{
   Top panel: TF~(CR-like $\ra$ SR/VR-like) for the 0L-4Q, 1L-4Q, and 1Y-4Q regions, as a function of the \ptV cut value. 
   The MC TFs are shown in dashed lines and the data TFs (only for the 1L-4Q and 1Y-4Q) are illustrated in the solid ones.
   Middle panel: Data/MC of the TFs from the top panel. 
   Bottom panel: MC-to-MC ratio of 1L/1Y MC TF to the 0L MC TF, 
   %with the (a) \meffJVs cut aligned with the SRs ($\meffJVs>1.3~\TeV$), or (b) with the VR1L(1Y)-4Q ($\meffJVs>1.0~\TeV$).
   with the \meffJVs cut aligned with the VR1L(1Y)-4Q ($\meffJVs>1.0~\TeV$).
  }
  %\label{fig:Background_boosted:TF_vs_pTV}
  \label{fig:Background_boosted:TF_vs_pTV_loose}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

