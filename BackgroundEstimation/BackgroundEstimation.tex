\DeclareInputPath{Background Estimation}
\chapter{Background estimation}
\label{ch:Backgroundestimation}

In this chapter, a method to estimate backgrounds is described.
The background composition in each SR as estimated with MC samples is shown in Figure~\ref{fig:bg:bg_comp_sr} as an illustration.
The dominant background is $V+$jets in all the SRs and accounts for $> 60\%$ where $V$ stands for the SM electroweak boson.
$VV$, $t+X$, and \ttbar are the sub-dominant backgrounds.
These are classified as reducible backgrounds, as explained in Section~\ref{sec:bg_est:strategy}.
$VVV$ and $\ttbar+X$ are minor backgrounds and account for $\sim 10\%$.
These are classified as irreducible backgrounds.
The overview of the strategy how to estimate the background is explained in Section~\ref{sec:bg_est:strategy}.
Then, how the reducible backgrounds are estimated and validated are described in Sections~\ref{sec:bg:non-resonant} and~\ref{sec:bg:VR_1L1Y}, respectively.
The irreducible background estimation is discussed in Section~\ref{sec:bg:resonant}.
Multi-jet backgrounds are negligible in the SRs since high \met is required.
More detail is described in Appendix~\ref{sec:app:multijet}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.98\hsize]{BackgroundEstimation/figure/SR_BGcomp.pdf}
\caption[Background composition of physics processes estimated from the MC samples in SRs, directly.]
{Background composition of physics processes estimated from the MC samples in SRs, directly. 
SR-2B2Q-VZh is the logical union of SR-2B2Q-VZ and SR-2B2Q-Vh.
$V$ denotes $W/Z/h/\gamma$ in this figure.
}
\label{fig:bg:bg_comp_sr}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Strategy}
\label{sec:bg_est:strategy}


Due to requiring large \met ($\met > 200\GeV$ or $300\GeV$), SM processes containing leptonically decaying bosons, such as $W\ra l\nu$ and $Z\ra\nu\nu$, can remain as backgrounds by \met originating from $\nu$.
For the case of $W\ra l\nu$, $l$ must be $\tau$ decaying hadronically or $e/\mu$ experimentally not identified as lepton, since $\nL = 0$ is required.
These backgrounds can be classified into two categories, ``reducible backgrounds'' and ``irreducible backgrounds.''
\begin{description}
\item [Reducible backgrounds]: At most one of two leading large-$R$ jets originate from the real SM electroweak bosons, and the others are not from $W/Z/h$ bosons.
\Vjets (including \gammajets), $VV$ (including $V\gamma$), $t+X$, \ttbar and $Vh$ processes are categorized.
Figure~\ref{fig:bg:jet_comp_SR0L_4Q} and Figure~\ref{fig:bg:jet_comp_SR0L_2B2Q} show, the true origins of the large-$R$ jets as estimated from MC samples for the case of \Znunu, $VV$ in SR-4Q-VV, and \ttbar in SR-2B2Q-Vzh (the logical union of SR-2B2Q-VZ and SR-2B2Q-Vh).
It is seen that they contain at least one large-$R$ jet that originates from quarks or gluons of initial or final state radiations, as illustrated in Figure~\ref{fig:sample:feyn_vjets}.
This holds for all the reducible backgrounds (the origin of the large-$R$ jets for the other processes are in Appendix~\ref{sec:app:jetcomp}).
The reducible backgrounds account for at least 85\% of SRs.
\Znunu background is the dominant background in all the SRs, and the sub-dominant backgrounds are \Wjets and $VV$ (mainly $ZV\ra \nu\nu qq$).

\item [Irreducible backgrounds]: Both two leading large-$R$ jets originate from the real SM electroweak bosons, which decay hadronically.
In the SM, processes producing at least three SM electroweak bosons can create large \met (from one boson decaying leptonically) as well as two large-$R$ jets; therefore, they can become these irreducible backgrounds.
\ttbarX (mainly $tt(\ra bqqbqq)+Z(\ra\nu\nu)$) and $VVV$ (mainly $VV(\ra qqqq)+Z(\ra\nu\nu)$) are the main of this category.

\end{description}


The irreducible backgrounds are estimated with MC samples directly.
The reducible backgrounds are estimated in a data-driven way as follows.
``Control regions'' (CRs) with high purity of the reducible backgrounds are defined.
The reducible backgrounds are normalized in the CRs using the following normalization factor (NF):
\begin{equation}
\textrm{NF} = \frac{N_{\textrm{data}}-N_{\textrm{irreducible BG}}}{N_{\textrm{reducible BG}}},
\label{eq:bg_est:nf_def}
\end{equation}
where $N_{\textrm{irreducible BG}}$ and $N_{\textrm{reducible BG}}$ represent the number of events estimated by irreducible and reducible background MC samples directly.
This normalization factor is almost solely due to the $W/Z\ra qq$ tagging for quark- and gluon-initiated jets, i.e., correcting for the data and MC difference in such mis-tagging efficiency.
Then, to estimate the reducible backgrounds in the SRs, this normalization factor is applied to those in the SRs estimated with MC samples.
This corresponds to the extrapolation of the $W/Z\ra qq$ (mis-)tagging against quark- or gluon-initiated jets from the CR to SR.
The normalization factors separately for the 4Q and 2B2Q categories are estimated.
More detail is introduced in Section~\ref{sec:bg:non-resonant}.



%Multi-jet backgrounds are negligible in SRs and validation regions defined in Section~\ref{sec:bg:VR_1L1Y} since high \met in SRs, an isolated lepton, or a high \pt photon in validation regions is required.
%Multi-jet background estimation is described in Appendix~\ref{sec:app:multijet}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[\Znunu in SR-4Q-VV]{\includegraphics[width=0.7\textwidth]{BackgroundEstimation/figure/Znunu_JetComp_In_SR0L4Q.pdf}}
\subfigure[$VV$ in SR-4Q-VV]{\includegraphics[width=0.7\textwidth]{BackgroundEstimation/figure/diboson_JetComp_In_SR0L4Q.pdf}}
\caption[The fraction of the jet origin in reducible background events in SR-4Q-VV.]{
The fractions of the jet origin of \Znunu (a) and $VV$ (b) in SR-4Q-VV are shown.
The $x$-axis represents the origin of a leading large-$R$ jet, and the $y$-axis represents a sub-leading large-$R$ jet.
At least one jet is originated from quark or gluon, denoted as $q/g$ (ISR/FSR).
}
\label{fig:bg:jet_comp_SR0L_4Q}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[\Znunu in SR-2B2Q-VZh]{\includegraphics[width=0.7\textwidth]{BackgroundEstimation/figure/Znunu_JetComp_In_SR0L2B2Q.pdf}}
\subfigure[\ttbar in SR-2B2Q-VZh]{\includegraphics[width=0.7\textwidth]{BackgroundEstimation/figure/ttbar_JetComp_In_SR0L2B2Q.pdf}}
\caption[The fraction of the jet origin in reducible background events in SR-2B2Q-VZh.]{
The fractions of the origin of $J_{qq}$ and $J_{bb}$ in \Znunu (a) and \ttbar (b) control samples with SR-2B2Q-VZh selections are shown.
The $x$-axis represents the origin of $J_{bb}$, and the $y$-axis represents $J_{qq}$.
At least one jet is originated from quark or gluon, denoted as $q/g$ (ISR/FSR).
}
\label{fig:bg:jet_comp_SR0L_2B2Q}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

In this analysis, the statistical error on the data is 40-100\% in the SRs (discussed in Section~\ref{sec:result:bgfig}, and uncertainties are shown in Figure~\ref{fig:result:sr_yield}).
Thus, a robust background estimation is important, even if the uncertainty derived from the background estimation is a bit large.


\clearpage

\begin{fmffile}{metapost/DataAndSimulations-metapost}
\input{tmp_diagram}
\end{fmffile}


\section{Reducible Backgrounds Estimation}
\label{sec:bg:non-resonant}
\input{BackgroundEstimation/Non-resonant}

\section{Irreducible Backgrounds Estimation}
\label{sec:bg:resonant}
\input{BackgroundEstimation/Resonant}

