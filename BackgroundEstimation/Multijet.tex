\chapter{Multi-jet Backgrounds in SRs/CRs/VRs}
\label{sec:app:multijet}

Multi-jet backgrounds are supposed to be negligible due to $\met > 200\GeV$ in the SRs, an isolated lepton with $\pt > 30\GeV$, $\met > 50\GeV$, $\ptW > 200\GeV$ in the 1L category and an isolated photon with $\pt > 200\GeV$ in the 1Y category.
In similar phase spaces of the previous studies~\cite{Aad:2742847,Aad:2716368,Aad:2750578}, multi-jet backgrounds are negligible.
However, multi-jet backgrounds are still part of the ``reducible backgrounds,'' the contribution should be taken into account by the reducible background estimation in the first order.
Additionally, the jet composition of multi-jet samples is not similar to other samples, such as \Vjets or \ttbar.
Therefore, the goal in this section is that multi-jet backgrounds are confirmed to be negligible in each region using a data-driven method, i.e., the impact of jet composition differences between multi-jet and other backgrounds is small.

\section{0-lepton Category}

Multi-jet backgrounds remain in the 0L category due to ``fake \met'' caused by mis-measured jets typically.
However, $\mindphimet > 1.0$ is required by Precut0L, and the selection suppresses multi-jet backgrounds.
The blue line in Figure~\ref{fig:bg:jet_min_dphi_nomet} represents the \mindphimet distribution of multi-jet backgrounds with no \met cut and failing leading and sub-leading large-$R$ jets for boson tagging requirements.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[no \met cut \label{fig:bg:jet_min_dphi_nomet}]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/jetjet_dphimin_noCut}}
\subfigure[$\met > 100$~GeV]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/jetjet_dphimin_MET100}}
\caption[\mindphimet distributions of multi-jet background MC samples]{
\mindphimet distributions of multi-jet background MC samples.
(a) is not applied any \met cut and (b) is selected with $\met > 100~\GeV$.
The blue line represents the events with failing leading and sub-leading large-$R$ jets of boson tagging requirements, and the red line represents the events that contain one passing large-$R$ jet and one failing large-$R$ jet.
Each distribution is normalized to unity.
}
\label{fig:bg:jet_min_dphi}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

As shown in Figure~\ref{fig:bg:jet_min_dphi}, \mindphimet and the result of boson tagging is not correlated.
Thus, an ABCD method exploiting the invariant nature of \mindphimet shape regardless of the result of boson tagging is used for the multi-jet backgrounds estimation.
The definition of each region is summarized in Table~\ref{tab:bg:multi_0L_ABCD}.
With high \met cut, multi-jet backgrounds are strongly suppressed, and MC statistics are limited in high \mindphimet region ($\mindphimet > 1.0$).
In order to avoid limited statistics, multi-jet backgrounds are estimated from an exponential function fitted to the post-subtraction data, defined as the data subtracted non-multi-jet backgrounds of MC samples.
For example, multi-jet yield in region-B is calculated from the fitted exponential function in region-A.
The MC statistics are limited in region-C and region-D since at least one large-$R$ jet is required to satisfy boson tagging requirements.
Therefore, multi-jet yields in region-D are calculated as the estimated yields in region-B times the ratio of the failed region to pass region, i.e. $n(\textrm{region-B})\times n(\textrm{region-C})/n(\textrm{region-A})$.

\input{BackgroundEstimation/table/Multi_0L_Selection}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
\subfigure[$\met > 200$~GeV in region-(A+B)]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/QCD_hist_FF_MET200.pdf}}
\subfigure[$\met > 200$~GeV in region-(C+D)]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/QCD_hist_TF_MET200.pdf}}
\centering
\subfigure[$\met > 300$~GeV in region-(A+B)]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/QCD_hist_FF_MET300.pdf}}
\subfigure[$\met > 300$~GeV in region-(C+D)]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/QCD_hist_TF_MET300.pdf}}
\caption[\mindphimet distributions in (A+B)/(C+D) region with 0-lepton selections.]{
\mindphimet distributions in each region of the ABCD method.
\mindphimet distributions with $\met > 200~\GeV$, in the region where 2 fail leading large-$R$ jets of $W/Z\ra qq$ tagging (a) or one of leading large-$R$ jets pass (b).
\mindphimet distributions with $\met > 300~\GeV$ is shown in (c) and (d).
}
\label{fig:bg:all_min_dphi_ABCD}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

In order to calculate the upper yields of multi-jet backgrounds, the yields in region-B are estimated with an exponential function, such as $e^{ax+b}$.
Figure~\ref{fig:bg:QCD:diff} shows the \mindphimet distribution of multi-jet candidates in region-(A+B) with $\met > 200~\GeV$.
Black points represent (Data - non-multi-jet MC), and red histogram represents multi-jet MC samples.
The blue line is a fitted function.
The results of the ABCD method and the yields estimated from MC samples directly are shown in Table~\ref{tab:bg:multi_ABCD_0L_result}.
After applying $\met > 300~\GeV$, multi-jet backgrounds are negligible.
In the CRs and SRs, the contribution of multi-jet backgrounds is smaller since additional selections, such as boson tagging and \meffJ, are applied.

\input{BackgroundEstimation/table/Multi_ABCD_result}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
  \includegraphics[width=0.6\columnwidth]{BackgroundEstimation/figure/QCD_diff_MET_200.pdf}
  \caption[\mindphimet distribution of multi-jet candidates]{
\mindphimet distribution of multi-jet candidates in the region-(A+B) with $\met > 200~\GeV$.
The black points represent (Data - non multi-jet MC), and red histogram represents multi-jet MC samples.
The blue line is a fitted function.
}
  \label{fig:bg:QCD:diff}
\end{figure}
%-------------------------

\clearpage
\section{1L Category}

Multi-jet events with mis-identified leptons may remain in the 1L category.
Mis-identified leptons are referred to as ``fake leptons,'' and they are estimated by the fake-factor method~\cite{STDM-2011-24,ATL-PHYS-PUB-2010-005}.
In the fake factor method, the ratio is defined as the number of leptons with loose selections to ones with tight selections.
The loose selected leptons are referred to as ``anti-signal'' leptons.
They are defined as the ones, which fail the isolation selection and pass all the other signal lepton selections described in Table~\ref{tab:reco:summary_objects}.
The tightly selected leptons are the signal leptons.
Thus, the fake factor (FF) is defined as,
\begin{equation}
{\textrm {FF}}=\frac{N_{\textrm {signal}}}{N_{\textrm {anti-signal}}}.
\end{equation}
For estimating the fake factor, a kinematically loosely selected region is usually defined in a data-driven method.
However, such a region cannot be defined due to tight lepton identification criteria and large \pt ($> 30~\GeV$).
Therefore, the fake factor is evaluated from multi-jet MC samples directly.

The fake factors of electrons and muons are shown in Figure~\ref{fig:bg:FF_1L}.
The systematic uncertainty derived from the fake component variation is the largest source of uncertainty on the fake factor, and it is represented as the red box in Figure~\ref{fig:bg:FF_1L}.
These values are estimated as the variations with the fraction of fake events shifted up and down by a factor of two, originating from $b$-hadron decays, $c$-hadron decays, light-flavor hadron decays, punch-through pions (for $\mu$), and photon conversion (for $e$).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[Electron]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/fakeEff_ele_jetjet.pdf}}
\subfigure[Muon]{\includegraphics[width=0.48\textwidth]{BackgroundEstimation/figure/fakeEff_ele_jetjet.pdf}}
\caption[Fake factors for electrons and muons with a function of lepton \pt in multi-jet MC samples.]{
Fake factors for electrons (a) and muons (b) calculated using multi-jet MC samples.
The error bars and the red boxes represent the statistical and systematic uncertainty, respectively.
The systematic uncertainty is based on the fake composition modeling.
}
\label{fig:bg:FF_1L}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

In the ID region (= the 1L category), multi-jet events are estimated using the fake factor from the corresponding anti-ID region.
Multi-jet backgrounds in the anti-ID region are evaluated from the data after subtracting the contribution of non-multi-jet backgrounds and normalized to be consistent with the data.
A flat 20\% uncertainty of the normalization factor is assigned to account for the typical level of uncertainty of the subtracted backgrounds.

The estimated yields in the 1L category with different selections are summarized in Table~\ref{tab:bg:1l_QCD_result}.
The contributions of multi-jet backgrounds typically account for 3\%-5\% of the total background in 1-electron regions and less than 1\% in 1-muon regions.
Therefore, the contribution of multi-jet background is negligible in 1-lepton regions.

\input{BackgroundEstimation/table/FF_1L_result}

\section{1Y category}

Multi-jet events containing ``fake photons'' remain in the 1Y category.
The contribution of multi-jet backgrounds is estimated using an ABCD method.
Region-D is the target region, and the definitions are,
\begin{itemize}
\item \textbf{A:} pass loose identification criteria, fail tight identification criteria, and fail isolation criteria
\item \textbf{B:} pass loose identification criteria, fail tight identification criteria, and pass isolation criteria
\item \textbf{C:} pass tight identification criteria, and fail isolation criteria
\item \textbf{D:} pass tight identification criteria, and pass isolation criteria
\end{itemize}
where loose identification criteria are the ``LoosePrime4a'' working point and it is identical to LoosePrime4 in Ref.\cite{EGAM-2018-01}.

We perform the closure test using multi-jet MC samples in a loose 1-photon region.
The ID efficiency and the isolation efficiency with a function of photon \pt are shown in Figure~\ref{fig:bg:1y_eff_ABCD}.
In $\pt < 350~\GeV$, good closure is observed.
However, we can see that there is a discrepancy at $\pt > 350~\GeV$.
The discrepancy causes underestimation, and a flat 50\% closure uncertainty is assigned for this discrepancy.

\begin{figure}[t]
\begin{center}
\subfigure[$\epsilon(\text{ID})$]{\includegraphics[width=0.48\hsize]{BackgroundEstimation/figure/photon_IDEff_jetjet.pdf}}
\subfigure[$\epsilon(\text{Iso})$]{\includegraphics[width=0.48\hsize]{BackgroundEstimation/figure/photon_IsoEff_jetjet.pdf}}
\caption[Efficiency of the ID and the isolation in multi-jet MC samples]{ Efficiency of the tight photon ID selection (a) and the isolation for photons passing the loose ID (b), overlap removal, calculated using the multi-jet MC.
}
\label{fig:bg:1y_eff_ABCD}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The yield of multi-jet events in region-D is calculated as $n(\textrm{region-C})\times n(\textrm{region-B})/n(\textrm{region-A})$.
Data events subtracted from the non-multi-jet MC samples are used as the multi-jet events.
The normalization factor is applied to agree with the observed data in region-D.
Like the estimation in the 1-lepton region, a flat 20\% uncertainty on the normalization factor is added.
The results are summarized in Table~\ref{tab:bg:1y_QCD_result}.
The contributions of multi-jet backgrounds typically account for 2\%-5\% of the total background, and the contribution is enough to be negligible as an independent background component in the fit.

\input{BackgroundEstimation/table/ABCD_1Y_result}
