

import glob,re,os,subprocess


def main():


    devnull = open('/dev/null','w')
    texs = glob.glob('*/*.tex')
    for tex in texs:
        txt = regex('\.tex$').sub('-tex.txt',tex)

        newpath = os.path.join('grammary',txt)
        dirname = os.path.dirname(newpath)
        if not os.path.exists(dirname):
            subprocess.call(['/usr/bin/mkdir', '-p', dirname])

        subprocess.call(['/usr/bin/rsync', '-av', tex, newpath],stdout=devnull)

def regex(expr):

    if 'cached_regex' not in globals():
        cached_regex = dict()
    if expr not in cached_regex:
        cached_regex[expr] = re.compile(expr)
    return cached_regex[expr]



if __name__ == '__main__':
    main()
