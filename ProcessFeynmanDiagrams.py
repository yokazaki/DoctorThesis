#! /usr/bin/env python


import os, glob, subprocess, argparse

def main(args):

    cwd = os.getcwd()
    os.chdir(args['indir'])

    for mpfile in glob.glob(args['pattern']):
        cmd = ['mpost', mpfile]
        print cmd
        subprocess.call(cmd)

    os.chdir(cwd)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = "")
    parser.add_argument('--dir', action = "store", dest = "indir", help = "Directory where metapost files live")
    parser.add_argument('--pat', action = "store", dest = "pattern", help = "Pattern of the metapost file name interpreted by glob")
    args = vars(parser.parse_args())

    print args

    main(args)
