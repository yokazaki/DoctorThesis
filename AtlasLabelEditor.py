#! /usr/bin/env python


import os, re, glob, subprocess, argparse

def delete_atlas_label_pdf2pdf(pdfs):

    uncompressed_pdfs = []
    for pdf in pdfs:
        outpdf = regex('\.pdf$').sub('-pdf-uncompressed.pdf',pdf)
        uncompressed_pdfs.append(outpdf)
        print('Uncompressing PDF: {} => {}'.format(os.path.basename(pdf),os.path.basename(outpdf)))
        subprocess.call(['pdftk', pdf, 'output', outpdf, 'uncompress'])

    delete_atlas_label(uncompressed_pdfs)

    for pdf in uncompressed_pdfs:
        outpdf = regex('-pdf-uncompressed\.pdf$').sub('-pdf-atlaslabel-removed.pdf',pdf)
        print('Recompressing PDF: {} => {}'.format(os.path.basename(pdf),os.path.basename(outpdf)))
        subprocess.call(['pdftk', pdf, 'output', outpdf, 'compress'])


def pdf2eps(pdfs):
    epss = []
    for pdf in pdfs:
        outeps = regex('\.pdf$').sub('-pdf-atlaslabel-removed.eps',pdf)
        subprocess.call(['pdftops', '-eps', pdf, outeps])
        epss.append(outeps)
    return epss


def delete_atlas_label(imgs):
    for img in imgs:
        print('Removing ATLAS labels: {}'.format(img))
        subprocess.call(['sed', '-i', '-e', 's/ATLAS//g', img])
        subprocess.call(['sed', '-i', '-e', 's/Internal//g', img])
        subprocess.call(['sed', '-i', '-e', 's/Work In Progress//g', img])
        subprocess.call(['sed', '-i', '-e', 's/Work in Progress//g', img])

def main(args):

    imgs = []
    if os.path.isfile(args['input']):
        imgs = [args['input']]
    else:
        imgs = glob.glob(os.path.join(args['input'], '*.'+args['input_format']))
    imgs = [ img for img in imgs if not regex('.*(uncompressed|removed|converted-to)\.pdf$').match(img) ]
    if args['input_format'] == 'pdf' and args['output_format'] == 'pdf':
        # pdf2pdf
        delete_atlas_label_pdf2pdf(imgs)
    else:
        # pdf2eps, eps2eps
        if args['input_format'] == 'pdf' and args['output_format'] == 'eps':
            imgs = pdf2eps(imgs)
        delete_atlas_label(imgs)

def regex(expr):
    if 'cached_regex' not in globals():
        global cached_regex
        cached_regex = {}
    if expr not in cached_regex:
        cached_regex.update({expr:re.compile(expr)})
    return cached_regex[expr]


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = "")
    parser.add_argument('--input', action = "store", dest = "input", help = "Path of either the input file or the directory for input files")
    parser.add_argument('--ifmt', action = "store", dest = "input_format", help = "Input file format")
    parser.add_argument('--ofmt', action = "store", dest = "output_format", help = "Output file format")
    args = vars(parser.parse_args())


    main(args)



