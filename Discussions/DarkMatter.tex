As discussed in Section~\ref{sec:intro:darkmatter}, the lightest neutralino can be a good candidate for the dark matter.
Figure~\ref{fig:dis:relicdensity_LSPmass} shows the dark matter density with \bino/\wino/\hino-like dark matter as a function of $m(\None)$.
If other particles can also be dark matter components, the expected dark matter density of \bino/\wino/\hino will be small.




\subsection{\bino-like Dark Matter}

In this thesis, the SUSY models with a large mass difference between \heavyino and \lightino are targeted, and the other SUSY particles, such as sleptons and BSM Higgs bosons, are decoupled.
Thus, only the $Z$-/$h$-funnel models~\cite{Hamaguchi:2015rxa} as the \bino-dominant dark matter scenario can explain the dark matter density ($\Omega_{\None}h^2 = 0.12$).
In these models, $m(\None)\sim m(Z)/2,~m(h)/2$.
%\paragraph*{$Z$-/$h$-funnel}
For the $Z$-/$h$-funnel models, the density of \bino is determined by the coupling between the dark matter (LSP) and $Z/h$ bosons.
The coupling depends on the higgsino mass parameter $\mu$ and the ratio of the Higgs vacuum expectation values $\tanb$ (more details described in Appendix~\ref{app:dm_zh_couple}).
Therefore, the upper limit for $\mu$ can be set if the density of \bino is allowed to be smaller than the observed dark matter density.

Then, the \hinoBino model\footnote{In the \winoBino model, we need to introduce an inconsistent assumption for this analysis. To explain the observed dark matter density, the higgsino mass parameter $\mu$ is required to be not very large, even though higgsino is assumed to be decoupled. Thus, the \winoBino model motivated by the dark matter is not considered here.} is considered.
Figure~\ref{fig:dis:hinoBinodarkmatter} shows the constraint from this analysis.
From the results in this analysis, the $m(\Ntwo)$ between $500\GeV$ and $900\GeV$ are excluded regardless of \tanb and the sign of $\mu$.
Thus, in the \hinoBino scenario motivated by the dark matter, $5.5 < \tanb < 7$ is excluded in the $h$-funnel case with $\mu < 0$ and $\tanb > 8.5$ is excluded in the $h$-funnel case with $\mu >0$.
In these regions, the XENON-1T experiment is insensitive (more detail is described in Appendix~\ref{app:dm_zh_couple}).
While the indirect dark matter searches, such as Fermi-LAT~\cite{Ackermann_2015} and AMS-02~\cite{Cuoco_2018}, provide the limits on the dark matter annihilation cross-sections times the velocity for $\None\None\ra b\bar{b}$ or $\tau\bar{\tau}$, they do not provide the significant constraint for the $Z$-/$h$-funnel models since the upper limit is at most $\mathcal{O}(10^{-28})~\textrm{cm}^{3}\textrm{s}^{-1}$ and enough smaller than the expected cross-sections times the velocity of \bino~\cite{Hamaguchi:2015rxa}.
Other constraints discussed in Ref.\cite{Hamaguchi:2015rxa} do not provide effective exclusion limits.
Therefore, this analysis provides the most stringent limit on the \hinoBino scenario motivated by the dark matter.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[$Z$ funnel]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/combined_Z_funnel.pdf}}
  \subfigure[$h$ funnel]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/combined_h_funnel.pdf}}
  \caption[Exclusion of dark matter in \hinoBino model]{
Expected (dashed green band) and observed (hashed green band) limits at the 95\% CL on the \hinoBino model as a function of \tanb and $m(\Ntwo)$, when the mass of \bino-like LSP (\None) being (a) half of the $Z$-mass~($42.6~\GeV$), or (b) half of the $h$-mass~($62.5~\GeV$) so that the LSP dark matter can annihilate via $Z/h$ resonance~\cite{ATLAS_EW_FullHad}.
The areas surrounded by the bands represent the excluded range of \Ntwo.
The limits are assumed to be constant along \tanb given the small dependency on $\mathcal{B}(\Ntwo \ra Z\None)$ seen in Figure~\ref{fig:result:Bino-LSP}.
The overlaid red solid (blue dashed) line indicates \Ntwo that reproduces the observed dark matter relic density, $\Omega h^2=0.12$, with $\mu > 0$ ($\mu < 0$)~\cite{Hamaguchi:2015rxa}.
%, below (above) which the dark matter relic density is under-abundant (over-abundant).
}
   \label{fig:dis:hinoBinodarkmatter}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{\wino-/\hino-like Dark Matter}

In the light \wino-LSP or \hino-LSP cases, the lightest chargino degenerates.
Such case  with $m(\Cone) < 103\GeV$ is already excluded by LEP~\cite{LEP_SUSY}.
Considering the heavy \wino-LSP or \hino-LSP cases, the lightest neutralino mass $m(\None)$ is favored to be 3 (1)~TeV, as shown in Figure~\ref{fig:dis:relicdensity_LSPmass}.
However, these heavy LSPs are not considered in this analysis, but the models where \wino or \hino is the LSP and one of the dark matter components.
%In these cases, our results can be compared with other results, such as the direct/indirect dark matter searches and \wino-LSP or \hino-LSP searches in ATLAS/CMS with a small mass difference between \Cone and \None.
To study these dark matter models, the phenomelogical MSSM parameters are scanned over using libraries, such as SOFTSUSY~\cite{Allanach_2002}, SPheno~\cite{Porod_2012}, FeynHiggs~\cite{bahl2019precision}, GM2Calc~\cite{Athron_2016} and micrOMEGAs~\cite{B_langer_2018}.
The ranges for scan of the parameters are summarized in Table~\ref{tab:dis:pMSSM_scan}.
In this thesis, the \winoHino and \hinoWino models are considered.
Then, the ranges of scan for these models are considered, and the results are shown in the \hino and \wino mass parameters plane ($\mu$, $M_{2}$).
In this study, the positive \wino mass ($M_{2} > 0$) and the positive/negative \hino mass case is only considered.
The reason why the negative $M_{2}$ case is not considered is that the transformation that $M_{2} \ra -M_{2}$ corresponds to the conversion of $\mu \ra -\mu$, i.e., the sign of $M_{2}\times\mu$ is important.



\input{Discussions/tables/pMSSM_Scan}


Some MSSM parameters, such as the mass parameters for superpartners, are constrained by the ATLAS and CMS analyses.
However, these constraints depend on the LSP type; in most cases, \bino-LSP is considered.
Thus, the exclusion limits can not apply to this study for the \wino-LSP or \hino-LSP cases.
For example, the searches for stop, as shown in Figure~\ref{fig:inro:stop_ATLAS} target the decay process of $\tilde{t}\ra X\None$, however, in the \wino-LSP or \hino-LSP cases, $\tilde{t}\ra X\Cone$ process is allowed.
Then, the exclusion limits become smaller.

Thus, the following selections for the dark matter motivated scenario in the \winoHino or \hinoWino case are imposed:
\begin{itemize}
\item Selections for the \winoHino and \hinoWino models:
\begin{itemize}
\item $|M_{2}|,|\mu| < |M_{1}|$ (\bino is decoupled)
\item $\mathcal{B}(\wino \ra W/Z/h+\hino) \sim 100\%$ or $\mathcal{B}(\hino \ra W/Z/h+\wino) \sim 100\%$
\end{itemize}
\item Passing the loose constraint on the observed dark matter density: $\Omega_{M}h^{2} < 0.14$ 
\item Passing the constraint on the Higgs mass: $124\GeV < m_{h} < 128\GeV$
\item Passing the constraints from other analyses
\begin{itemize}
\item XENON-1T~\cite{Aprile_2018,Aprile_2019} (direct dark matter search)
\item Fermi-LAT~\cite{Ackermann_2014} and AMS-02~\cite{Cuoco_2018} (indirect dark matter search)
\item The searches for electroweakinos with a small mass difference between \Cone (\Ntwo) and \None~\cite{SUSY-2016-06,SUSY-2018-16,CMS-SUS-16-048}
\item The combined LEP search~\cite{LEP_SUSY} ($m(\Cone) > 103\GeV$)
\item $2.69\times 10^{-4} < \mathcal{B}(b\ra s\gamma) < 3.87\times 10^{-4}$ ($\pm 2\sigma$ of theoretical prediction~\cite{Misiak_2007} and experimental measurements~\cite{Amhis_2021})
\item $1.6\times 10^{-9} < \mathcal{B}(B_{s}\ra\mu^{+}\mu^{-}) < 4.2\times 10^{-9}$ ($\pm 2\sigma$ of LHCb~\cite{Aaij_2017} and CMS~\cite{CMS-BPH-16-004} measurements)
\item $6.6\times 10^{-5} < \mathcal{B}(B^{+}\ra\tau^{+}\nu_{\tau}) < 16.1\times 10^{-5}$ ($\pm2\sigma$ of experimental results~\cite{rosner2016leptonic} and theoretical prediction~\cite{Charles_2005})
\end{itemize}
\end{itemize}
%s+gamma and tau+nu <- H+/-
%mumu <- A or H
The result of the scan is shown in Figure~\ref{fig:dis:WH_HW_DM}.
$|M_{2}-\mu| < 90\GeV$ is not considered in this thesis.
In large $(\mu-M_{2})$ region, i.e., the target phase space in this analysis, \wino-like \Cone and \None degenerate.
Thus, the search for a small mass difference between \Cone and \None in ATLAS ~\cite{SUSY-2016-06} provides a stringent limit.
However, if \hino is the LSP, this analysis provides the most powerful sensitivity than other experiments.
That is because the direct and indirect searches have low sensitivities and provide the constraints of interactions of whole the dark matter, not one component, such as \None.
ATLAS and CMS have poor sensitivities in a mass difference between \Cone and \None ($ >2\GeV$).
The asymmetry on the sign of $\mu$ is caused by a difference in the dark matter annihilation cross-sections.
%Compressed search provides the stringent limit in $(m(\Ntwo),~\Delta m(\Ntwo,~\None))$ with light \Ntwo mass ($< 200\GeV$)
This analysis provides the most stringent limits for the scenario motivated by the dark matter with \hino-like LSP ($\mu > 150\GeV$).

%-------------------------
\begin{figure}[h]
 \centering
  %\includegraphics[width=0.65\columnwidth]{Discussions/figure/DarkMatter_hinowino.pdf}
  \includegraphics[width=0.65\columnwidth]{Discussions/figure/winohino_DM_add_line.pdf}
  \caption[The \winoHino and \hinoWino model motivated by the dark matter.]{
Reults of pMSSM scan in the \winoHino and \hinoWino model motivated by the dark matter in  ($\mu,~M_{2}$).
The $z$-axis represents the fraction of relic density.
The red lines represent the observed exclusion limits of this analysis ($\tanb = 30$) at the 95\% CL.
The gray lines represent $M_{2} = |\mu|$.
}
\label{fig:dis:WH_HW_DM}
\end{figure}
%-------------------------

