
As discussed in Section~\ref{sec:intro:muon_g-2_anomaly}, the discrepancy can be explained using the loop of electroweakinos and sleptons.
Discrepancy of the muon g-2 at $4.2\sigma$ level is reported by the Fermilab group~\cite{Abi_2021}:
\begin{equation}
\Delta a_{\mu} = a_{\mu}^{\textrm{expm}}-a_{\mu}^{\textrm{SM}} = (25.1\pm 5.9)\times 10^{-10}.
\end{equation}
A contribution of $\Delta a_{\mu}$ by the SUSY particles ($\Delta a_{\mu}^{\textrm{SUSY}}$) is proportional to \tanb.
Thus, if \tanb is small, electroweakinos are required to be light to explain the muon g-2 anomaly.
The light electroweakino cases have already been excluded by the previous analyses with multi-lepton final states.
Thus, the heavy electroweakino cases, i.e., with large \tanb are considered in this thesis.

In order to evaluate the contribution by the SUSY particles in the target models of this thesis, $\Delta a_{\mu}^{\textrm{SUSY}}$ is calculated with various \bino/\wino/\hino mass parameter ($M_{1},~M_{2},~\mu$), and the fixed smuon mass, \tanb.
The settings of calculations are,
\begin{itemize}
\item $\tanb = 60$
\item $m_{\tilde{\mu}_{L}} = 700\GeV$
\item $m_{\tilde{\mu}_{R}} = \left\{
\begin{array}{ll}
701\GeV & (\textrm{light}~\tilde{\mu}_{R}~\textrm{model}) \\
3000\GeV & (\textrm{heavy}~\tilde{\mu}_{R}~\textrm{model})
\end{array}
\right.$
\item The heaviest of $(M_{1},~M_{2},~\mu)$ is assumed to be $3000~\GeV$.
\end{itemize}
The settings are determined by the typical upper limit of \tanb and passing the constraints on the smuon search~\cite{SUSY-2018-32}.

The results with the light or heavy $\tilde{\mu}_{R}$ models are shown in Figure~\ref{fig:dis:g_2calc_Lonly} and Figure~\ref{fig:dis:g_2calc_LR}. 
Figure~\ref{fig:dis:g_2calc_Lonly} shows the results in the heavy $\tilde{\mu}_{R}$ model, where the contributions of right-handed smuons are assumed to be negligible.
Figure~\ref{fig:dis:g_2calc_LR} shows the results in the light $\tilde{\mu}_{R}$ model.
The red lines represent the observed exclusion limits of this analysis.
%We know that there is a small dependency on exclusion, regardless of the branching ratios, the sign of $\mu$, and \tanb.
%Due to the assumption that $m_{\tilde{\mu}_{L}} = 700~\GeV$ in the calculations, the exclusion limits of this analysis in \heavyino > $700~\GeV$ are not correct because some decay process via $\tilde{\mu}$, such as $\Cone \ra \tilde{\mu}\nu \ra \mu\nu\None$ in \winoBino, can be allowed.



%-------------------------
\begin{figure}[t]
 \centering
  \includegraphics[width=0.65\columnwidth]{Discussions/figure/smuon_limits.pdf}
  \caption[Exclusion limits of smuon at the 95\% CL.]{
Exclusion limits of smuon at the 95\% CL~\cite{ATL-PHYS-PUB-2021-019}.
The hatched bands correspond to the observed muon g-2 anomaly~\cite{Abi_2021}.
The muon g-2 bands are calculated using the GM2Calc~\cite{Athron_2016} and SPheno~\cite{Porod_2012} packages.
}
\label{fig:dis:smuon}
\end{figure}
%-------------------------



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[\winoBino \label{fig:dis:g_2calc_Lonly_WB}]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mH_3000_WB.pdf}}
  \subfigure[\hinoBino]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mW_3000_HB.pdf}}
  \subfigure[\winoHino, \hinoWino]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mB_3000_WH_HW.pdf}}
  \caption[$\Delta a_{\mu}^{\textrm{SUSY}}$ with heavy right-handed smuon.]{
$\Delta a_{\mu}^{\textrm{SUSY}}$ with heavy right-handed smuon in the \winoBino (a), \hinoBino (b), \winoHino and \hinoWino (c) models.
The black solid line represents $\Delta a_{\mu}^{\textrm{SUSY}} = 2.5\times 10^{-9}$, and the dashed lines represent $1\sigma$ band.
The red lines are the observed exclusion limits of the \winoBino model with $\mathcal{B}(\Ntwo \ra Z\None) = 100\%$ (a), the \hinoBino model with $\mathcal{B}(\Ntwo \ra Z\None) = \mathcal{B}(\Nthr \ra h\None) = 100\%$ (b), and the \winoHino and \hinoWino models with $\tanb = 30$ and $\mu > 0$ (c) at the 95\% CL in this analysis.
}
   \label{fig:dis:g_2calc_Lonly}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[\winoBino]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mH_3000_WB_addR.pdf}}
  \subfigure[\hinoBino]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mW_3000_HB_addR.pdf}}
  \subfigure[\winoHino, \hinoWino]{\includegraphics[width=0.45\columnwidth]{Discussions/figure/tanb60_mL700_mB_3000_WH_HW_addR.pdf}}
  \caption[$\Delta a_{\mu}^{\textrm{SUSY}}$ with $m_{\tilde{\mu}_{L}} \sim m_{\tilde{\mu}_{R}}$.]{
$\Delta a_{\mu}^{\textrm{SUSY}}$ with $m_{\tilde{\mu}_{L}} \sim m_{\tilde{\mu}_{R}}$ in the \winoBino (a), \hinoBino (b), \winoHino and \hinoWino (c) models.
The black solid line represents $\Delta a_{\mu}^{\textrm{SUSY}} = 2.5\times 10^{-9}$, and the dashed lines represent $1\sigma$ band.
The red lines are the observed exclusion limits of the \winoBino model with $\mathcal{B}(\Ntwo \ra Z\None) = 100\%$ (a), the \hinoBino model with $\mathcal{B}(\Ntwo \ra Z\None) = \mathcal{B}(\Nthr \ra h\None) = 100\%$ (b), and the \winoHino and \hinoWino models with $\tanb = 30$ and $\mu > 0$ (c) at the 95\% CL in this analysis.
}
   \label{fig:dis:g_2calc_LR}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the \winoBino and \hinoBino models, $\Delta a_{\mu}^{\textrm{SUSY}}$ is typically smaller than $1.5\times 10^{-9}$ with $\tanb = 60$ and $m_{\tilde{\mu}} \sim 700~\GeV$.
However, $\Delta a_{\mu}^{\textrm{SUSY}}$ with large \tanb (for example, $\tanb=100$) is consistent with the anomaly since $\Delta a_{\mu}^{\textrm{SUSY}}$ strongly depends on \tanb and $m_{\tilde{\mu}}$.
Since the results in this analysis only depend on the mass, constraints on the scenarios motivated by the muon g-2 anomaly can be set if very large \tanb is assumed.
On the other hand, the \winoHino and \hinoWino models are favored by the muon g-2 anomaly.
XENON-1T provides stringent limits on the dark matter mass in the \winoHino model, as discussed in Ref.\cite{iwamoto2021winohiggsino}.
The search for electroweakinos with a small mass difference between \Cone and \None in the ATLAS experiment~\cite{SUSY-2016-06} provides stringent limits for the \hinoWino model, as discussed in Section~\ref{sec:dis:darkmatter}.
The \winoHino and \hinoWino models motivated by the muon g-2 anomaly are constrained by this analysis as well.

\subsection{Light Electroweakino Scenarios motivated by Muon g-2 Anomaly}

In this sub-section, specific parameter space, which passes the constraints by the other analyses and has a similar topology of target models in this thesis, is considered.
The target models in this thesis are required to have a large mass difference ($> 400\GeV$) and light \wino and \hino cases, such as the \winoHino or \hinoWino model is favored by the muon g-2 anomaly, as discussed above.
However, these models have already been excluded by the other analyses~\cite{iwamoto2021winohiggsino,SUSY-2016-06}.
Then, light \bino is also introduced in these models.
If \hino is the lightest or second lightest of (\bino, \wino, \hino), the case is excluded by XENON-1T.
Thus, the heaviest \hino case is considered.
If \wino and \bino do not degenerated, the multi-decay process with different $\Delta m$, such as $\hino\ra V\wino$ and $\hino\ra V\bino$, can be observed and they are not targeted in this thesis.
On the contrary, if \wino and \bino degenerate (typically $< 5\GeV$), the results in this thesis can be interpreted since the difference in observed mass difference between two processes is small, and decay products of \wino (\bino) decaying into \bino (\wino) have low momenta and cannot be reconstructed by kinematic selections in this thesis.
Since \wino-like dark matter case is excluded by XENON-1T, the case where $m(\bino) < m(\wino) < m(\hino)$ and $m(\wino) - m(\bino)$ is typically less than $5\GeV$ is considerd.
%We consider a general case for the scenario motivated by the muon g-2 anomaly with light electroweakinos.
%We assume that $m(\bino)/m(\wino)/m(\hino) < 1$~TeV and \hino is produced in the $pp$-collision.
The mass hierarchies of this case and a similar case such as \hinoWino are shown in Figure~\ref{fig:dis:complex_model}.
In this case, the mass difference between $\Cone$ and $\None$ is larger than the \wino-like LSP case.
The target decay processes in this thesis are illustrated as red lines.
The orange lines represent the decay process with a small mass difference, such as $\Cone$ and $\None$, and the decay products have low momenta.
Thus, they are not observed in this thesis due to kinematic selections.

As the mass differences between \Cone (\Ntwo) and \None are small, similar exclusion limits to the \hinoWino model in this thesis because low \pt leptons and jets, as illustrated in an orange arrow in Figure~\ref{fig:dis:complex_model}, do not pass the lepton and jet selections and the exclusion limits for the \hinoWino model in this thesis have a small dependency on MSSM parameters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
\includegraphics[width=0.6\columnwidth]{Discussions/figure/complex_model.pdf}
\caption[Mass hierarchies of the \hinoWino and $(\tilde{H},~\tilde{W}\textrm{-}\tilde{B})$ models.]{
Mass hierarchies of the \hinoWino (left) and $(\tilde{H},~\tilde{W}\textrm{-}\tilde{B})$ (right) models.
The decay processes shown with the orange arrowsare not considered if $\Delta m$ between \Cone/\Ntwo and \None is small enough.
}
\label{fig:dis:complex_model}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The phenomenological MSSM parameters are scanned over using libraries, such as SOFTSUSY~\cite{Allanach_2002}, SPheno~\cite{Porod_2012}, FeynHiggs~\cite{bahl2019precision}, GM2Calc~\cite{Athron_2016} and micrOMEGAs~\cite{B_langer_2018}.
The scan ranges of the parameters are summarized in Table~\ref{tab:dis:pMSSM_scan_muong2}.
$M_{2}$ and $\mu$ are assumed to be positive because $\Delta a_{\mu}$ is low due to small \tanb if $M_{2} < 0$, and the dominant contribution of the \wino-\hino-$\tilde{\nu}_{\mu}$ is negative value if $M_{2} > 0$ and $\mu < 0$.

\input{Discussions/tables/pMSSM_Scan_muong2}

The following selections are applied:
\begin{itemize}
\item Selections for the this target the \winoHino and \hinoWino models:
\begin{itemize}
\item $|M_{2}|,|M_{1}| < |\mu|$
\item $\mathcal{B}(\hino \ra W/Z/h+\wino/\bino) \sim 100\%$ 
\item $m(\Cone)-m(\None) < 5\GeV,~m(\Ntwo)-m(\None) < 5\GeV$
\end{itemize}
\item Passing the constraint on the observed dark matter density: $\Omega_{M}h^{2} < 0.12$~\cite{WMAP2013,Planck2015}
\item Passing the constraint on the Higgs mass: $124\GeV < m_{h} < 128\GeV$
\item Passing the constraints from other analyses
\begin{itemize}
\item XENON-1T~\cite{Aprile_2018,Aprile_2019} (direct dark matter search)
\item Fermi-LAT~\cite{Ackermann_2014} and AMS-02~\cite{Cuoco_2018} (indirect dark matter search)
\item The searches for electroweakinos with a small mass difference between \Cone (\Ntwo) and \None~\cite{SUSY-2016-06,SUSY-2018-16,CMS-SUS-16-048}
\item The combined LEP search~\cite{LEP_SUSY} ($m(\Cone) > 103\GeV$)
\item $2.69\times 10^{-4} < \mathcal{B}(b\ra s\gamma) < 3.87\times 10^{-4}$ ($\pm 2\sigma$ of theoretical prediction~\cite{Misiak_2007} and experimental measurement~\cite{Amhis_2021})
\item $1.6\times 10^{-9} < \mathcal{B}(B_{s}\ra\mu^{+}\mu^{-}) < 4.2\times 10^{-9}$ ($\pm 2\sigma$ of LHCb~\cite{Aaij_2017} and CMS~\cite{CMS-BPH-16-004} measurements)
\item $6.6\times 10^{-5} < \mathcal{B}(B^{+}\ra\tau^{+}\nu_{\tau}) < 16.1\times 10^{-5}$ ($\pm2\sigma$ of experimental results~\cite{rosner2016leptonic} and theoretical prediction~\cite{Charles_2005})
\end{itemize}
\end{itemize}
The result of the parameter scan is shown in Figure~\ref{fig:dis:WH_g-2_scan}.
The blue points represent the remaining models after the selections described above.
This analysis provides the exclusion limits on this SUSY scenario motivated by the muon g-2 anomaly.

%-------------------------
\begin{figure}[t]
 \centering
  \includegraphics[width=0.65\columnwidth]{Discussions/figure/Muon_g-2_hino_winobino.pdf}
  \caption[Muon g-2 motivated $(\hino,~\wino-\bino)$ model.]{
Results of pMSSM scan in the  $(\hino,~\wino\textrm{-}\bino)$ model motivated by the muon g-2 anomaly.
The red lines represent the observed exclusion limit in this analysis ($\tanb = 30$) at the 95\% CL for the \hinoWino model.
}
\label{fig:dis:WH_g-2_scan}
\end{figure}
%-------------------------

%https://arxiv.org/pdf/1710.06105.pdf
