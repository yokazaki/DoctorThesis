\subsection{Reconstruction and Identification}
\label{app:el_ph_reco_detail}
\paragraph*{Reconstruction}

The reconstruction of electrons and photons is based on similar algorithms.
Detailed procedures are described in Ref.\cite{EGAM-2018-01}, only an outline is presented.

Electrons and photons are reconstructed using the information in the calorimeters and the inner detectors (ID).
In the first step of the reconstruction, topo-clusters are reconstructed from the energy deposits in the electromagnetic (EM) calorimeter, not including the measured energy in the hadronic calorimeters.
However, in the transition region ($1.37 < |\eta| < 1.63$), the energy measured in the presampler and scintillator between the calorimeter cryostats is also added.
These clusters are seeds for the reconstruction.
Only those that satisfy the requirements that the energy greater than 400~MeV and the ratio of the energy to the total cluster energy (including the energy measured in the hadronic calorimeter) are selected for the reconstruction.
The selected clusters are referred to as ``EM topo-clusters.''
In parallel, tracks are reconstructed from the ID tracks and the fixed-size clusters instead of topo-clusters.
The fixed-size clusters are reconstructed by a sliding-window algorithm~\cite{ATL-LARG-PUB-2008-002}.
The size of fixed-size clusters is $3 \time 5$ units in $\eta \times \phi$, and the size of units is $0.025 \times 0.025$ in the $\eta-\phi$ plane and corresponds to the granularity of the second layer in the EM calorimeter.
The fixed-size clusters, which have summed transverse energy greater than $2.5\GeV$, are used only to create regions-of-interest.
The tracks are built from the reconstructed ID tracks and the fixed clusters using a modified pattern recognition algorithm based on a Kalman filter formalism~\cite{FRUHWIRTH1987444}, the global $\chi^{2}$ fit~\cite{Cornelissen_2008}, and a Gaussian sum filter (GSF) algorithm~\cite{ATLAS-CONF-2012-047}.

Additionally, conversion vertices used for reconstructing converted photons are introduced.
Both the ID tracks and tracks reconstructed only in the TRT matching the fixed-size clusters are used as input to reconstruct conversion vertices.
Two types of the converted vertex are used.
One is a conversion vertex associated with two opposite-charged tracks, required to be consistent with a massless particle.
Another is a single-track vertex reconstructed from the TRT tracks.
Additionally, we use the ID tracks and the TRT tracks matching the fixed-size clusters to reconstruct converted photons.
In order to keep the converted photon purity high, the TRT tracks are required to match to electron tracks~\cite{ATLAS-CONF-2011-128}.

%Topo-clusters are reconstructed in the electromagnetic (EM) calorimeter and are selected as ``EM topo-clusters'', which have energy greater than 400~MeV.
%In parallel, tracks are reconstructed from the ID tracks and fixed-size clusters are built using a modified pattern recognition algorithm based on a Kalman filter formalism~\cite{FRUHWIRTH1987444}, the global $\chi^{2}$ fit~\cite{Cornelissen_2008}, and a Gaussian sum filter (GSF) algorithm~\cite{ATLAS-CONF-2012-047}.
%Additionally, we use the ID tracks and the TRT tracks matching the fixed-size clusters to reconstruct converted photons.
%To keep the converted photon purity high, the TRT tracks are required to have a high probability to be electron tracks~\cite{ATLAS-CONF-2011-128}.

After reconstructing the EM topo-clusters, tracks, and conversion vertices, they are used to reconstruct ``superclusters.''
The reconstruction of the supercluster candidates originating from electrons or photons is performed separately.
In the first step, the EM topo-clusters are divided into ``seed clusters'' and ``satellite clusters'' with \ET selections track matching requirements.
The satellite clusters, tracks, and converted vertices that match the seed cluster spatially are added to the seed clusters with an energy greater than thresholds.
They are referred to as ``superclusters.''

The seed cluster candidates of electrons are required to have \ET greater than $1\GeV$ and the matched ID tracks with at least four hits.
For photon candidates, it is required to have a minimum \ET of $1.5\GeV$ without requirements for track and conversion vertex matching.
All EM topo-clusters not satisfying these requirements are treated as satellite clusters.
In the next step, the algorithm to find satellite clusters associated with the seed clusters runs.
A satellite cluster within $\Delta\eta\times\Delta\phi=0.075 \times 0.125$ around the seed cluster barycentre is added to the seed cluster.
In the reconstruction algorithm of electrons, spatial matching requirements are loosed to be $\Delta\eta\times\Delta\phi=0.125 \times 0.300$.
For photon reconstruction, a satellite cluster with the same converted vertices and tracks reconstructed from only ID hits are added to the seed cluster.
In addition, a satellite cluster that has the best matched (electron) tracks associated with the same converted vertices is also added.
At this step, a satellite cluster that has already been added to seed clusters is not used.

Their energy is calculated from the corresponding calorimeter cells in the presampler and the first three LAr calorimeter layers, except in the transition region ($1.4 < |\eta| < 1.6$).
In the transition region, the energy measured by the scintillator between the calorimeter cryostats is also added.

After building the electron and photon superclusters, they are corrected for their energy and position.
Matching the tracks and the converted vertices to the superclusters is performed in the same way as matching the EM topo-clusters and them.
At this level, a supercluster can be both candidates of electron and photon due to performing the building of the electron and photon supercluster candidates individually. 
To solve this problem, the logic shown in Figure~\ref{fig:reco:e_ph_separate} is applied.
%The superclusters identified as ``ambiguous'' in the final classification are determined to be treated in the requirements of each analysis.
The superclusters identified as ``ambiguous'' in the final classification are treated as photons in this thesis.

%-------------------------
\begin{figure}[t]
 \centering
  \includegraphics[width=0.95\columnwidth]{Reconstruction/figure/E_Ph_Separate.pdf}
  \caption[A flowchart in the logic resolving particles reconstructed as both electrons and photons.]{
A flowchart in the logic resolving particles reconstructed as both electrons and photons.
An ‘innermost hit’ represents the nearest hit in the pixel to the beam-line along the track trajectory.
$E/p$ is defined as the ratio of the supercluster energy to the momentum of tracks.
The radial position of the conversion vertex is denoted as $R_{\textrm{conv}}$.
$R_{\textrm{firstHit}}$ is the smallest radial position of a hit in the track or tracks that make a conversion vertex~\cite{EGAM-2018-01}.
}
  \label{fig:reco:e_ph_separate}
\end{figure}
%-------------------------

%\subsubsection{Identification of Electrons and Photons}

%Electron and photon identifications use variables summarized in Table~\ref{tab:reco:eph_input}.
%The outlines are presented below.

\input{Reconstruction/table/EPh_input}

\paragraph*{Electron Identification}

The discriminating variables, as shown in Table~\ref{tab:reco:eph_input}, are used to distinguish promptly isolated electrons from hadronic jets, converted photons, and true electrons from heavy-flavor hadrons.
%The variables can be categorized into groups,
%\begin{itemize}
%\item The primary electron tracks
%\item Shapes of the electromagnetic shower in the calorimeter
%\item Hadronic leakage
%\item Matching between the tracks and the clusters
%\end{itemize}
The variables can be categorized into four groups, primary electron tracks, shapes of the electromagnetic shower in the calorimeter, hadronic leakage, and matching between the tracks and the clusters.
These variables are used for the likelihood calculation, probability density functions (pdfs) $P$.
%For the likelihood calculation, probability density functions (pdfs) $P$, are used.
The pdfs are obtained from smoothing histograms for each corresponding discriminating variables with an adaptive kernel density estimator (KDE~\cite{KDE}) as implemented in TVA~\cite{TMVA}.
Additionally, the pdfs for signal and background are calculated separately, and depend on $|\eta|$ and $\ET$.
Thus, likelihoods are obtained individually and defined as,
\begin{equation}
L_{S(B)}(\bm{x}) = \prod_{i=1}^{n}P_{S(B),i}(x_{i}),
\end{equation}
where $P_{S(B),i}(x_{i})$ is the pdf of $i-$th variable for signal (background).
Besides, the likelihood discriminant $d_{L}$ is defined as,
\begin{equation}
d_{L} = \log (L_{S}(\bm{X})/L_{B}(\bm{X})).
\end{equation}
Several working points for the electron identification are defined and called ``Loose,'' ``Medium,'' and ``Tight.''
Their selections for the likelihood discriminant and other variables are different, corresponding to the efficiency.
The efficiencies depend on the number of reconstructed vertices.
Thus, the thresholds of likelihood discriminants are modified as linear functions of pile-up level, maintaining the rejection power of background electrons.


\paragraph*{Photon Identification}

The hadronic leakage and shower shape variables are shown in Table~\ref{tab:reco:eph_input} are used to distinguish between prompt, isolated photons, and backgrounds from hadronic jets.
The variables measured in the EM first layer are useful for rejecting two highly collimated photons from $\pi^{0}$ decays.
Several working points for the photon identification are defined and referred to as ``Loose,'' ``Medium,'' and ``Tight.''
Photons identified by a tight working point pass loose and medium working points.
The Tight identification is optimized by TMVA (multivariate data analysis tool) and the selections depend on \ET.
Additionally, the identification is performed separately for converted and unconverted photons.

\subsection{Isolation Criteria}

In order to reject electrons from heavy hadrons and photons from bremsstrahlung and $\pi_{0}$, the selection called ``Isolation'' is applied.
Two types of isolation variables to keep electrons and photons identification with high quality are used.
The first one is the raw calorimeter isolation variable ($E_{\textrm{T,raw}}^{\textrm{isol}}$).
$E_{\textrm{T,raw}}^{\textrm{isol}}$ is the sum of the transverse energy measured as positive-energy in topo-clusters where the center of energy (COE) of the topo-cluster is inside a cone around the electron or photon cluster COE.
The second one is the fully corrected calorimeter isolation variable ($E_{\textrm{T}}^{\textrm{coneXX}}$).
The definition is,
\begin{equation}
E_{\textrm{T}}^{\textrm{coneXX}} = E_{\textrm{T,raw}}^{\textrm{isolXX}} - E_{\textrm{T,core}} - E_{\textrm{T,leakage}}(\ET,\eta,\Delta R) - E_{\textrm{T,pile-up}}(\eta,\Delta R),
\end{equation}
where XX is the cone parameter which is the distance in the $\eta-\phi$ plane, $\Delta R = \textrm{XX}/100$.
Besides, the track isolation variables ($p_{\textrm{T}}^{\textrm{coneXX}}$) are used.
$p_{\textrm{T}}^{\textrm{coneXX}}$ represents the sum of the transverse momentum measured by tracks inside a cone around the electron or photon cluster direction.
Tracks associated with the electron or converted photon are excluded in the calculation.
For the electron, the track isolation variables with a variable cone parameter ($p_{\textrm{T}}^{\textrm{varconeXX}}$) replace ones of a fixed cone parameter, and the definition is 
\begin{equation}
\Delta R = \textrm{min}\left(\frac{10}{\pt [ \GeV ]}, \Delta R_{\textrm{max}}\right),
\end{equation}
where $\Delta R_{\textrm{max}}$ is the maximum cone parameter.
The track isolation variables with a variable cone parameter are useful to reject other decay products from the heavy particles because the products are close to electrons originating from the same high-momentum heavy particles.
In the calculations, we consider tracks, which have \pt greater than $1\GeV$, $|\eta|<2.5$ and the requirements for hits in the ID.

\paragraph*{Electron Isolation}

Tracks are required that they associate with the primary vertex or satisfy $|\Delta z_{0}\sin{\theta}| < 3$~mm where $|z_{0}\sin{\theta}|$ represents the shortest distance between the muon track and the primary vertex in a longitudinal projection.
``Gradient'' and ``Tight'' working points are used in this thesis, while several working points are defined in Ref.\cite{EGAM-2018-01}.
The ``Gradient'' working point is optimized for efficiency depending on \pt, uniform in $\eta$.
In the ``Gradient'' working point, $E_{\textrm{T}}^{\textrm{cone20}}$ and $p_{\textrm{T}}^{\textrm{varcone20}}$ calculated using tracks with $|\Delta z_{0}\sin{\theta}| < 3$~mm are used.
These cut values are optimized to keep the efficiency defined as $\epsilon = 0.1143\times\pt+92.14$\% and derived from simulated \Jee and \Zee samples.
The ``Tight'' isolation is required to have $E_{\textrm{T}}^{\textrm{cone20}} < 0.06\times\pt$ and $p_{\textrm{T}}^{\textrm{varcone20}} < 0.06\times\pt$.
In this working point, tracks with $|\Delta z_{0}\sin{\theta}| < 3$~mm or loose vertex association are used for the $p_{\textrm{T}}^{\textrm{varcone20}}$ calculation.

\paragraph*{Photon isolation}

All tracks are required to satisfy $|\Delta z_{0}\sin{\theta}| < 3$~mm.
We use only ``Tight'' isolation working points defined in Ref.\cite{EGAM-2018-01}.
Others are not used in this thesis.
Photons are required to have $E_{\textrm{T}}^{\textrm{cone40}} < 0.022\times\ET+2.45\GeV$ and $p_{\textrm{T}}^{\textrm{cone20}} < 0.05\times\ET$.

\subsection{Efficiency Measurement}
\label{sec:reco:e_ph_eff_calib_detail}

The efficiency measurement of electrons and photons is described in Ref.\cite{EGAM-2018-01,PERF-2017-01,PERF-2017-02}.
The outlines are presented in this section.

\paragraph*{Electron efficiency measurement}

The efficiency measurement of isolated electrons is important in the SM measurement and searches for BSM signals.
The measured efficiency includes the effects of the selection described above, such as the reconstruction, the identification, and the isolation, and so on.
The total efficiency is factorized as,
\begin{equation}
\epsilon_{\textrm{total}} = \epsilon_{\textrm{reco}}\times\epsilon_{\textrm{id}}\times\epsilon_{\textrm{iso}}\times\epsilon_{\textrm{trig}}=\left(\frac{N_{\textrm{reco}}}{N_{\textrm{all}}}\right)\times\left(\frac{N_{\textrm{id}}}{N_{\textrm{reco}}}\right)\times\left(\frac{N_{\textrm{iso}}}{N_{\textrm{id}}}\right)\times\left(\frac{N_{\textrm{trig}}}{N_{\textrm{iso}}}\right),
\end{equation}
where $N_{i}$ and $\epsilon_{i}$ are the number and efficiency of $i$, respectively.
$N_{\textrm{all}}$ is the number of produced electrons.
The ``reco,'' ``id,'' ``iso,'' and ``trig'' represent the variables of the reconstructed electrons at the level of the reconstruction, the identification, isolation, and trigger.

The efficiency to reconstruct electrons is extracted from the MC simulation of \Jee and \Zee samples.
Other factors are also determined from the measured efficiency using a tag-and-probe method in \Jee and \Zee control samples of data and simulation.
In the tag-and-probe method, one of the electrons is required to satisfy a strict selection for keeping high quality.
Additionally, the electron is also required to fire the single electron trigger for the data-taking.
The electron is referred to as a ``tag'' electron.
Another electron is treated as a ``probe'' electron and used for calculating efficiency.
Due to requiring no selections for ``probe'' electrons, no bias measurement can be performed.
For correcting differences in the efficiency between data and MC samples in the measurements, the data-to-MC ratios are used as scale factors.


\paragraph*{Photon efficiency measurement}

The efficiency measurement is performed using three control samples, inclusive-photon production samples, radiative photon samples (\Zllg), \Zee samples for electron extrapolation.
Inclusive-photon production samples are collected by single-photon triggers and photons are required to pass ``Loose'' criteria.
The efficiency is extracted by a matrix method using four regions where photons are required to satisfy the ``Tight'' identification and the track-based isolation cuts.
\Zllg samples are collected by single-lepton triggers or di-lepton triggers.
Additionally, the invariant mass of the di-lepton and photon system ($m_{ll\gamma}$) is required to be around the $Z$ mass, and the invariant mass of the di-lepton system ($m_{ll}$) is less than the $Z$ mass.
The additional requirements are useful to reject $Z+\gamma$ samples and $Z+$jets samples, including mis-identified hadronic jets as photons.
The efficiency is extracted from the $m_{ll\gamma}$ distributions.
Finally, the efficiency of the electron extrapolation samples is based on the tag-and-probe method and extracted after the shape variables of the ``probe'' electrons are extrapolated by Smirov transform~\cite{Smirov}.
These measurements are performed in both data and MC samples.
The observed differences are combined to obtain scale factors using a weighted average.


\subsection{Energy Calibration}
\label{sec:reco:e_ph_E_calib}

The energy calibration of electrons and photons is performed in \Zee control samples.
%The energy scale and energy resolution are obtained from the measurement.
The energy scale is defined as a factor ($\alpha_{i}$) to correct the energy in data samples, and the energy resolution applies to simulation samples.
The mis-modeling of the energy resolution is parameterized as a constant term ($c_{i}$), and the noise term is negligible.
Here, $i$ represents different $\eta$ regions.
These corrections are described as,
\begin{eqnarray}
E^{\textrm{data,corr}} &=& E^{\textrm{data}}/(1+\alpha_{i}), \\
\left(\frac{\sigma_{E}}{E}\right)^{\textrm{MC,corr}} &=& \left(\frac{\sigma_{E}}{E}\right)^{\textrm{MC}}\oplus c_{i},
\end{eqnarray}
where $\oplus$ represents a sum in quadrature.
These corrections are applied to the invariant mass of the di-electron system as follows,
\begin{eqnarray}
m_{ij}^{\textrm{data,corr}} &=& m_{ij}^{\textrm{data}}/(1+\alpha_{ij}), \\
\left(\frac{\sigma_{m}}{m}\right)_{ij}^{\textrm{MC,corr}} &=& \left(\frac{\sigma_{m}}{m}\right)_{ij}^{\textrm{MC}}\oplus c_{ij},
\end{eqnarray}
where $i$ and $j$ are the $\eta$ regions of reconstructed electrons, $\alpha_{ij} = (\alpha_{i}+\alpha_{j})/2$ and $c_{ij} = (c_{i}\oplus c_{j})/2$.
The $\alpha_{i}$ and $c_{i}$ are extracted from a simultaneous fit in the $m_{ee}$ distribution.

For the photon energy scale correction, validation in \Zllg control samples is performed.
The residual energy scale factors ($\Delta\alpha$) are extracted from the $m_{ll\gamma}$ distribution.

