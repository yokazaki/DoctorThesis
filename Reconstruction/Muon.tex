\subsection{Reconstruction}
\label{sec:reco:muon_reco_detail}

Muons can reach the outermost detectors, the muon spectrometer (MS), depositing the energy in the inner detector (ID) and the calorimeters.
Muons are reconstructed from the MS and the ID information, described in Ref.\cite{atlascollaboration2020muon}.
The outline is introduced in this section.

Muon reconstruction starts from muon reconstruction tracks in the MS.
The first step of track reconstruction in the MS is to reconstruct local track segments from hits in an individual MS station using a Hough transform.
After finding track segments, they are combined with other segments in different stations to reconstruct track candidates with a loose pointing constraint that muons come from the interaction points and an assumption that a trajectory of a muon is a parabola due to the trajectory bent in the magnetic field.
Finally, the muon trajectory is obtained from the global $\chi^{2}$ fit, considering the effects of interactions in the detector inside the MS and misalignments between the different detector chambers.
The transverse momentum (\pt) is calculated by re-fit with additional considerations for the energy loss in the calorimeters and back-extrapolation to the beamline.

The muon reconstructed tracks in the MS are combined with information of the ID and calorimeters.
There are five reconstruction procedures and corresponding muon types.
However, only two types are considered in this thesis, and they are introduced according to their priority.
Other types are described in Ref.\cite{atlascollaboration2020muon}.

\paragraph*{Combined muons}
Combined muons are obtained from a combined track fit based on the ID and MS hits considering the energy loss in the calorimeters.
Their hits are re-fitted in the combined fit procedures.
%In the forward region ($| \eta | > 2.5$), only the MS tracks and track segments in the pixel and SCT detectors can be used.
%The reconstructed muons combined with only the ID and MS in $| \eta | > 2.5$ are referred to as silicon-associated forward (SiF) muons.

\paragraph*{Inside-out combined muons}
Inside-out combined muons are reconstructed using the extrapolation from the ID tracks to the MS, searching at least three aligned MS hits with a combined track fit.
In this algorithm, the reconstructed MS tracks are not considered.

%\paragraph*{Muon-spectrometer extrapolated muons}
%The reconstruction algorithm of muon-spectrometer extrapolated muons differs from the combined muons and the inside-out combined muons in that the fit parameters are extrapolated to the beamline.
%Due to the difference, the ID tracks are not always necessary and the $\eta$ range where muons can be reconstructed is extended to the outside of the ID.

%\paragraph*{Segment-tagged muons}
%Segment-tagged muons are reconstructed using the extrapolation from the ID tracks to the MS searching at least one MS segment.
%In terms of requiring segments, the segment-tagged muons are different from the inside-out combined muons.

%\paragraph*{Calorimeter-tagged muons}
%Calorimeter-tagged muons reconstructed using the extrapolation from the ID tracks to the calorimeter.
%In the algorithm, energy deposits consistent with a minimum-ionizing particle are searched and the calorimeter information and the ID tracks are fitted using the parameters obtained from the ID tracks.

In order to obtain high quality of the reconstructed muons except for the SiF muons, the ID tracks used for the reconstruction are required following selections,
\begin{itemize}
\item Not less than one hit in the pixel detector
\item Not less than five hits in the SCT detector
\item Not more than two missing hits in the pixel detector and the SCT detector
\end{itemize}
For the SiF muons, at least one pixel hit and at least four hits of the pixel detector and the SCT detector in total are required at the reconstruction step.
Additionally, the following selections at the reconstruction procedures of muons are applied.
\begin{itemize}
\item The number of MS stations where at least three hits in the MDT or CSC detectors are used to reconstruct muons is not more than two (except for $| \eta | < 0.1$).
\item The number of MS stations where at least three hits in the MDT or CSC detectors is one, and muons have at most MS stations where at most two hits are used and at least three hits are missed in the trajectory ($| \eta | < 0.1$).
\item The number of MS stations where at least three hits in the MDT or CSC detectors are used to reconstruct muons is at least three ($2.5 < |\eta| < 2.7$). 
\item $q/p$ significance $= \frac{|q/p_{\textrm{ID}}-q/p_{\textrm{MS}}|}{\sqrt{\sigma^{2}(q/p_{\textrm{ID}})+\sigma^{2}(q/p_{\textrm{MS}})}} < 7$  where $q/p_{\textrm{ID}}$ and $q/p_{\textrm{MS}}$ are the ratio of the charge $q$ to the momentum $p$ in the measurement of the ID and MS detectors, respectively, the corresponding uncertainties are denoted as $\sigma^{2}(q/p_{\textrm{ID}}), \sigma^{2}(q/p_{\textrm{MS}})$.
\end{itemize}
The reconstructed muons satisfying these requirements are called ``medium muons.''

\paragraph*{Isolation}

In order to distinguish between muons from the reconstructed primary vertex and other sources, for example, pile-up interactions, cosmic rays, and hadron decays, kinematic selections called ``Isolation'' are applied.

%Additionally, optional selections can be imposed.
Vertex association requirements to reject muons from pile-up interactions or cosmic rays are introduced.
For ensuring muons originating from the reconstructed primary vertex, the shortest distance between the muon track and the primary vertex in a longitudinal projection ($|z_{0}\sin{\theta}|$) is less than 0.5 mm, where $z_{0}$ is the longitudinal impact parameter defined as the closest point of the muon track to the beamline, measured relative to the reconstructed primary vertex position.
Besides, the other selection for prompt muons is done with ``$d_{0}$ significance,'' defined as $|d_{0}|/\sigma(d_{0})$, where $d_{0}$ is referred to as the transverse impact parameter and represents the shortest distance between a track and the measured beamline position in the transverse plane, and $\sigma(d_{0})$ is the total uncertainty of track fit.
$d_{0}$ significance is required to be less than 3.

The other optional selection is employed to distinguish between muons originating from the primary vertex and ones from hadron decays.
The selection consists of track-based isolation and calorimeter-based isolation. 
The track-based isolation uses the scalar sum of the transverse momenta of the ID tracks associated with the primary vertex in a cone with $\Delta R = \textrm{min}(10~\GeV/\pt, 0.3)$ around the muon in the $\eta-\phi$ plane.
The muon track itself and the ID tracks with $\pt \leq 1$~\GeV are excluded in the sum.
The scalar sum of the transverse momenta of the ID tracks is denoted as $p_{\textrm{T}}^{\textrm{varcone30}}$.
The calorimeter-based isolation uses the sum of the transverse energy of topo-clusters with $\Delta R = 0.2$ around the muon position extrapolated to the calorimeters.
The energy deposit of the muon itself is not included.
The sum of the transverse energy of topo-clusters is denoted as $E_{\textrm{T}}^{\textrm{topocone20}}$.

Two working points for isolation requirements are used.
One is ``Tight'' and another is ``TightTrackOnly.''
``Tight'' working point requires both the track-based isolation and the calorimeter-based isolation.
``TightTrackOnly'' requires only the track-based isolation, which is tighter than one of the ``Tight'' working.
%``Tight'' working point requires both the track-based isolation ($p_{\textrm{T}}^{\textrm{varcone30}} < 0.04\cdot p_{\textrm{T}}^{\mu}$) and the calorimeter-based isolation ($E_{\textrm{T}}^{\textrm{topocone20}} < 0.15\cdot p_{\textrm{T}}^{\mu}$).
%``TightTrackOnly'' requires only the track-based isolation ($p_{\textrm{T}}^{\textrm{varcone30}} < 0.06\cdot p_{\textrm{T}}^{\mu}$).



\subsection{Muon Efficiency Measurement}
\label{app:muon_eff}

The muon efficiency measurement is based on the tag-and-probe method in the samples, including di-muon pairs, such as \Zmm and \Jmm.
In the tag-and-probe method, ``tag muons'' are required to be reconstructed with high quality and fire the online trigger.
``Probe muons'' are required to be identified with the algorithm of interest and have the charge opposite to the tag muons and $|\eta| < 2.5$.

%We use the two control samples corresponding to the \pt of the probe muons.
%For the probe muons with $\pt > 10\GeV$, \Zmm control samples are used.
%In the measurement of \Zmm, the invariant mass of reconstructed $Z$ bosons in the di-muon system is required to be around the $Z$ boson mass.
%The $m_{\mu\mu}$ distribution from \Zmm signals is extracted from the $m_{\mu\mu}$ distribution subtracting the backgrounds, and the efficiency is calculated using the expected yields of the signal events.
%The measured efficiency is factorized with the individual measured efficiencies under different conditions, such as the fraction of MS probes matched to an ID track or the efficiency in the ID and MS.
%Detailed discussions are described in Ref.\cite{atlascollaboration2020muon}.
%Besides, \Jmm control samples are used for the measurement at $3$~\GeV $\leq \pt \leq 20\GeV$.
%Similar measurement with different $m_{\mu\mu}$ window cuts around the \Jpsi mass is performed.
\Zmm control samples ($\pt > 10\GeV$) and \Jmm control samples ($3\GeV\leq\pt\leq20\GeV$) are used.
%In these measurements, $m_{\mu\mu}$ distribution subtracting the backgrounds, and the efficiency is calculated using the expected yields of the signal events.
%The measured efficiency is factorized with the individual measured efficiencies under different conditions, such as the fraction of MS probes matched to an ID track or the efficiency in the ID and MS.
%Detailed discussions are described in Ref.\cite{atlascollaboration2020muon}.
Details are described in Ref.\cite{atlascollaboration2020muon}.
There are disagreements of the measured efficiency between data and MC samples.
In order to correct the difference, the efficiency scale factor (SF) defined as the ratio of the measured efficiency in data samples to MC samples, is used.


To extend the full MS acceptance, we use the double ratio of the data/MC in the forward region ($|\eta| > 2.5$) to near forward region ($2.2 < |\eta| < 2.5$) for the efficiency SF calculation due to the limitation of the ID coverage ($|\eta| < 2.5$).
The measurement is performed in \Zmm control samples and $m_{\mu\mu}$ window cuts are required.
The forward muons are required to have \pt greater than $10$~\GeV and an opposite charge to the central muons.
The central muons are required to be identified with high quality and have $\pt > 25\GeV$.

The results are shown in Figure~\ref{fig:reco:mu_eff}.
In the overlap region of \pt from 10 to $20\GeV$, the measured efficiencies are a good agreement between \Jmm and \Zmm within uncertainties.
The observed discrepancy in $|\eta| > 2.5$ is caused by the different reconstruction algorithms between the SiF muons and combined muons.

%%%%%%%%%%%%% muon efficiency %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[Efficiency in $0.1 < |\eta| < 2.5|$]{\includegraphics[width=0.45\columnwidth]{Reconstruction/figure/Medium_mu_efficiency.pdf}}
  \subfigure[Efficiency as a function of $\eta$]{\includegraphics[width=0.45\columnwidth]{Reconstruction/figure/Medium_mu_efficiency_eta.pdf}}
  \caption[Muon reconstruction and identification efficiencies]{
Muon reconstruction and identification efficiencies in $0.1 < |\eta| < 2.5|$ (a) and as a function of $\eta$ (b)~\cite{atlascollaboration2020muon}.
}
   \label{fig:reco:mu_eff}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\subsection{Momentum Calibration}
\label{sec:reco:mu_Pt_calib}

The ID tracks (MS segments) are corrected by the simulated transverse momenta of reconstructed muons in the ID (MS) sub-detectors.
The corrected transverse momenta, $p_{\textrm{T}}^{\textrm{Cor, Det}}$, is defined as,
\begin{equation}
p_{\textrm{T}}^{\textrm{Cor, Det}} = \frac{p_{\textrm{T}}^{\textrm{MC, Det}}+\sum_{n=0}^{1}s_{n}^{\textrm{Det}}(\eta,\phi)\left(p_{\textrm{T}}^{\textrm{MC, Det}}\right)^{n}}{1+\sum_{m=0}^{2}\Delta r_{m}^{\textrm{Det}}(\eta,\phi)\left(p_{\textrm{T}}^{\textrm{MC, Det}}\right)^{m-1}g_{m}}, ~(\textrm{Det~denotes~ID~or~MS}), 
\end{equation}
where $p_{\textrm{T}}^{\textrm{MC, Det}}$ is the transverse momentum of reconstructed muons in the simulation before the correction, $g_{m}$ is normally distributed random variable for smearing, $\Delta r_{m}^{\textrm{Det}}(\eta,\phi), s_{n}^{\textrm{Det}}(\eta,\phi)$ represent the momentum resolution smearing and the scale correction corresponding to $(\eta,\phi)$ detector region.
$g_{m}$ has zero mean and unit width.
$s_{1}^{\textrm{Det}}$ corrects the inaccuracy of the magnetic field, and $s_{0}^{\textrm{Det}}$ term is the correction factor for the inaccuracy of the energy loss.
We consider the energy loss only in the calorimeter and other materials inside the MS, not including one between the interaction point and the ID.
The denominator represents the momentum smearing and includes the effects of energy loss, multiple scattering, and the fluctuations of hits caused by the misalignment of the MS and spatial resolution.

The correction factors are extracted using a binned maximum-likelihood fit from the invariant mass distributions in \Jmm and \Zmm control samples.
For the MS corrections, \pt imbalance variable, defined as:
\begin{equation}
\rho = \frac{\pt^{\textrm{MS}}-\pt^{\textrm{Cor, ID}}}{\pt^{\textrm{Cor, ID}}},
\end{equation}
are used to correct the energy loss in the calorimeter between the ID and the MS.

