When an SM electroweak boson with high \pT decays hadronically, the opening angle between the daughter quarks becomes small.
This is characterized by the angular distance defined by $\Delta R = \sqrt{\Delta\eta^2+\Delta\phi^2}$ where $\Delta\eta$ ($\Delta\phi$) is spatial distance in pseudo-rapidity (azmuthal angle).
The distance between quarks is typically $0.4$ for a $W$ boson with $\pT = 400\GeV$.
While the most commonly used $R = 0.4$ jets, referred to as ``small-$R$ jets'' (described in Section~\ref{sec:reco:small}) in the document, have difficulty in reconstructing hadronically decaying SM electroweak bosons as a single jet or two separate jets with all decay products, large-radius jets have the advantage to reconstruct the two quarks as a single jet.
$R = 1.0$ is chosen for this analysis referred to as ``large-$R$ jets.''
The large-$R$ jet reconstruction method, including a pile-up mitigation technique, is described in this sub-section.
The boson tagging technique to identify the SM electroweak bosons using large-$R$ jets is introduced in Chapter~\ref{ch:BosonTagging}.
In this analysis, to search for electroweakinos, kinematic selections are applied to select the large-$R$ jets originating from the SM electroweak bosons with high \pt.
The selections are described in Section~\ref{sec:reco:summary}.


\subsubsection{Trimming and Mass}
\label{sec:reco:large_tech}

The large-$R$ jets after the anti-$k_{T}$ algorithm clustering with $R = 1.0$ are referred to as ``ungroomed jets.''
In order to remove the contribution from the underlying event and pile-up, ``trimming''~\cite{Krohn:2009th} is applied to the ungroomed jets.
Any sub-jets clustered by the $k_{t}$ algorithm with $R = 0.2$~\cite{AntiKt} are removed if the \pt is less than the 5\% of that of the ungroomed large-$R$ jet.

The performance of the trimming algorithm against the pile-up is discussed below using the jet mass as an observable.
As discussed later in Chapter~\ref{ch:BosonTagging}, the jet mass is one of the key variables to distinguish between the boson jets and background jets originating from quarks or gluons effectively.
The calorimeter-based jet mass ($m^{\textrm{calo}}$) of a large-$R$ jet is calculated from the topo-cluster energy and defined as:
\begin{equation}
m^{\textrm{calo}} = \sqrt{\left(\sum_{i\in J}E_{i}\right)^{2}-\left(\sum_{i\in J}\vec{p_{i}}\right)^{2}},
\end{equation}
where $E_{i}$ ($\vec{p_{i}}$) is the energy (momentum) of $i$-th cluster.
For sufficiently high Lorentz-boosted massive particles, the angular spread in the decay products is comparable with the granularity of the calorimeter cell segmentation.
The mean uncalibrated jet mass value $\Braket{m^{\textrm{jet}}} = \Braket{m^{\textrm{calo}}}$ is shown in Figure~\ref{fig:reco:trim}.
Good stability of large-$R$ jets against the reconstructed primary vertex multiplicity ($N_{\textrm{PV}}$) is seen after the trimming, while ungroomed jet mass is strongly dependent.


%%%%%%%%%%%%% Trimmed mass %%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
 \centering
  \subfigure[$200$~GeV $< \pt^{\textrm{jet}} < 300$~GeV]{\includegraphics[width=0.45\columnwidth]{Reconstruction/figure/Trimming_jet_mass_pt_200_300.pdf}}
  \subfigure[$600$~GeV $< \pt^{\textrm{jet}} < 800$~GeV]{\includegraphics[width=0.45\columnwidth]{Reconstruction/figure/Trimming_jet_mass_pt_600_800.pdf}}
  \caption[Performance of the jet grooming.]{
Performance of the jet grooming.
The distribution of mean uncalibrated jet mass with a function of the reconstructed vertex multiplicity ($N_{\textrm{PV}}$)~\cite{ATLAS-CONF-2012-066} in \ttbar MC sample is shown.
The black line represents the mean mass of large-$R$ jets without trimming.
The red line represents the default ATLAS option for the trimming parameters.
}
   \label{fig:reco:trim}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%\subsubsection{Large-$R$ Jet Mass Definition}
%\label{sec:reco:large_mass}
% because the spread scales as 1/\pt of particles.
From here on, how to improve the resolution of the jet mass is discussed.
In order to avoid the limitation on the granularity of the calorimeter cell, information from the associated tracks are additionally employed.
Track-assisted mass, $m^{\textrm{TA}}$, is defined as:
\begin{equation}
m^{\textrm{TA}} = \frac{\pt^{\textrm{calo}}}{\pt^{\textrm{track}}}\times m^{\textrm{track}},
\end{equation}
where $\pt^{\textrm{calo}}$ is a transverse momentum of a large-$R$ jet calculated by calorimeter-cell cluster constituents, $\pt^{\textrm{track}}$ is a transverse momentum of the four-vector sum of tracks associated with the large-$R$ jet, and $m^{\textrm{track}}$ is an invariant mass of the four-vector sum of tracks, which the pion mass is assigned for each track.
Since $\pt^{\textrm{track}}$ is calculated using only charged particles and $\pt^{\textrm{calo}}$ is calculated by charged and neutral particles, the ratio of $\pt^{\textrm{calo}}$ to $\pt^{\textrm{track}}$ is used to correct charged-to-neutral fluctuations and improve the jet mass resolution with respect to a track-only jet mass definition ($m^{\textrm{track}}$) as shown in Figure~\ref{fig:reco:large_mass}.



%-------------------------
\begin{figure}[ht]
 \centering
  \includegraphics[width=0.55\columnwidth]{Reconstruction/figure/large_jet_mass_comp.pdf}
  \caption[Mass distribution of uncalibrated and calibrated jets originating from $W/Z$ bosons.]{
Mass distribution of uncalibrated (before jet mass calibration, represented as dashed line) and calibrated (after jet mass calibration, represented as solid line) jets originating from $W/Z$ bosons~\cite{ATLAS-CONF-2016-035}.
The red line represents calorimeter-based jet mass ($m^{\textrm{calo}}$), the blue one represents track-assisted jet mass ($m^{\textrm{TA}}$) and the green represents the invariant mass of the four-vector sum of tracks associated with the large-$R$ jet $m^{\textrm{track}}$.
}
  \label{fig:reco:large_mass}
\end{figure}
%-------------------------

Figure~\ref{fig:reco:comb_mass_resolution} shows the resolution of jet mass response defined as the ratio of the reconstructed jet mass to the truth jet mass as a function of truth jet from $W/Z$ bosons.
Half of the 68\% interquartile range (IQnR)\footnote{This is defined as the difference between the $16^{\textrm{th}}$ and $84^{\textrm{th}}$ percentiles of a given distribution.} divided by the median are used to represent the resolution.
Thanks to the spatial resolution of the constituents using the track information, the resolution of $m^{\textrm{TA}}$ is better than $m^{\textrm{calo}}$ in the high \pt region ($\pt > 1\TeV$).
However, since $\pt^{\textrm{track}}$ is only based on charged particles and $m^{\textrm{calo}}$, $\pt^{\textrm{calo}}$ are calculated by charged and neutral particles, the resolution of $m^{\textrm{calo}}$ is better in the low \pt region.

%-------------------------
\begin{figure}[ht]
 \centering
  \includegraphics[width=0.55\columnwidth]{Reconstruction/figure/comb_mass_resolution.pdf}
  \caption[The resolution of the jet mass response as a function of truth jet \pt.]{
The resolution of the jet mass response as a function of truth jet \pt originating from $W/Z$ bosons for calorimeter-based jet mass (red dashed line), track-assisted jet mass (blue solid line), and the combined jet mass (black dotted line)~\cite{JETM-2018-02}.
}
  \label{fig:reco:comb_mass_resolution}
\end{figure}
%-------------------------


To exploit the merit of both $m^{\textrm{calo}}$ and $m^{\textrm{TA}}$, a statistical combination of the masses are considered, referred to as ``combined mass'' ($m^{\textrm{comb}}$):
\begin{eqnarray}
m^{\textrm{comb}} &=& a \times m^{\textrm{calo}} + b \times m^{\textrm{TA}}, \\
a &=& \frac{\sigma_{\textrm{calo}}^{-2}}{\sigma_{\textrm{calo}}^{-2}+\sigma_{\textrm{TA}}^{-2}}, \\
b &=& \frac{\sigma_{\textrm{TA}}^{-2}}{\sigma_{\textrm{calo}}^{-2}+\sigma_{\textrm{TA}}^{-2}},
\end{eqnarray}
where $\sigma_{\textrm{calo}}$ ($\sigma_{\textrm{TA}}$) is the parametrized $m^{\textrm{calo}}$ ($m^{\textrm{TA}}$) resolution obtained from the jet mass response distribution, using the central 68\% inter-quantile range of the jet mass response distribution.
As shown in Figure~\ref{fig:reco:comb_mass_resolution}, \mcomb outperfoms both $m^{\textrm{calo}}$ and $m^{\textrm{TA}}$ in the resolution across the \pt regions.

In this thesis, large-$R$ jet mass refers to \mcomb.

\clearpage


\subsubsection{Energy and Mass Calibration}
\label{sec:reco:largeR_calib}

Since the calorimeter in the ATLAS detector is categorized into sampling calorimeters, only some of the energy is measured.
Thus, the measured energy of jets needs to be scaled.
The scale factor is called ``jet energy scale'' (JES), and the method to obtain the scale factor is called ``scale calibration.''
The calibration is composed of two steps; one is an MC-based calibration to obtain the JES, and the other is ``in-situ calibration'' to correct the difference between the data and MC samples using a data-driven method.
The jet mass needs to be corrected for residual bias between the data and MC samples to be applied while the jet is calibrated.
Like the jet energy, the scale factor for the jet mass (JMS) is obtained by the MC-based calibration and in-situ calibration.
In the in-situ calibration, the jet energy resolution (JER) and the jet mass  resolution (JMR) are measured, and their differences between the data and MC samples are evaluated as the correction for the MC samples.

The workflow of large-$R$ jet calibration, together with the reconstruction, is summarized in Figure~\ref{fig:reco:large_calib}.

%-------------------------
\begin{figure}[t]
 \centering
  \includegraphics[width=0.85\columnwidth]{Reconstruction/figure/LargeR_calib_seq.pdf}
\caption{
Summary of large-$R$ jet reconstruction and calibration procedure~\cite{JETM-2018-02}.
}
  \label{fig:reco:large_calib}
\end{figure}
%-------------------------

\paragraph*{MC-based Calibration}
At the MC-based calibration step, the JES for large-$R$ jets is evaluated as a ratio of their energy to the truth jets.
The average jet energy response of reconstructed jets to truth jets ($E_{\textrm{reco}}/E_{\textrm{truth}}$) is parametrized as the function of truth jet energy and $\eta$.
After the JES correction based on MC simulation, a similar procedure applies to the JMS calibration using the jet mass response of reconstructed jets to the truth.
The JMS is derived using the MC samples with the \pt fixed to the post-JES calibration values.
The JMS is computed for $m^{\textrm{calo}}$ and $m^{\textrm{TA}}$ independently and depends on the jet \pt, mass, and $\eta$.
The \mcomb is derived using the same coefficients.

\paragraph*{In-situ Calibration}

After the MC-based calibration, the difference in the jet energy between the data (reconstructed jets) and MC samples (truth jets) still remains.
%For jet energy scale calibration, we use ``in-situ methods'' similar to the one used for small-$R$ jets.
In order to reduce the residual difference, data-driven measurements are performed using samples of jets balanced with well-calibrated objects such as photons, leptons, and small-$R$ jets~\cite{JETM-2018-02}.
%The calibration procedure is described in Ref.\cite{JETM-2018-02}, the outline is introduced in this section.
%We also use three processes according to the \pt of target jets and measure each \pT balance of jets with respect to reference objects of the data and MC.
Three processes, \Zjets, \gammajets, and multi-jets, are used depending on the \pt of target jets.
In these processes, each \pT balance of jets with respect to well-calibrated reference objects or systems of the data and MC samples, such as $Z$ bosons reconstructed by lepton decays, $\gamma$, and low-\pt jets system is measured.
The reference in multi-jets events is $\pt^{\textrm{recoil}}$ obtained as the four-vector sum of calibrated small-$R$ jets.
%\Zjets and \gammajets events are used for calibrating large-$R$ jets that have $\pt \geq 200$~\GeV and $\arrowvert\eta\arrowvert < 0.8$.
%Additionally, multi-jets events are used to calibrate higher \pt large-$R$ jets ($\pt \geq 300$~\GeV and $\arrowvert\eta\arrowvert < 0.8$).
%The reference in multi-jets events is $\pt^{\textrm{recoil}}$ obtained as the four-vector sum of calibrated small-R jets.
%At least 3 small-$R$ jets are required for the measurement.
To apply the calibration for the forward jets ($0.8 < \arrowvert\eta\arrowvert < 2.5$), we also measure the ratio of two jets using di-jet topology.
In this measurement, one each jet in the central region ($|\eta| < 0.8$) and in the forward region ($0.8 \leq |\eta| \leq 2.5$) is required.
We measure the momentum asymmetry:
\begin{equation}
\mathcal{A}=\frac{\pt^{\textrm{left}}-\pt^{\textrm{right}}}{\pt^{\textrm{avg}}},
\end{equation}
where ``left'' and ``right'' are denoted for simplicity; $\pt^{\textrm{avg}}=(\pt^{\textrm{left}}+\pt^{\textrm{right}})/2$.
The calibration factor $c$ for each jet is described as,
\begin{equation}
\mathcal{R}=\frac{c^{\textrm{left}}}{c^{\textrm{right}}}=\frac{2+\Braket{\mathcal{A}}}{2-\Braket{\mathcal{A}}}=\frac{\pt^{\textrm{left}}}{\pt^{\textrm{right}}}.
\end{equation}
%Thus, the \pt response is measured to obtain the calibration factor.
The final result of these measurements is shown in Figure~\ref{fig:sys:large_jes}.
The difference between the data and MC samples is smaller than 3\%, and the statistical uncertainty is dominant in $\pt \geq 1.5$~TeV.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
\subfigure[in situ method for the central jets]{\includegraphics[width=0.48\textwidth, height=0.27\textheight]{Systematics/figure/large_jes_ratio}}
\subfigure[$\eta$ inter-calibration]{\includegraphics[width=0.48\textwidth, height=0.27\textheight]{Systematics/figure/large_jes_ratio_eta}}
\caption[Large-$R$ jets response by in-situ calibration.]{
Large-$R$ jets response by in-situ calibration.
(a) The result is combined with three measurements using \Zjets (dielectron channel, upward-pointing triangles; and dimuon channel, downward-pointing triangles), \gammajets ((open squares), and multi-jets (open circles) events.
The response defined as $R = \Braket{\pt^{\textrm{jet}}/\pt^{\textrm{reference}}}$, where the bracket represents the average.
The ratio of the response in the data to that in MC samples as a function of the jet \pt is shown.
The correction factor shown in the black curve is derived by smoothing the measurements and is applied on the jet \pT in the data~\cite{JetETMissPublicResults}.
(b) Relative response of jets in the data (black circles) and MC samples (red and blue triangles with difference generators) as a function of $\eta$ in the detector.
The response of large-$R$ jets with $280\GeV \leq \pt \leq 380\GeV$ is shown as an example.
This figure shows the result with the data in 2015 and 2016.
The lower panel shows the ratio of MC samples to the data.
The dashed lines provide reference points for the viewer~\cite{JETM-2018-02}.
}
\label{fig:sys:large_jes}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


$m^{\textrm{calo}}$ and $m^{\textrm{track}}$ are corrected by in-situ calibration after the MC-based calibration.
The detailed procedure is described in Ref.\cite{JETM-2018-02}.
For calibrating the jet mass scale, we measure the average calorimeter-to-tracker response defined as:
\begin{equation}
R_{\textrm{trk}}^{m}=\Braket{\frac{m^{\textrm{calo}}}{m^{\textrm{track}}}},
\end{equation}
where $m^{\textrm{calo}}$ is measured mass using topo-clusters of calorimeter cells and $m^{\textrm{track}}$ is a track jet mass that matches the same jet.
This method is called ``$R_{\textrm{trk}}$ method,'' done by di-jet events.
The results are shown in Figure~\ref{fig:reco:large_jms_Rtrk}.
In this study, three different generators and three different tracking variations for the default generator \PYTHIA 8 are used to estimate systematic uncertainties.
These systematic uncertainties are discussed in Section~\ref{sec:sys:largejets}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[ht]
\centering
\includegraphics[width=0.55\textwidth]{Reconstruction/figure/large_Rtrk_mass}
  \caption[JMS in the $R_{textrm{trk}}$ method as a function of the large-R jet \pt.]{
JMS in the $R_{textrm{trk}}$ method as a function of the large-R jet \pt.
Measurement of $R_{\textrm{trk}}^{m}$ as a function of the large-R jet \pt with large-$R$ jet $m/\pt = 0.2$.
Results using three different generators are shown.
Tracking uncertainty for the default generator \PYTHIA 8 is shown in a the gray band~\cite{JETM-2018-02}.
}
\label{fig:reco:large_jms_Rtrk}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\clearpage
