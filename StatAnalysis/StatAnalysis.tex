
\DeclareInputPath{StatAnalysis}

\chapter{Statistical Analysis}\label{ch:StatAnalysis}

We use the \textsc{HistFitter}~\cite{Baak:2014wma} framework to perform the statistical analysis.
The outline is introduced in this section.

We use the profile likelihood method to search for new phenomena in this analysis.
As described in Sec.\ref{ch:Backgroundestimation}, the reducible backgrounds are estimated using MC samples normalized to the data in control regions.
The normalization factors are determined in a fitting with systematic uncertainties described in Sec.\ref{ch:Systematics}.
In this section, the outline of the profile likelihood method is described.
Additionally, we introduce the method of hypothesis tests for background estimation and signal model tests.

\section{Profile Likelihood}
\label{sec:stat:profile}

Generally, if we use a set of free parameters $\bm{\theta}$ that perform background estimation in $N$ independent regions $E=(E_{0}(\bm{\theta}),...,E_{N}(\bm{\theta}))$, the likelihood can be written as 
\begin{equation}
L(x;\bm{\theta}) = \prod_{j=1}^{N}\frac{(E_{j}(\bm{\theta}))^{x_{j}}}{x_{j}!}e^{-E_{j}(\bm{\theta})},
\end{equation}
where $\bm{x} = (x_{1},...,x_{N})$ is observed events.
It is a simple product of the Poisson distribution.
In a maximum likelihood method, we find a set of $\bm{\theta}$ values that plausibly explains the measurement under the condition of maximizing the likelihood.
The set values of $\bm{\theta}$ are denoted as $\bm{\hat{\theta}}$.
We perform the maximum likelihood approach to fit MC samples to the observed data for estimating the background or signal yields.

In this analysis, reducible backgrounds are normalized by fitting to the data in control regions, i.e. the normalization factor of reducible backgrounds (denoted as $\mu_{B}$) is a parameter $\bm{\theta}$ calculated using the maximum likelihood method.
Additionally, the signal strength is a parameter $\bm{\theta}$ and the most interesting parameter in this analysis.
It is denoted as $\mu_{S}$.
$\mu_{S}$ and $\mu_{B}$ are treated as free parameters in the likelihood formula.

Other parameters included in $\bm{\theta}$ are derived from systematic uncertainties described in Sec.\ref{ch:Systematics}.
They are called ``nuisance parameters.''
From the measurement in the control regions, a Bayesian probability ($\rho(\theta|\tilde{\theta})$) can be obtained.
$\tilde{\theta}$ is the central value of the measurement in the control regions and $\rho(\theta|\tilde{\theta})$ reflects the degree of our belief in what the true value of $\theta$ is.
From the Bayes’ theorem, $\rho(\theta|\tilde{\theta})$ is approximated as follows,
\begin{equation}
\rho(\theta|\tilde{\theta}) = p(\tilde{\theta}|\theta)\times\pi(\theta),
\end{equation}
where $p(\tilde{\theta}|\theta)$ is a ``frequentist'' probability and $\pi(\theta)$ is a Bayesian prior density.
$\pi(\theta)$ can be re-formulated to flat before choosing the distribution of $p(\tilde{\theta}|\theta)$~\cite{ATLAS:1379837}.
Thus, the distribution of $\rho(\theta|\tilde{\theta})$ can be used as the distribution of $p(\tilde{\theta}|\theta)$ directly.
In this analysis, we consider the probability functions of systematic uncertainties as a Gaussian distribution and statistical uncertainty as a Gamma distribution.
The Gaussian probability density function is described as,
\begin{equation}
\rho(\theta|\tilde{\theta}) = p(\tilde{\theta}|\theta) = \frac{1}{\sqrt{2\pi}\sigma}\textrm{exp}\left(-\frac{(\theta-\tilde{\theta})^{2}}{2\sigma^{2}}\right),
\end{equation}
where $\sigma$ is the uncertainty in the measurement.
For the Gamma probability density function,
\begin{equation}
\rho(\theta|N) = p(N|\theta) = \frac{\theta^{N}}{N!}\textrm{exp}(-\theta),
\end{equation}
where $\tilde{\theta} = N$ and $N$ is the number of observed events.
The combined form with all nuisance parameters ($p(\bm{\theta})$) is described as,
\begin{equation}
p(\bm{\theta}) = \prod_{i}p(\theta_{i}|\tilde{\theta}_{i}),
\end{equation}
and works as a constraint term in the likelihood function.

Using $\mu_{S}$, $\mu_{B}$, and $p(\bm{\theta})$, the likelihood function is written as,
\begin{equation}
L(x;(\mu_{S},\mu_{B},\bm{\theta})) = \prod_{j}^{N}\frac{(E_{j}(\mu_{S},\mu_{B},\bm{\theta}))^{x_{j}}}{x_{j}!}e^{-E_{j}(\mu_{S},\mu_{B},\bm{\theta})},
\end{equation}
where the expected number of events in $j$-th region is $E_{j} = \mu_{S}s_{j}(\bm{\theta})+b_{j}(\mu_{B},\bm{\theta})$, $s_{j}$ and $b_{j}$ are the expected number of events of signals and backgrounds.
$b_{j}$ includes the term of irreducible backgrounds which are not related to $\mu_{B}$.

We use the profile likelihood ratio method~\cite{Cowan:2010js} to calculate the significance and upper limit on a cross-section.
In this method, we obtain a set of parameters to maximize the likelihood function with fixed signal strength.
The obtained parameters are denoted as $\bm{\hat{\hat{\theta}}}$.
For a fixed $\mu_{S}$ value, a profile likelihood ratio is defined as,
\begin{equation}
\lambda(\mu_{S})=\frac{L(\mu_{S},\hat{\hat{\mu_{B}}},\bm{\hat{\hat{\theta}}})}{L(\hat{\mu_{S}},\hat{\mu_{B}},\bm{\hat{\theta}})}.
\label{eq:stat:PLF}
\end{equation}
The numerator is the conditional maximum likelihood estimator of $\bm{\theta}$ and a function of $\mu_{S}$.
The denominator is called the unconditional maximum likelihood function.

\section{Hypothesis Tests}
\label{sec:stat:hypotest}

From the definition of $\lambda(\mu_{S})$ described in Eq.\ref{eq:stat:PLF}, if $\lambda(\mu_{S})$ is nearly 1, then there is a good agreement between data and the assumption of certain $\mu_{S}$ value.
The $\mu_{S}$ is converted to be a statistic,
\begin{equation}
q_{\mu_{S}} = -2 \textrm{ln}\lambda(\mu_{S}),
\end{equation}
where $q_{\mu_{S}}$ is treated as the basis of a statistical test and referred to as ``test statistics.''
We use the $p$-value to quantify the level of disagreement between data and the hypothesis, and the definition of the $p$-value is,
\begin{equation}
p_{\mu_{S}}=\int_{q_{\mu_{S},\textrm{obs}}}^{\infty}f(q_{\mu_{S}}|\mu_{S})~dq_{\mu_{S}},
\end{equation}
where $q_{\mu_{S},\textrm{obs}}$ is the statistic value $q_{\mu_{S}}$ of the observed data, $f(q_{\mu_{S}}|\mu)$ is the probability density function of $q_{\mu_{S}}$ with the assumption of the fixed signal strength $\mu_{S}$.
Figure~\ref{fig:stat:p-val} illustrates the relation between the $p$-value and $f(q_{\mu_{S}}|\mu_{S})$ distribution, and the significance $Z$.




To perform the hypothesis tests, we need obtain a $f(q_{\mu_{S}}|\mu)$ function for each $\mu_{S}$ test.
The $f(q_{\mu_{S}}|\mu_S)$ function can be obtained by the pseudo experiments called ``toy experiments'' or an analytic approximation approach.
The data of pseudo experiments are generated with fixed nuisance parameters to the maximum likelihood estimation and the test statistic function is sampled using the data.
The alternative method is using an analytic approximation of $f(q_{\mu_{S}}|\mu)$.
Wald's theorem~\cite{Wald} provides the statistic $q_{\mu_{S}}$ as a function of $\hat{\mu_{S}}$ and the error.
The asymptotic formula uses ``Asimov dataset''~\cite{Cowan:2010js} generated under the condition where the derivatives of the log-likelihood to nuisance parameters are zero.

We set upper limits on cross-sections and other parameters of signals.
Generally, we consider a $\mu_{S}$ value where the median $p$-value is equal to 0.05 as an upper limit because it is equivalent to be the median upper limit on $\mu_{S}$ at the 95\% confidence level.
For setting upper limits or excluding signal models, we use a statistic called CLs.
The definition of CLs is,
\begin{equation}
\textrm{CL}_{\textrm{s}} = \frac{\textrm{CL}_{\textrm{splusb}}}{\textrm{CL}_{\textrm{b}}} = \frac{p_{\mu_{S}}}{p_{b}},
\end{equation}
where $p_{\mu_{S}},p_{b}$ are the $p$-value of signal-and-background hypothesis and background-only hypothesis~\cite{Junk_1999}.
When the CLs value is less than 0.05, the signal model is excluded with the 95\% confidence level.

We perform three hypothesis tests.
One is ``background-only fit'' based on the background-only hypothesis with $\mu_{S} = 0$.
In this analysis, we consider no contribution of any BSM signals and estimate the deviation between the data and MC samples.
The second is ``model-independent fit.''
In the model-independent fit, the CLs calculation is performed with a various number of the signal event.
In this calculation, the signal model is assumed to be a generic BSM signal, not just SUSY models.
A number of signal events satisfying that $\textrm{CL}_{\textrm{s}} = 0.05$ is obtained and we set an upper limit on a visible cross-section for each signal region.
The last one is ``model-dependent fit.''
In this test, signal models are assumed, and the $\mu_{S}$ value is fixed to be 1.
CLs values are obtained for each signal model and mass point.
We set the exclusion limit on the SUSY mass parameter plane for each signal model.


