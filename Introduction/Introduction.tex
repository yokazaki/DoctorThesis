\DeclareInputPath{Introduction}

\chapter{Introduction}
\label{ch:Introduction}

The Standard Model (SM) of the particle physics describes the interaction among elementary particles and successfully explains most experimental results.
With Higgs boson discovery~\cite{HIGG-2012-27,CMS-HIG-12-028} by the ATLAS~\cite{PERF-2007-01} and CMS~\cite{CMS-TDR-08-001} experiments, all predicted particles in the SM have been observed.
Since its discovery, the ATLAS and CMS experiments have been measuring the nature of the Higgs boson, such as the Yukawa coupling, using the Large Hadron Collider (LHC), the world's highest energy proton-proton collision ($pp$-collision).
There is no deviation of the Yukawa couplings to SM fermions in the third generation ($b, t, \tau$) and gauge bosons from the expected values~\cite{HiggsPublicResults}.
%The SM describes nature successfully.
However, there are still several remaining problems in the SM.
One is that the SM does not explain naturally why the observed Higgs mass is $\mathcal{O}(100\GeV)$, regardless of the large quantum correction ($> \mathcal{O}(10^{36}\GeV^2)$) by the SM particles.
Another is the existence of the dark matter, which accounts for $\sim85\%$ of the matter in the universe.
However, it cannot be explained by the SM~\cite{VCR:1970Ast,VCR:1980Ast,WMAP2013,Planck2015}.
Thus, the existence of a new physics beyond the Standard Model (BSM) is implied.

One of the promising theoretical frameworks to resolve these problems is Supersymmetry (SUSY), which introduces an additional symmetry between fermions and bosons.
SUSY also adds supersymmetric partners (superpartners) of the SM particles, with the same quantum numbers except for their spins.
In the supersymmetry framework, the quantum correction for the Higgs mass is canceled between the SM particles and their superpartners (``naturalness'').
Besides, the lightest SUSY particle can be a good candidate of the dark matter.
The masses of the superpartners of the SM particles are predicted to be less than $\mathcal{O}$(TeV) to explain these problems.
Additionally, light SUSY particles can provide an explanation of the muon g-2 anomaly.
LHC is the first and only accelerator that explores $\mathcal{O}$(TeV) scale physics.
Therefore, only the possible place where SUSY particles ($< \mathcal{O}$(TeV)) can be searched for directly.

The superpartners of quarks and gluons, called strong SUSY particles, have already been explored up to approximately 2~TeV on their mass.
However, since the cross-sections of the superpartners of the SM electroweak bosons (electroweakinos) are smaller than strong SUSY particles, the electroweakinos have not yet been explored up to high mass.
For example, the exclusion limit in the mass of a wino, which is the superpartner of the SU(2) gauge field, is up to $700\GeV$.
Then, in this thesis, we search for electroweakinos using the full amount of available data, corresponding to 139\ifb, collected by the ATLAS detector in 2015-2018.
Our target is the pair production of electroweakinos (\heavyino), which decay into light electroweakinos (\lightino) and either of $W$ boson, $Z$ boson, or Higgs boson ($W/Z/h$).
In the previous searches, leptonically decaying $W/Z$ bosons or $b$-tagged jets of $Z/h\ra bb$ have been used in these searches for $\heavyino\ra\lightino+W/Z/h$.
This is to suppress a huge amount of backgrounds caused by jets originating from quarks and gluons.
However, the previous searches suffer from a low branching ratio of leptonically decaying bosons.
Here, on the contrary, we challenge the fully hadronic final states ($W/Z/h\ra qq/bb$ where $q$ denotes light flavor quark, $u, d, s, c$), as shown in Figure~\ref{fig:intro:diagram_fullhad} to profit its large branching fraction.
The search for electroweakinos in the $qqqq$ final state with two $W/Z$ bosons decaying into two light-flavor quarks is the first time at the LHC.


%-------------------------
\begin{figure}[t]
 \centering
\subfigure[$qqqq$ final state]{\includegraphics[width=0.48\textwidth]{Introduction/figure/chichi-qqqq-XX.pdf}}
\subfigure[$qqbb$ final state]{\includegraphics[width=0.48\textwidth]{Introduction/figure/chichi-qqbb-XX.pdf}}
  \caption{Diagram of the electroweakino decay in fully hadronic final states.}
  \label{fig:intro:diagram_fullhad}
\end{figure}
%-------------------------

In the previous analyses, the number of leptons is required explicitly, i.e., the target bosons decayed from \heavyino are determined for each analysis, for example, 1-lepton for $W\ra l\nu$ and 2-leptons for $Z\ra ll$.
On the contrary, $W/Z/h$ are all reconstructed as jets in the fully hadronic final states.
Therefore, this analysis enables to search for electroweakinos comprehensively regardless of the decay process since the difference between $\heavyino^{\pm}\ra W^{\pm}+\lightino^{0}$ and $\heavyino^{\pm}\ra Z/h+\lightino^{\pm}$ is not deliberately identified.

The advantages of the search for electroweakinos in fully hadronic final states are:
\begin{itemize}
\item Statistical benefit due to a large branching ratio of hadronically decaying $W/Z/h$ bosons
\item Characteristic signatures of boosted $W/Z/h$ for the case of a large mass splitting between \heavyino and \lightino, i.e., the two quarks from hadronically decaying bosons are collimated
\item Less dependency to theoretical models (flavor independency)
\end{itemize}

We make use of the following two characteristic signatures for the search.
One is the large missing transverse energy, \met, due to \lightino does not interact with the detector.
The other is a large radius jet to reconstruct collimated daughter quarks decayed from boosted $W/Z/h$ as a single jet.
Especially for this analysis, we introduced a novel experimental method of tagging boosted $W/Z/h$ bosons using large-radius jets (``boson tagging'').
In the boson tagging, we use the jet substructure variables to distinguish $W/Z/h$ bosons from the quark- or gluon-initiated jets, as shown in Figure~\ref{fig:reco:jet_prong}.
The boson tagging was optimized to retain approximately 50\% efficiency to $W/Z/h$ while to makes quark- or gluon-initiated jets down to 1-10\%.
The dominant backgrounds are \Znunu, \Wjets, and $VV$ ($V$ denotes $W/Z/h$), where \met is due to leptonically decaying $W/Z$ and quark- or gluon-initiated jets are mis-identified as boosted $W/Z/h$.
Most of these backgrounds are not well described by a simulation.
However, in this analysis, we estimated these backgrounds in a data-driven way.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[1-prong jets such as quark- or gluon-initiated jet]{\includegraphics[width=0.48\textwidth]{Reconstruction/figure/jet_1prong_schematic}}
\subfigure[2-prong jets such as the hadronic decays of $W/Z$ bosons]{\includegraphics[width=0.48\textwidth]{Reconstruction/figure/jet_2prong_schematic}}
\caption[Schematic views of 1-prong jets and 2-prong jets]{
Schematic views of 1-prong jets (a) and 2-prong jets (b).
Collinear (blue) and soft (green) radiations are illustrated.
The angular size of the collinear radiation is $R_{cc}$ and the \pt fraction of the soft radiation is $z_{s}$.
For 2-prong jets, collinear-soft radiation emitted from the dipole formed by the two subjets represents the orange line.
$R_{12}$ represents the angle between these two subjets~\cite{Larkoski:2014gra}.
}
\label{fig:reco:jet_prong}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


After target theoretical models of this thesis are described in Chapter~\ref{ch:theory}, experimental setup is discussed in Chapter~~\ref{ch:Instruments} and~\ref{ch:DataAndSimulations}.
Then, how physics objects are experimentally reconstructed is explained in Chapter~\ref{ch:Reconstruction}, additionally in Chapter~\ref{ch:BosonTagging}, for the boson tagging, which is one of the keys of this thesis.
How the candidates are selected and how backgrounds are evaluated as written in Chapter~\ref{ch:EventSelection} and~\ref{ch:Backgroundestimation}.
Systematic uncertainties considered in this thesis are described in Chapter~\ref{ch:Systematics}, and the results and theoretical interpretations are discussed in Chapter~\ref{ch:Results} and~\ref{ch:Discussion}.
The SUSY scenarios motivated by the dark matter, the naturalness, and the muon g-2 anomaly are discussed in the same chapter.
The final conclusion is given in Chapter~\ref{ch:Conclusion}.


%In this thesis, the theoretical notations of SUSY are introduced in Chapter~\ref{ch:theory}.
%The Large Hadron Collider and the ATLAS detector, which are the experimental apparatus, are described in Chapter~\ref{ch:Instruments}.
%The data and Monte Carlo simulations are described in Chapter~\ref{ch:DataAndSimulations}.
%Reconstruction techniques of physics objects are introduced in Chapter~\ref{ch:Reconstruction}, and boson tagging techniques are described in Chapter~\ref{ch:BosonTagging}.
%Event selections and background estimation methods are introduced in Chapter~\ref{ch:EventSelection},~\ref{ch:Backgroundestimation}.
%For SUSY signal searches, we use 0-lepton regions.
%However, we use 1-lepton and 1-photon regions to validate the estimation of backgrounds in 0-lepton regions.
%Thus, we introduce selections for 1-lepton and 1-photon regions.
%In Chapter~\ref{ch:Systematics}, we describe systematic uncertainties.
%In Chapter~\ref{ch:StatAnalysis}, methods for statistical analysis are introduced.
%We discuss the results of SUSY signal searches in Chapter~\ref{ch:Results},
%Besides, the prospects and impacts on the unexplained problems are discussed in Chapter~\ref{ch:Discussion}.


%\section{Unsolved problems of SM}
%\label{sec:problem_SM}
%\input{Introduction/Problem_SM}

%\section{Supersymmetry}
%\label{sec:susy}
%\input{Introduction/Supersymmetry}

