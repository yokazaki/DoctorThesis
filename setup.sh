

BASEDIR=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
export MAKE_OPTIONS="-C ${BASEDIR} PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:${PATH}"
alias make="make ${MAKE_OPTIONS}"

