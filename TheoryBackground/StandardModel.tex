


\section{The Standard Model of particle physics}\label{sec:theSM}


The \textit{Standard Model of particle physics (SM)} is a field theory that includes fields corresponding to the observed particles.
``Particles'' appear by quantizing the motion of these fields.
The SM has the following three ingredients.
(i) \textit{Fermions}, which constitute matters. This type of particles consists of \textit{quarks} and \textit{leptons}.
(ii) \textit{Gauge symmetry}, from which the interactions of the fermions are derived. Fields of another type called \textit{gauge bosons} are required.
(iii) \textit{The origin of the mass} is explained by introducing \textit{Higgs field}.


The elementary particles are summarized in Tables \ref{tab:intro:fermion} and \ref{tab:intro:boson}.
Known interactions of these particles are \textit{electromagnetic interaction}, \textit{weak interaction}, \textit{strong interaction} and \textit{gravity}.
The electromagnetic and weak interactions are unified into \textit{electroweak interactions}.
Three interactions excluding the gravity are derived from \textit{gauge symmetry} and mediated by corresponding \textit{gauge bosons}.

The basic description of the SM is presented in the next sections following arguments given in Refs.~\cite{Peskin:1995ev,Halzen:1984mc}.


\SimpleTable(tables/SMFermion.tex){
\label{tab:intro:fermion}
Fermions in the Standard Model.
}

\SimpleTable(tables/SMBoson.tex){
\label{tab:intro:boson}
Bosons in the Standard Model.
}



\subsubsection{Gauge symmetry}

Lagrangian of the elementary fields is required to be invariant (symmetric) under certain \textit{gauge transformations} which are explained in the next paragraph.
The set of gauge transformations forms a group called \textit{gauge group}.
An example is the \textit{Yang-Mills} Lagrangian:
\begin{equation}
\mathcal{L}
=-\frac{1}{4}F_{\mu\nu}^{a}F^{a\mu\nu}
+\bar{\psi}(i\slashed{D})\psi
-m\bar{\psi}\psi.
\label{eq:theory:Yang-Mills}
\end{equation}
The anti-symmetric tensor $F_{\mu\nu}^a$ is defined as:
\begin{equation}
F_{\mu\nu}^{a}=\partial_{\mu}A_{\nu}^{a}-\partial_{\nu}A_{\mu}^{a}+gf^{abc}A_{\mu}^{b}A_{\nu}^{c},
\end{equation}
where $A_{\mu}^a$ are vector fields and $f^{abc}$ is the \textit{structure constants} of the gauge group.
$\psi$ is a set of fermion fields.
The \textit{covariant derivative} $D_{\mu}$ is defined as
\begin{equation}
D_{\mu} = \partial_{\mu}-igA_{\mu}^{a}t^{a},
\end{equation}
where $g$ is the \textit{gauge coupling constant} and $t^a$ is the generators of the gauge group\footnote{Generators which belong to different simple Lie sub-algebras of the gauge group can have different coupling constants.}.


The Yang-Mills Lagrangian in Eq.~\ref{eq:theory:Yang-Mills} is invariant under the following gauge transformation.
The fermion fields are transformed as:
\begin{equation}
\psi\to\exp\left(i\alpha^at^a\right)\psi,
\end{equation}
where $\alpha^a=\alpha^a(x)$ are functions of the spacetime.
For an infinitesimal transformation, this is rewritten as
\begin{equation}
\psi\to(1+i\alpha^at^a)\psi.
\end{equation}
The transformation of the vector fields are
\begin{equation}
A_{\mu}^{a}\to A_{\mu}^{a}+\frac{1}{g}\partial_{\mu}\alpha^{a}+f^{abc}A_{\mu}^{b}\alpha^c.
\end{equation}
Under these transformations, the components in the Lagrangian are transformed as:
\begin{align}
(\partial_{\mu}-igA_{\mu}^{a}t^{a})\psi
&\to(1+i\alpha^bt^b)(\partial_{\mu}-igA_{\mu}^{a}t^{a})\psi+O(\alpha^2), \\
F_{\mu\nu}^{a}
&\to F_{\mu\nu}^{a}+gf^{abc}F_{\mu\nu}^{b}\alpha^c+O(\alpha^2).
\end{align}
In the second line, the \textit{Jacobi identity}\footnote{$f^{ade}f^{bcd}+f^{cde}f^{abd}+f^{bde}f^{cad}=0$.} is used.
Using these rules, the terms in the Yang-Mills Lagrangian are invariant under these gauge transformations.

The electroweak and strong interactions are described as Yang-Mills theories based on the gauge groups $SU(2)_L\times U(1)_Y$ and $SU(3)_C$, respectively.
The implication of these theories are explained in the following sections.

\subsubsection{Electroweak interaction}


The SM unifies the electromagnetic and weak interactions as \textit{electroweak interaction}.
The behavior of the electroweak interaction is understood as an individual interaction of right-handed and left-handed fermions.
The right and left-handed fermions are defined by the eigen states of 1 and -1 of the $\gamma^5$ matrix, where $\gamma^5$ is $i\gamma^0\gamma^1\gamma^2\gamma^3$ and $\gamma^{\mu}$ ($\mu=0,\cdots,3$) are the \textit{Dirac matrices}.
The left-handed fermions form \textit{isospin doublets} combining \textit{neutrino} and \textit{electron}, \textit{up quark} and \textit{down quark}.
The electroweak interaction is identified as a gauge interaction based on $SU(2)_L\times U(1)_Y$.
The interaction between these isospin doublets and gauge bosons $W_{\mu}^a\;\;(a=1,2,3)$ and $B_{\mu}$ are described as:
\begin{align}
-i\bar{L}\gamma^\mu\left(g\tau^{a}W_{\mu}^a+g'\frac{Y_L}{2}B_{\mu}\right)L
\label{eq:left_lep},\\
-i\bar{Q}\gamma^\mu\left(g\tau^{a}W_{\mu}^a+g'\frac{Y_Q}{2}B_{\mu}\right)Q
\label{eq:left_quark},
\end{align}
where $L=(\nu_{eL},e_L)$, $Q=(u_L,d_L')$ and $\tau^a\;\;(a=1,2,3)$ are the half of the Pauli matrices: $\tau^a=\sigma^a/2$.
There are three copies of $L$ and $Q$ referred to as \textit{generation}, and they are described by the same Lagrangian.
$d_L'$ is an eigen state of the weak interaction and related to a mass eigen state $d_L$ through the Cabbibo-Kobayashi-Maskawa matrix $V_{\text{CKM}}$ as:
\begin{align}
\left(\begin{array}{c}d'_L\\s'_L\\b'_L\end{array}\right)=
V_{\text{CKM}}
\left(\begin{array}{c}d_L\\s_L\\b_L\end{array}\right).
\end{align}
$Y_L$ and $Y_Q$ are called \textit{hyper charge} and the values are summarized in Table \ref{tab:intro:EWcharge}.


\SimpleTable(tables/EWCharge.tex)[
Charge of the electroweak interaction.
]{
\label{tab:intro:EWcharge}
Charge of the electroweak interaction.
The values for $\nu_R$ are written for the case that they exist.
$T^3$ has a value of $\tau^3/2$ for the left-handed fields,
and 0 for the right-handed fields.
$Q_{\text{em}}$ is the electric charge and $T^3+Y/2=Q_{\text{em}}$ is satisfied.
}



The gauge fields $W_{\mu}^i$ and $B_{\mu}$ in Eqs.~\ref{eq:left_lep} and \ref{eq:left_quark} are related to the gauge bosons in Table \ref{tab:intro:boson} by:
\begin{align}
&W_{\mu}^{\pm}=\frac{W_{\mu}^1\mp iW_{\mu}^2}{\sqrt{2}}\\
&\left(\begin{array}{c}Z_{\mu}\\ A_{\mu}\end{array}\right)=
\left(\begin{array}{cc}\cos\theta_{W}&-\sin\theta_W\\ \sin\theta_W&\cos\theta_W\end{array}\right)
\left(\begin{array}{c}W_{\mu}^{3}\\ B_{\mu}\end{array}\right),
\end{align}
where $\theta_W$ is the Weinberg angle, which is related to the gauge couplings through:
\begin{equation}
\cos\theta_W = \frac{g}{\sqrt{g^2+g'^{2}}},\;\sin\theta_W = \frac{g'}{\sqrt{g^2+g'^2}}.
\end{equation}


On the other hand, the right-handed fermions do not form isospin doublets but are isospin singlets.
In this case, these right-handed fermions do not interact with the $W$ boson.
The interaction of the right-handed and the gauge boson are described as:
\begin{align}
-ig'\frac{Y_e}{2}\bar{e_R}\gamma^\mu B_{\mu}e_R,
\label{eq:right_lep}\\
-ig'\frac{Y_u}{2}\bar{u_R}\gamma^\mu B_{\mu}u_R,
\label{eq:right_up}\\
-ig'\frac{Y_d}{2}\bar{d_R}\gamma^\mu B_{\mu}d_R.
\label{eq:right_down}
\end{align}


By rewriting Eq.~\ref{eq:left_lep} in terms of the observed field $A_\mu,Z_{\mu}$ and $W_{\mu}^{\pm}$, the equation becomes
\begin{align}
-i\frac{g}{\sqrt{2}}&\bar{L}\gamma^{\mu}\left(\tau^+W_{\mu}^++\tau^-W_{\mu}^-\right)L
+
-i\sqrt{g^2+g'^2}\bar{L}\gamma^{\mu}\left(\tau_3\cos^2\theta_W-\frac{Y_L}{2}\sin^2\theta_W\right)LZ_{\mu}
-ie\bar{L}\gamma^{\mu}\paren{\tau^3+\frac{Y_L}{2}}LA_{\mu}\\
=&
-i\frac{g}{\sqrt{2}}\left(\bar{\nu}_L\gamma^{\mu}e_LW_{\mu}^++\bar{e}_L\gamma^{\mu}\nu_LW_{\mu}^-\right)\nonumber\\
&-i\sqrt{g^2+g'^2}\left[\bar{\nu}_L\gamma^{\mu}\nu_L - \left(\frac{1}{2}-\sin^2\theta_W\right)\bar{e}_L\gamma^{\mu}e_L\right]Z_{\mu}
-ie\bar{e}_L\gamma^{\mu}e_LA_{\mu}, \label{eq:theory:EW_interaction_lepton}
\end{align}
where $\tau^{\pm}=(\tau^1\mp i\tau^2)$, and $e$ is the electric charge of the electron:
\begin{equation}
e=\frac{gg'}{\sqrt{g^2+g'^2}}.
\end{equation}
Note that in Eq.~\ref{eq:theory:EW_interaction_lepton} the values of hypercharges in Table \ref{tab:intro:EWcharge} are substituted.
Similarly, Eq.~\ref{eq:right_lep} becomes
\begin{align}
-ig'&\frac{Y_e}{2}\bar{e}_R\gamma^{\mu}\left(\cos\theta_WA_{\mu}-\sin\theta_WZ_{\mu}\right)e_R\\
&=
ig'\sin\theta_W\bar{e}_R\gamma^{\mu}e_RZ_{\mu}
-ie\bar{e}_R\gamma^{\mu}e_RA_{\mu}.
\end{align}
The values of hypercharges in Table \ref{tab:intro:EWcharge} are substituted.

A scalar field in isospin doublet called \textit{Higgs field} is introduced, and the motion of this field is described as:
\begin{align}
&\left|\left(\partial_{\mu}-ig\tau^{a}W_{\mu}^a-i\frac{g'}{2}B_{\mu}\right)\varphi\right|^2+V(\varphi),\\
&V(\varphi)=\mu^2\varphi^{\dagger}\varphi-\lambda\left(\varphi^{\dagger}\varphi\right)^2.
\label{eq:higgsgauge}
\end{align}
The minimum of $V(\varphi)$ is given when $(\varphi^{\dagger}\varphi)=\mu^2/2\lambda$.
The vacuum goes to one of the minima
\begin{equation}
\varphi =
\left(\begin{array}{c}
0 \\ \frac{\mu}{\sqrt{2\lambda}}
\end{array}\right)=
\frac{1}{\sqrt{2}}\left(\begin{array}{c}
0 \\ \vev
\end{array}\right),
\label{eq:intro:vev}
\end{equation}
where \vev is a constant called \textit{vacuum expectation value (VEV)}.
This is referred to as \textit{electroweak symmetry breaking (EWSB)}.
After the EWSB, Eq.~\ref{eq:higgsgauge} becomes
\begin{align}
&\left(\frac{\vev g}{2}\right)^2W_{\mu}^+W^{-\mu}+
\frac{1}{8}\vev^2(gW_{\mu}^3+g'B_{\mu})^2\\
&=\left(\frac{\vev g}{2}\right)^2W_{\mu}^+W^{-\mu}+
\frac{1}{8}\vev^2(g^2+g'^2)Z_{\mu}Z^{\mu}.
\end{align}
The masses of $W_{\mu}^{\pm}$ and $Z_{\mu}$ are identified as:
\begin{equation}
m_W=\frac{1}{2}\vev g,\;
m_Z=\frac{1}{2}\vev\sqrt{g^2+g'^{2}} \label{eq:intro:mZmW}.
\end{equation}
The mass of $A_{\mu}$ is $M_A=0$.


It is possible to add terms:
\begin{align}
&\lambda_e\left(\bar{L}\varphi e_{R}+\bar{e}_R\varphi^{*} L\right),\\
&\lambda_d\left(\bar{Q}\varphi d_{R}+\bar{d}_R\varphi^{*} Q\right),\\
&\lambda_u\left(\bar{Q}\varphi_c u_{R}+\bar{u}_R\varphi_c^{*} Q\right)\;\;\;\;\;(\varphi_c=-i\tau_2\varphi^*),
\end{align}
thanks to the Higgs field that absorbs the difference in the representation of the left- and right-handed fermions.
The $\lambda_f$ ($f=e,d,u$) are called \textit{Yukawa coupling constants}.
After the EWSB ($\langle\varphi\rangle=(0,\vev)/\sqrt{2}$), these terms become
\begin{align}
&\frac{\lambda_e\vev}{\sqrt{2}}\left(\bar{e}_{L}e_{R}+\bar{e}_Re_L\right),\\
&\frac{\lambda_d\vev}{\sqrt{2}}\left(\bar{d}_{L}d_{R}+\bar{d}_Rd_L\right),\\
&\frac{\lambda_u\vev}{\sqrt{2}}\left(\bar{u}_{L}u_{R}+\bar{u}_Ru_L\right).
\end{align}
The fermions acquire their masses:
\begin{equation}
m_f=\frac{\lambda_f\vev}{\sqrt{2}}\;(f=e,u,d,\cdots).
\label{eq:intro:fermion_mass}
\end{equation}



For completeness, the kinetic terms of the gauge bosons are given by
\begin{equation}
-\frac{1}{4}W_{\mu\nu}^{a}W^{a\mu\nu}-\frac{1}{4}B_{\mu\nu}B^{\mu\nu}.
\end{equation}
The antisymmetric tensors $W_{\mu\nu}^{a}$ and $B_{\mu\nu}$ are defined as:
\begin{align}
&W_{\mu\nu}^{a} = \partial_{\mu}W^{a}_{\nu}-\partial_{\nu}W^{a}_{\mu}+g\epsilon^{abc}W^{b}_{\mu}W^{c}_{\nu},\\
&B_{\mu\nu} = \partial_{\mu}B_{\nu}-\partial_{\nu}B_{\mu}.
\end{align}


\subsubsection{Strong interaction}

The quarks have a degree of freedom called \textit{color}.
By imposing the $SU(3)_C$ gauge symmetry on a color triplets of Dirac fields $\vect{\psi}$,
the triplet interacts with new gauge fields, gluon $G_{\mu}^a\;(a=1,\cdots,8)$ as:
\begin{align}
-ig_{s}\bar{\vect{\psi}}\gamma^{\mu}t^a\vect{\psi} G_{\mu}^a,
\end{align}
where $t^a$ are the representation matrices of $SU(3)$.
The kinetic term of the gluon is written as:
\begin{equation}
-\frac{1}{4}G_{\mu\nu}^aG^{a\mu\nu},
\end{equation}
where
\begin{align}
G_{\mu\nu}^{a} = \partial_{\mu}G^{a}_{\nu}-\partial_{\nu}G^{a}_{\mu}+g_sf^{abc}G^{b}_{\mu}G^{c}_{\nu}.
\end{align}
$f^{abc}$ is the structure constant of $SU(3)$.
This gauge theory based on the $SU(3)_C$ symmetry is referred to as \textit{Quantum Chromodynamics (QCD)}.

The leptons are color singlet unlike the quarks.
Hence, the leptons do not interact with the gluon.
