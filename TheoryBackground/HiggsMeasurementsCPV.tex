


\section{Measurements of the properties of the Higgs boson relevant to CP violation}
\label{sec:Measurement_CPV}

\subsection{Electroweak of baryogenesis via the Yukawa couplings to the top and bottom quarks}
\label{subsec:EWBG_2HDM}


There can be two or more Higgs fields in theories.
A model that contains two Higgs doublets is called \textit{2 Higgs Doublet Model (2HDM)} \cite{Branco:2011iw}.
For example, theories with supersymmetry must include two Higgs doublets to avoid anomalies and make the theory sensible.
The Yukawa interaction in the general \textit{2HDM} is written as:
%% \begin{align}
%% V=&m_{11}^2\Phi_1^{\dagger}\Phi_1
%% +m_{22}^2\Phi_2^{\dagger}\Phi_2
%% +m_{12}^2(\Phi_1^{\dagger}\Phi_2-\Phi_1^{\dagger}\Phi_2)
%% +\frac{\lambda_{1}}{2}(\Phi_1^{\dagger}\Phi_1)^2
%% +\frac{\lambda_{2}}{2}(\Phi_2^{\dagger}\Phi_2)^2 \nonumber\\
%% &+\lambda_{3}\Phi_1^{\dagger}\Phi_1\Phi_2^{\dagger}\Phi_2
%% +\lambda_{4}\Phi_1^{\dagger}\Phi_2\Phi_2^{\dagger}\Phi_1
%% +\frac{\lambda_{5}}{2}\left[(\Phi_1^{\dagger}\Phi_2)^2 + (\Phi_2^{\dagger}\Phi_1)^2\right]
%% \end{align}
\begin{equation}
\mathcal{L}_Y = -\bar{f}_L(Y_1\Phi_1+Y_2\Phi_2)f_R + \text{h.c.},
\label{eq:intro:2HDM_Yukawa}
\end{equation}
where $\Phi_1$ and $\Phi_2$ are scalar fields of $SU(2)_L$ doublet.
These scalars include five physical particles (Higgs bosons): $h$, $H$, $H^{\pm}$ (scalars) and $A$ (pseudo-scalars).
With these Higgs bosons, Eq.~\ref{eq:intro:2HDM_Yukawa} is rewritten as:
\begin{equation}
\mathcal{L}_Y = -\bar{f}_Ly_{\phi}^ff_R + \bar{u}\left[V\rho^dP_R-\rho^{u\dagger}VP_L\right]dH^++\text{h.c.},
\label{eq:intro:2HDM_Yukawa_masseigenstate}
\end{equation}
where coupling constants $y^{f}_{ij}$ for flavor $f=u,d,e$ are defined as:
\begin{align}
&y_{hij}^f = \frac{\lambda_i^f}{\sqrt{2}}\delta_{ij}\sin(\beta-\alpha) + \frac{\rho_{ij}^f}{\sqrt{2}}\cos(\beta-\alpha),\\
&y_{Hij}^f = \frac{\lambda_i^f}{\sqrt{2}}\delta_{ij}\cos(\beta-\alpha) + \frac{\rho_{ij}^f}{\sqrt{2}}\sin(\beta-\alpha),\\
&y_{Aij}^f = \mp i\frac{\rho_{ij}^f}{\sqrt{2}}.
\end{align}
In these formulae, $i$ and $j$ indicate the flavor, and $\tan\beta$ represents the ratio of the two vacuum expectation values (VEVs) of $\Phi_1$ and $\Phi_2$.
$\alpha$ is the mixing angle of $h$ which is constructed from the $\Phi_1$ and $\Phi_2$ components.


The phenomenological importance of the 2HDM is that it can include CP violation in the Higgs sector.
\textit{It is proposed that the BAU can be generated in general 2HDM framework} \cite{Fuyuto:2017ewj,Modak:2018csw}
avoiding limits from experiments such as flavor physics and the electric dipole moments of the electron, the neutron, and the proton.
In this scenario, the baryon number is generated when the electroweak phase transition (EWPT) took place.
At the EWPT, a domain with the broken electroweak symmetry expands to the whole universe.
For the left-handed fields, more quarks are reflected by interaction with the domain wall of the EWPT than anti-quarks (Sakharov's condition 1).
Similarly, for the right-handed fields, more anti-quarks are generated.
In the symmetric phase, \textit{sphaleron process} frequently occurs since the sphaleron energy is proportional to the VEV.
The sphaleron process acts only left-handed fields, which means the net baryon number is generated (Sakharov's condition 2).
If the EWPT is strongly first order, the generated baryon number is conserved in the broken phase (Sakharov's condition 3).


Measurements of the Higgs boson coupling at the LHC, especially the coupling of the Higgs boson to the $t$ and \bquarks, are sensitive to viable regions for the electroweak baryogenesis (EWBG) in a complementary way to other experiments.
The viable regions for the EWBG via extra bottom Yukawa coupling in the general 2HDM are shown in Figure \ref{fig:intro:EWBG_bottom_transport}.
Thus, measurements of the coupling of the Higgs boson and the \bquark is important in the context of EWBG.
This measurement is discussed in Section \ref{sec:HCombKappa}.

\SimpleGraphics[130mm](figures/EWBGviaBottom.pdf)
[Contours of viable regions of the EWBG via the extra Yukawa coupling of the bottom quark transport in the general 2HDM framework and experimental limits from Ref.~\cite{Modak:2018csw}.]{
\label{fig:intro:EWBG_bottom_transport}
Contours of viable regions of the EWBG via the extra Yukawa coupling of the bottom quark in the general 2HDM framework are shown in the blue line \cite{Modak:2018csw}.
The regions excluded with 2$\sigma$ are shown as the following.
The gray shaded regions represent the Higgs measurements at the LHC.
The purple regions correspond to measurements of the branching ratio of $B$-hadrons: $B\to X_s\gamma$.
The red curve shows a measurement of the difference of the CP-asymmetry between charged and neutral $B\to X_s\gamma$ decays.
The parameters for the 2HDM is selected as indicated in the plots.
}

The coupling of the Higgs boson to the top quark can also be relevant to the EWBG.
Ref.~\cite{Fuyuto:2017ewj} suggests a parameter space viable for the EWBG with the $H\to\gamma\gamma$ decay width as shown in Figure \ref{fig:intro:EWBG_top_transport}.
As seen in this figure, the measurement of the Higgs coupling to the top quark is sensitive to parts of the parameter values that can generate a sufficient baryon number.
The most stringent limit on the electron EDM (eEDM) is $|d_e|<1.1\times 10^{-29}\;e\mathrm{cm}$ \cite{Andreev:2018ayy}.
However, the eEDM can be smaller by choosing another parameter $\rho_{ee}$ in this model, and EWBG is still possible \cite{PhysRevD.101.011901}.
Therefore, a test by the measurement of the Higgs couplings is still motivated.

The measurement of the \Hbb coupling also contributes to the precision of other couplings of the Higgs boson because the \Hbb coupling has the largest contribution to the total width of the Higgs boson.
Experimental observables are branching ratios, which are partial widths divided by the total width.
Thus, to constrain partial widths or coupling constants, it is necessary to constrain the total width.
This is discussed in Section \ref{sec:HCombKappa}.


\SimpleGraphics(figures/EWBGviaTop.pdf)[
Contour of a viable region of the EWBG via the extra Yukawa coupling of the top quark \cite{Fuyuto:2017ewj}.
]{
\label{fig:intro:EWBG_top_transport}
Contour of a viable region of the EWBG via the extra Yukawa coupling of the top quark \cite{Fuyuto:2017ewj}.
$Y_B/Y_B^{\text{obs}}=1$ corresponds to the observed asymmetry of the baryon number.
The limits from the electron EDM are indicated with $d_e$.
The blue dashed curves show the variation of the $H\to\gamma\gamma$ width that can be tested by the LHC.
}



\subsection{Measurement of CP property in the $VVH$ vertex}
\label{subsec:CPOddVVH}

While theoretical works are rarely found for models that generate the BAU via CP violation in the $VVH$ vertex,
it is possible that the $VVH$ vertex has a CP-odd structure.
For example, these CP-odd BSM interactions can be generated by the CP-mixed 125 GeV Higgs boson in a BSM such as 2HDM \cite{Artoisenet:2013puc,Maltoni:2013sma}.

The CP structure of the coupling of the Higgs and vector bosons was studied in the Higgs decays.
An analysis was performed using datasets from ATLAS Run 1 \cite{Aad:2015mxa}.
In the analysis, the Higgs coupling to the vector bosons is parametrized as:
\begin{align}
\mathcal{L}^V = &\left\{
\kappa_{\text{SM}}\cos\alpha\left[\frac{1}{2}g_{HZZ}Z_{\mu}Z^{\mu}+g_{HWW}W_{\mu}^{+}W^{-\mu}\right]\right. \nonumber\\
&-\frac{1}{4}\frac{1}{\Lambda}\left[\kappa_{HZZ}\cos\alpha Z_{\mu\nu}Z^{\mu\nu}+\kappa_{AZZ}\sin\alpha Z_{\mu\nu}\widetilde{Z}^{\mu\nu}\right] \nonumber\\
&\left.-\frac{1}{2}\frac{1}{\Lambda}\left[\kappa_{HWW}\cos\alpha W_{\mu\nu}^{+}W^{-\mu\nu}+\kappa_{AWW}\sin\alpha W_{\mu\nu}^{+}\widetilde{W}^{-\mu\nu}\right]
\right\}\cdot h.
\label{eq:intro:EFT_VVH_HiggsDecay}
\end{align}
In this parametrization, $Z_{\mu\nu}Z^{\mu\nu}$ and $W^{+}_{\mu\nu}W^{-\mu\nu}$ is connected to CP-even BSM contributions while $Z_{\mu\nu}\widetilde{Z}^{\mu\nu}$ and $W^{+}_{\mu\nu}\widetilde{W}^{-\mu\nu}$ is connected to CP-odd BSM contributions.
When results are presented, the parameters $\kappa_{HVV}$ and $\kappa_{AVV}$ are redefined as:
\begin{equation}
\label{eq:intro:EFT_VVH_HiggsDecay_redefinition}
\widetilde{\kappa}_{AVV} = \frac{1}{4}\frac{\vev}{\Lambda}\kappa_{AVV},\;\;\;\;\;\;
\widetilde{\kappa}_{HVV} = \frac{1}{4}\frac{\vev}{\Lambda}\kappa_{HVV}.
\end{equation}
Constraints on the parameters $(\widetilde{\kappa}_{AVV}/\kappa_{\text{SM}})\cdot\tan\alpha$ and $\widetilde{\kappa}_{HVV}/\kappa_{\text{SM}}$ were derived and summarized in Table \ref{tab:intro:Constraints_VVH_Run1}.
The profiled likelihood scans for these coupling ratios are also shown in Figure \ref{fig:intro:Measurements_VVH_Run1}.

\SimpleTable(tables/VVHConstraintsRun1.tex)[
Expected and observed fitted values of $\widetilde{\kappa}_{HVV}/\kappa_{\text{SM}}$ and $(\widetilde{\kappa}_{AVV}/\kappa_{\text{SM}})\cdot\tan\alpha$,
and the 95\% excluded regions derived from the $H\to ZZ^{*}\to 4\ell$ and $H\to WW^{*}\to e\nu\mu\nu$ decays from Ref.~\cite{Aad:2015mxa}.
]{
\label{tab:intro:Constraints_VVH_Run1}
Expected and observed fitted values of $\widetilde{\kappa}_{HVV}/\kappa_{\text{SM}}$ and $(\widetilde{\kappa}_{AVV}/\kappa_{\text{SM}})\cdot\tan\alpha$,
and the 95\% excluded regions derived from the $H\to ZZ^{*}\to 4\ell$ and $H\to WW^{*}\to e\nu\mu\nu$ decays.
The signal strengths are considered as independent for decay channels and the beam energy ($\sqrt{s}=7,8\TeV$).
The table is taken from Ref.~\cite{Aad:2015mxa}.
}

\SimpleGraphics(\FigRow(figures/HiggsSpinParityRun1/fig_11a.eps)(figures/HiggsSpinParityRun1/fig_11b.eps))[
Observed scanned values of the log likelihood ratio as functions of $\widetilde{\kappa}_{HVV}/\kappa_{\text{SM}}$ and $(\widetilde{\kappa}_{AVV}/\kappa_{\text{SM}})\cdot\tan\alpha$ derived from the analysis of the Higgs decays \cite{Aad:2015mxa}.
]{
\label{fig:intro:Measurements_VVH_Run1}
Observed scanned values of the log likelihood ratio as functions of $\widetilde{\kappa}_{HVV}/\kappa_{\text{SM}}$ and $(\widetilde{\kappa}_{AVV}/\kappa_{\text{SM}})\cdot\tan\alpha$ derived from the analysis of the Higgs decays.
The red and blue curves represent the $H\to WW^{*}\to e\nu\mu\nu$ and $H\to ZZ^{*}\to 4\ell$ decays, respectively.
The black curve corresponds to the combination of the two decay channels assuming $\widetilde{\kappa}_{HZZ}=\widetilde{\kappa}_{HWW}=\widetilde{\kappa}_{HVV}$ and $\widetilde{\kappa}_{AZZ}=\widetilde{\kappa}_{AWW}=\widetilde{\kappa}_{AVV}$.
The 68\% and 95\% confidence intervals are indicated by the dotted and dotted-dashed lines indicated in the plots.
The plots are taken from Ref.~\cite{Aad:2015mxa}.
}


The momentum scale is bounded in the Higgs mass when studying the Higgs decays.
Thus, effects from BSM (via irrelevant terms) are suppressed by a factor $m_h/\Lambda^d$.
By studying the \VH production, the momentum scale can be higher than the decay channels since it is bounded by the beam energy.
It is suggested that the \VH production is sensitive to the CP structure in the $VVH$ vertex \cite{deBlas:2019rxi}.
An analysis is performed in Chapter \ref{ch:Discussion}.


\subsubsection{Interpretation of the differential cross-section as a function of \ptv}

CP-odd dimension-6 operators also modify the spectrum of \ptv in the \VH production.
Similar to the interpretation with the CP-even SMEFT, limits on the Wilson coefficients of the CP-odd operators can be derived as an interpretation of the differential cross-section as a function of \ptv.
This analysis is presented in Section \ref{sec:DiffXS:CP}.


\subsubsection{Interpretation of the differential cross-section as a function of a lepton angle}

Since the differential cross-section as a function of \ptv can be modified by both CP-even and CP-odd dimension-6 operators, the \ptv spectrum can not tell whether the dimension-6 interaction is CP-even/odd when an anomalous interaction is found.
An analysis using an angular distribution of the \zll decay is proposed to probe CP-violation in the \VH production.
Since an effect on the angular distribution is smaller than the effect on the \ptv spectrum,
the use of the angular information does not provide stronger constraint on the CP-odd operators than the analysis using the \ptv spectrum.
However, this angular analysis may be useful to test the CP property when an anomalous $VVH$ coupling is found.
This analysis is presented in Section \ref{sec:DiffXS:CP}.
