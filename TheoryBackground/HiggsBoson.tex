


\section{Higgs boson}\label{sec:Higgs}


\subsubsection{The Higgs boson and its interaction}


When the Higgs doublet has the VEV as Eq.~\ref{eq:intro:vev},
the Higgs field is parametrized using real fields $h$, $\phi^1$, $\phi^2$ and $\phi^3$ as:
\begin{equation}
\varphi = \frac{1}{\sqrt{2}}\left(\begin{array}{c}
-i(\phi^1-i\phi^2) \\ \vev+h+i\phi^3
\end{array}\right).
\label{eq:intro:higgs_doublet_ssb}
\end{equation}
$h$ corresponds to a physical scalar particle called \textit{Higgs boson}.
$\phi^i$ are \textit{Nambu-Goldstone bosons}, and they are absorbed into the gauge bosons $W^i$ as their longitudinal degrees of freedom.
Dropping terms of the Nambu-Goldstone bosons, Lagrangian in terms of the Higgs boson is transformed as
\begin{align}
&(\partial_{\mu}\varphi)^2+\mu^2 \varphi^{\dagger}\varphi - \lambda (\varphi^{\dagger}\varphi)^2 \\
&= \frac{1}{2}(\partial_{\mu}h)^2 - \frac{m_h^2}{2}h^2 - \sqrt{\frac{\lambda}{2}}m_hh^3 - \frac{\lambda}{4} h^4,
\end{align}
where $m_h=\sqrt{2}\mu=\sqrt{2\lambda}\vev$, which is identified as the mass of the Higgs boson.

Higgs couplings to the gauge bosons is expressed by terms of Lagrangian:
\begin{equation}
\left[
m_W^2W_{\mu}^{+}W^{-\mu}+\frac{m_Z^2}{2}Z_{\mu}Z^{\mu}
\right]\left(
1+\frac{h}{\vev}
\right)^2,
\label{eq:intro:higgs_WZ}
\end{equation}
where $m_W$ and $m_Z$ are defined as Eq.~\ref{eq:intro:mZmW}.
Higgs couplings to the fermions are described as
\begin{equation}
\mathcal{L} = -m_f\bar{\psi}_f\psi_f\left(1+\frac{h}{\vev}\right),
\label{eq:intro:higgs_fermion}
\end{equation}
where $m_f$ is the fermion mass defined in Eq.~\ref{eq:intro:fermion_mass}.



\subsubsection{Production mechanisms of the Higgs boson}


The Higgs boson is produced in proton-proton collisions by four processes:
gluon-gluon fusion (ggF) \cite{Dittmaier:2011ti,Dittmaier:2012vm},
vector boson fusion (VBF) \cite{Dittmaier:2011ti,Dittmaier:2012vm},
associated production with a top-quark pair ($ttH$) \cite{deFlorian:2016spz,Beenakker:2002nc,Dawson:2002tg,Dawson:2003zu,Yu:2014cka,Frixione:2014qaa},
associated production with a vector boson (\VH)\cite{Glashow:1978ab}.
The cross-sections as functions of the Higgs boson mass $m_h$ are shown in Figure \ref{fig:intro:higgs_xsecs}.
The mechanisms and the production cross-sections at the $m_h=125.09\GeV$, measured in the $H\to\gamma\gamma$ and $H\to ZZ^*\to 4\ell$ decay channels \cite{Aad:2015zhl}, of four processes are explained below.

\SimpleGraphics(figures/plot_13tev_H_sqrt.pdf){
\label{fig:intro:higgs_xsecs}
Theoretical calculation of the production cross-sections of the Higgs boson as functions of $M_h$ at $\sqrt{s}=13\TeV$ \cite{deFlorian:2016spz,twikiCrossSections,LHCHXSWG}.
}


The ggF process is expressed by the Feynman diagram shown in Figure \ref{fig:intro:feyn_ggF}.
The cross-section of ggF are calculated at N3LO (QCD) and NLO (EW) acuracy \cite{Anastasiou:2016cez,deFlorian:2016spz,twikiCrossSections}.
The value of the cross-section is
\begin{equation}
\sigma_{\text{ggF}}=48.52 (\unit{pb})^{+4.56\%}_{-6.72\%}(\text{theory})\pm 1.86\%(\text{PDF}) ^{+2.61\%}_{-2.59\%}(\alpha_s).
\end{equation}
The PDF and \alphaS uncertainties are derived following the recommendation of the PDF4LHC working group \cite{Botje:2011sn}.
The ``theory'' is remaining theory uncertainties including scale variations, PDF uncertainties, uncertainties on electroweak correction, effects from missing mass of quarks, and other uncertainties \cite{deFlorian:2016spz}.
For the scale variations, a short comment is given in Appendix \ref{app:ScaleUnc}.

% ggF
\begin{figure}[!bt]
\centering
\begin{fmfgraph*}(160,70)
\fmfstraight
\fmfleft{i1,i2}
\fmfright{o1,H,o2}
% gluons
\fmf{gluon}{i1,t1}
\fmf{gluon}{t2,i2}
\fmf{phantom,tension=0.5}{t1,o1}
\fmf{phantom,tension=0.5}{t2,o2}
\fmffreeze
% top loop
\fmf{fermion,tension=1}{t1,t2,t3,t1}
% Higgs boson
\fmf{dashes,tension=2.0}{t3,H}
% labels
\fmflabel{$g$}{i1}
\fmflabel{$g$}{i2}
\fmflabel{$H$}{H}
\end{fmfgraph*}
\caption{
\label{fig:intro:feyn_ggF}
Feynman diagram of the gluon-gluon fusion.}
\end{figure}




The VBF production is expressed by the Feynman diagram shown in Figure \ref{fig:intro:feyn_VBF}.
The cross-section of VBF is calculated at NNLO (QCD) and NLO (EW) accuracy \cite{deFlorian:2016spz,twikiCrossSections}.
The result of the cross-section is
\begin{equation}
\sigma_{\text{VBF}}=3.779\unit{pb}^{+0.4\%}_{-0.3\%}(\text{scale})\pm 2.1\%(\text{PDF})\pm 0.5\%(\alpha_s)
\end{equation}

% VBF
\begin{figure}[!bt]
\centering
\begin{fmfgraph*}(140,80)
\fmfleft{i1,i3}
\fmfright{o1,o2,o3}
\fmf{fermion,tension=1.5}{i1,v1}
\fmf{fermion}{v1,o1}
\fmf{fermion,tension=1.5}{i3,v3}
\fmf{fermion}{v3,o3}
\fmf{phantom,tension=0.3}{v1,v3}
\fmffreeze
\fmf{boson,label=$W$,label.side=left}{v3,v2,v1}
\fmf{dashes}{v2,o2}
\fmflabel{$q$}{i3}
\fmflabel{$q'$}{i1}
\fmflabel{$H$}{o2}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(140,80)
\fmfleft{i1,i3}
\fmfright{o1,o2,o3}
\fmf{fermion,tension=1.5}{i1,v1}
\fmf{fermion}{v1,o1}
\fmf{fermion,tension=1.5}{i3,v3}
\fmf{fermion}{v3,o3}
\fmf{phantom,tension=0.3}{v1,v3}
\fmffreeze
\fmf{boson,label=$Z$,label.side=left}{v3,v2,v1}
\fmf{dashes}{v2,o2}
\fmflabel{$q$}{i3}
\fmflabel{$q'$}{i1}
\fmflabel{$H$}{o2}
\end{fmfgraph*}
\caption{
\label{fig:intro:feyn_VBF}
Feynman diagrams for the VBF production}
\end{figure}


The $ttH$ production is expressed by the Feynman diagram in Figure \ref{fig:intro:feyn_tth}.
The cross-section of the $ttH$ production is calculated at NLO (QCD) and NLO (EW) accuracy \cite{deFlorian:2016spz,twikiCrossSections}.
The result is
\begin{equation}
\sigma_{ttH}=0.5065\unit{pb}^{+5.8\%}_{-9.2\%}(\text{scale})\pm 3.0\%(\text{PDF})\pm 2.0\%(\alpha_s).
\end{equation}


% tth
\begin{figure}[!bt]
\centering
\begin{fmfgraph*}(120,90)
    \fmfleft{d,i1,d,d,i3,d}
    \fmfright{o1,d,o2,d,o3}
    \fmf{gluon,tension=2}{i1,v1}
    \fmf{gluon,tension=2}{v3,i3}
    \fmf{fermion}{o1,v1}
    \fmf{fermion}{v3,o3}
    \fmf{phantom,tension=0.3}{v1,v3}
    \fmffreeze
    \fmf{fermion}{v1,v2,v3}
    \fmf{dashes,tension=1.3}{v2,o2}
    \fmflabel{$g$}{i3}
    \fmflabel{$g$}{i1}
    \fmflabel{$t$}{o3}
    \fmflabel{$\bar{t}$}{o1}
    \fmflabel{$H$}{o2}
  \end{fmfgraph*}
\caption{
\label{fig:intro:feyn_tth}
Feynman diagrams for the associated production with \ttbar ($ttH$).}
\end{figure}



The \VH productions are processes shown in Figure \ref{fig:intro:feyn_VH}.
The cross-section of the \WH production and the \qqZH contribution in the \ZH production are calculated at NNLO (QCD) and NLO (EW) accuracy \cite{deFlorian:2016spz,twikiCrossSections}.
The resulting values are
\begin{align}
& \sigma_{WH}=1.369\unit{pb}^{+0.5\%}_{-0.7\%}(\text{scale})\pm 1.7\%(\text{PDF})\pm 0.9\%(\alpha_s),\\
& \sigma_{\qqZH}=0.7612\unit{pb}^{+0.5\%}_{-0.6\%}(\text{scale})\pm 1.7\%(\text{PDF})\pm 0.9\%(\alpha_s).
\end{align}
The \ZH production is contributed by a loop-induced process \ggZH expressed by the right Feynman diagram in Figure \ref{fig:intro:feyn_VH}.
The cross-section of \ggZH is calculated at NLO+NLL (QCD) accuracy.
The value is
\begin{equation}
\sigma_{\ggZH}=0.1227\unit{pb}^{+25.1\%}_{-18.9\%}(\text{scale})\pm 1.8\%(\text{PDF})\pm 1.6\%(\alpha_s).
\end{equation}
The total \ZH cross-section is
\begin{equation}
\sigma_{ZH}=\sigma_{\qqZH}+\sigma_{\ggZH}=0.8839\unit{pb}^{+3.8\%}_{-3.1\%}(\text{scale})\pm 1.3\%(\text{PDF})\pm 0.9\%(\alpha_s).
\end{equation}

% VH
\begin{figure}[!bt]
\centering
 \begin{fmfgraph*}(110,55)
    \fmfleft{i1,i2}
    \fmfright{o1,o2}
    \fmf{fermion}{v1,i1}
    \fmf{fermion}{i2,v1}
    \fmf{boson,label=$W^*$,label.side=left}{v1,v2}
    \fmf{boson,label.side=left}{v2,o2}
    \fmf{dashes}{v2,o1}
    \fmflabel{$\bar{q}$}{i1}
    \fmflabel{$q$}{i2}
    \fmflabel{$W$}{o2}
    \fmflabel{$H$}{o1}
  \end{fmfgraph*}
\hspace{10mm}
 \begin{fmfgraph*}(110,55)
    \fmfleft{i1,i2}
    \fmfright{o1,o2}
    \fmf{fermion}{v1,i1}
    \fmf{fermion}{i2,v1}
    \fmf{boson,label=$Z^*$,label.side=left}{v1,v2}
    \fmf{boson,label.side=left}{v2,o2}
    \fmf{dashes}{v2,o1}
    \fmflabel{$\bar{q}$}{i1}
    \fmflabel{$q$}{i2}
    \fmflabel{$Z$}{o2}
    \fmflabel{$H$}{o1}
  \end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(150,50)
    \fmfstraight
    \fmfleft{i1,i2}
    \fmfright{o1,o2}
    % gluons
    \fmf{gluon}{i1,t1}
    \fmf{gluon}{t2,i2}
    \fmf{phantom,tension=0.92}{t1,t4}
    \fmf{phantom,tension=0.92}{t2,t3}
    % Higgs boson
    \fmf{dashes,tension=1}{t4,o1}
    \fmf{boson,tension=1}{t3,o2}
    % top loop
    \fmf{fermion,tension=0}{t1,t2,t3,t4,t1}
    % labels
    \fmflabel{$g$}{i1}
    \fmflabel{$g$}{i2}
    \fmflabel{H}{o1}
    \fmflabel{Z}{o2}
  \end{fmfgraph*}
\caption{
\label{fig:intro:feyn_VH}
Feynman diagrams for the associated production with a vector boson (\VH).}
\end{figure}


\subsubsection{Decay channels of the Higgs boson}


The mechanisms of Higgs decays are categorized into three groups.
The first group includes decays to a pair of fermions.
These decays occur via Yukawa couplings, and the corresponding Lagrangian term is Eq.~\ref{eq:intro:higgs_fermion}.
The Feynman diagrams are shown in Figure \ref{fig:intro:higgs_decay_fermion}.
The second group includes decays to four fermions with a $WW^*$ and $ZZ^*$ pair in the intermediate state.
These decays occur via the Higgs boson couplings to the gauge boson in Eq.~\ref{eq:intro:higgs_WZ},
and the Feynman diagrams are shown in Figure \ref{fig:intro:higgs_decay_vv}.
The third group contains decays to massless gauge bosons such as $\gamma\gamma$ and $gg$.
These decays are induced by a loop of fermions as shown in Figure \ref{fig:intro:higgs_decay_loop}.


Observables relevant to the decays are the branching ratios.
Methods for the calculation of the Higgs branching ratios are presented in Ref.~\cite{Dittmaier:2012vm} in detail.
The calculated branching ratios \cite{Garcia2150771} are shown in Figure \ref{fig:intro:HiggsBr} and Table \ref{tab:intro:HiggsBr}.


% h -> ff
\begin{figure}[!bt]
\centering
\begin{fmfgraph*}(75,75)
\fmfleft{i}
\fmfright{o1,o2}
\fmflabel{$H$}{i}
\fmflabel{$f$}{o1}
\fmflabel{$\bar{f}$}{o2}
\fmf{dashes,tension=2}{i,v}
\fmf{fermion}{v,o1}
\fmf{fermion}{o2,v}
\end{fmfgraph*}
\caption{
\label{fig:intro:higgs_decay_fermion}
Higgs decay to a pair of fermions}
\end{figure}


% h -> WW,ZZ
\begin{figure}[!bt]
\centering
  \begin{fmfgraph*}(100,100)
    \fmfstraight
    \fmfleft{i0,i1,i2}
    \fmfright{o1,o2,o3,o4}
    % fermions
    \fmf{fermion}{o1,v21,o2}
    \fmf{fermion}{o3,v22,o4}
    % phantoms to pull back fermion lines
    \fmf{phantom}{i0,v21}
    \fmf{phantom,tension=0.5}{v21,v22}
    \fmf{phantom}{i2,v22}
    \fmffreeze
    % HWW
    \fmf{dashes,tension=1.5}{i1,v1}
    \fmf{boson,label=$W^*$,label.side=right}{v1,v21}
    \fmf{boson,label=$W$,label.side=left}{v1,v22}
    \fmflabel{$H$}{i1}
  \end{fmfgraph*}
\hspace{10mm}
  \begin{fmfgraph*}(100,100)
    \fmfstraight
    \fmfleft{i0,i1,i2}
    \fmfright{o1,o2,o3,o4}
    % fermions
    \fmf{fermion}{o1,v21,o2}
    \fmf{fermion}{o3,v22,o4}
    % phantoms to pull back fermion lines
    \fmf{phantom}{i0,v21}
    \fmf{phantom,tension=0.5}{v21,v22}
    \fmf{phantom}{i2,v22}
    \fmffreeze
    % HWW
    \fmf{dashes,tension=1.5}{i1,v1}
    \fmf{boson,label=$Z$,label.side=right}{v1,v21}
    \fmf{boson,label=$Z^*$,label.side=left}{v1,v22}
    \fmflabel{$H$}{i1}
  \end{fmfgraph*}
\caption{
\label{fig:intro:higgs_decay_vv}
Higgs decays to a pair of vector bosons}
\end{figure}


% h -> yy,gg
\begin{figure}[!bt]
\centering
\begin{fmfgraph*}(110,55)
\fmfstraight
\fmfright{i1,i2}
\fmfleft{o1,H,o2}
% gluons
\fmf{boson}{i1,t1}
\fmf{boson}{t2,i2}
\fmf{phantom,tension=0.5}{t1,o1}
\fmf{phantom,tension=0.5}{t2,o2}
\fmffreeze
% W loop
\fmf{photon,tension=1}{t1,t2}
\fmf{photon,tension=1,label=$W^+$,label.side=right}{t2,t3}
\fmf{photon,tension=1,label=$W^-$,label.side=right}{t3,t1}
% Higgs boson
\fmf{dashes,tension=2.0}{t3,H}
% labels
\fmflabel{$\gamma$}{i1}
\fmflabel{$\gamma$}{i2}
\fmflabel{$H$}{H}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(110,55)
\fmfstraight
\fmfright{i1,i2}
\fmfleft{o1,H,o2}
% gluons
\fmf{boson}{i1,t1}
\fmf{boson}{t2,i2}
\fmf{phantom,tension=0.5}{t1,o1}
\fmf{phantom,tension=0.5}{t2,o2}
\fmffreeze
% top loop
\fmf{fermion,tension=1}{t1,t2,t3,t1}
% Higgs boson
\fmf{dashes,tension=2.0}{t3,H}
% labels
\fmflabel{$\gamma$}{i1}
\fmflabel{$\gamma$}{i2}
\fmflabel{$H$}{H}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(110,55)
\fmfstraight
\fmfright{i1,i2}
\fmfleft{o1,H,o2}
% gluons
\fmf{gluon}{t1,i1}
\fmf{gluon}{i2,t2}
\fmf{phantom,tension=0.5}{t1,o1}
\fmf{phantom,tension=0.5}{t2,o2}
\fmffreeze
% top loop
\fmf{fermion,tension=1}{t1,t2,t3,t1}
% Higgs boson
\fmf{dashes,tension=2.0}{t3,H}
% labels
\fmflabel{$g$}{i1}
\fmflabel{$g$}{i2}
\fmflabel{$H$}{H}
\end{fmfgraph*}
\caption{
\label{fig:intro:higgs_decay_loop}
Loop induced Higgs decay.}
\end{figure}



\SimpleGraphics(figures/SMHiggsBR-YR4-square.pdf){
\label{fig:intro:HiggsBr}
Theoretical calculation of the branching ratios of the Higgs boson as functions of the Higgs mass $M_h$ \cite{Dittmaier:2012vm}.
}

\SimpleTable(tables/HiggsBr.tex)[
Theoretical calculation of the branching ratios of the Higgs boson at the Higgs mass $M_h=125.09\GeV$ \cite{Dittmaier:2012vm}.
]{
\label{tab:intro:HiggsBr}
Theoretical calculation of the branching ratios of the Higgs boson \cite{Dittmaier:2012vm}.
These branching ratios are calculated with the Higgs mass $M_h=125.09\GeV$ \cite{Aad:2015zhl}.
The uncertainties indicated as ``theory'' are uncertainties due to the missing higher-order corrections.
The uncertainties indicated as ``$m_q$'' and ``\alphaS'' are due to the experimental uncertainties of these input parameters.
}
