


\section{Measurements of the properties of the Higgs boson assuming CP even scenarios}
\label{sec:Measurement_EFT}


\subsection{Effective Field Theories and model-independent measurement}

Production cross-sections and decay widths of particles are affected by extra interactions added to the Standard Model.
Since the nature of BSM is not known, it is useful to express the experimental results in a model-independent way.
Standard Model Effective Field Theory (SMEFT) \cite{Brivio:2017btx} can describe phenomena caused by a large variety of additional heavy fields, below the energy scale of the new physics.



\subsubsection{Formalism of the Standard Model Effective Field Theory}


The first part of the SMEFT consists of the renormalizable terms of the Standard Model Lagrangian constructed by the field operators with mass dimensions less than five.
The SMEFT adds operators of the SM fields with mass dimensions higher than four.
In the renormalization group picture, contributions of these higher dimensional operators get smaller in low energy phenomena.
Thus, these operators are referred to as \textit{irrelevant operators}.
The Lagrangian density of the SMEFT is written as:
\begin{equation}
{\cal L} = {\cal L}_{\text{SM}} + \frac{1}{\Lambda}\sum C_{i}^{(5)}Q_{i}^{(5)} + \frac{1}{\Lambda^2}\sum C_{i}^{(6)}Q_{i}^{(6)} + {\cal O}\left(\frac{1}{\Lambda^3}\right),
\label{eq:intro:SMEFTLagrangian}
\end{equation}
where $Q_{i}^{(d)}$ are field operators with mass dimension $d$ and $\Lambda$ is the energy scale of new physics, which is typically larger than the electroweak scale.
$C_{i}^{(d)}$ are dimensionless parameters associated to the dimension-$d$ operators, $Q_i^{(d)}$,
which are referred to as \textit{Wilson coefficients}.
Contributions from BSM appear in the irrelevant operators.
The Wilson coefficients specify phenomena at low energy.
$\Lambda$ can not be determined from the measurement of processes below the energy scale of new physics.
In other words, only $C^{(5)}/\Lambda$ and $C^{(6)}/\Lambda^2$ can be measured.


Since a model is specified by setting the Wilson coefficients,
cross-sections and branching ratios are also expressed as functions of the coefficients.
There are six operators which affect both \qqWH and \qqZH, and 7 operators which affect only \qqZH.
The scattering amplitude of \qqVH process shown in Figure \ref{fig:intro:feyn_VH} is dimensionless regardless of the Feynman diagram.
An amplitude that includes a dimension-6 interaction linearly depends on $C^{(6)}/\Lambda^2$ with the mass-dimension of -2 while the diagram by the Standard Model (SM) includes the electroweak gauge couplings ($g$, $g'$) and $m_V^2/v$.
By dimensional analysis, the amplitude by the dimension-6 term roughly depends on the momentum scale as $O(p^{2})$ compared to the SM amplitude whose dependence is roughly $O(p^{-1})$.
There is one subtlety in the argument above: the longitudinal component of a vector boson have the size of $E_V/m_V$ in the relativistic regime while the size of the transverse components remain $O(1)$.
Thus, the dependence of the scattering amplitudes on the momentum is different in different operators due to polarization of the vector boson.
Nevertheless, the cross-section of the \VH processes in the high \ptv phase space can show significant effects from these dimension-6 terms in the Lagrangian.
Thus, the goal of this thesis is to derive limits on the Wilson coefficients, especially for dimension-6 operators,
by interpreting the measurements of the differential cross-section of the \VH production.


Choice of how to expand dimension-6 interactions in the effective Lagrangian (\textit{basis}) is not unique.
Basis used in this thesis are called \textit{Warsaw basis} \cite{Grzadkowski:2010es},
which respects the gauge symmetry of the Standard Model (\SMGauge).
However, another basis, \textit{Strongly Interacting Light Higgs boson (SILH) basis} \cite{Contino:2013kra}, is often found in literature.
The SILH formalism expands in terms of the fields that diagonalize the mass matrix.


\subsection{Heavy Vector Triplet and its UV completed theories}


To explain that the limits on the Wilson coefficients serve as limits on an actual model, explicit models are described below.


An example of BSM models that can be described in the SMEFT framework is the \textit{Heavy Vector Triplet (HVT) model} \cite{Pappadopulo:2014qza},
which introduces real vector fields $V_{\mu}^{a}$ ($a=1,2,3$) in the adjoint representation of $SU(2)_L$.
The Lagrangian of HVT is written as:
\begin{align}
{\cal L}=
&-\frac{1}{4}D_{[\mu}V_{\nu]}^{a}D^{[\mu}V^{\nu]a}+\frac{m_V^2}{2}V_{\mu}^{a}V^{\mu a}\nonumber\\
&+ig_Vc_HV_{\mu}^{a}H^{\dagger}\tau^{a}\overleftrightarrow{D}^{\mu}H+\frac{g^2}{g_V}c_FV_{\mu}^{a}\sum_{f}\bar{f}_{L}\gamma^{\mu}\tau^{a}f_{L}\nonumber\\
&+\frac{g_V}{2}c_{VVV}\epsilon_{abs}V_{\mu}^aV^{\mu a}D^{[\mu}V^{\nu]c}
+g_V^2c_{VVHH}V_{\mu}^{a}V^{\mu a}H^{\dagger}H
-\frac{g}{2}c_{VVW}\epsilon_{abc}W^{\mu\nu a}V_{\mu}^bV_{\nu}^c,
\label{eq:intro:HVTLagrangian}
\end{align}
where the bracket means the anti-commutation with respect to the Lorentz indices:
\begin{equation}
D_{[\mu}V_{\nu]}^a \equiv D_{\mu}V_{\nu}^{a}-D_{\nu}V_{\mu}^{a},\;\;\;\;\;
D_{\mu}V_{\nu}^a \equiv \partial_{\mu}V_{\nu}^a+g\epsilon^{abc}W_{\mu}^{b}V_{\nu}^{c},
\end{equation}
$\tau^a$ are the half of the Pauli matrices: $\tau^a=\sigma^a/2$.
The arrow assigned to the covariant derivative represents Hermitian derivative as:
\begin{equation}
iH^{\dagger}\tau^{a}\overleftrightarrow{D}^{\mu}H \equiv
iH^{\dagger}\tau^{a}(D^{\mu}H) - i(D^{\mu}H)^{\dagger}\tau^{a}H.
\end{equation}
The heavy vector resonances $V_{\mu}^{a}$ are mixed with the SM vector bosons, $W^{\pm}$ and $Z$, through the term associated the $c_{H}$ in Eq.~\ref{eq:intro:HVTLagrangian}.
However, this mixing is small when the mass hierarchy, $m_{W}/m_{V}<10^{-1}$, is imposed \cite{Pappadopulo:2014qza}\footnote{This is presented for $g_{V}$ and $c_{H}$ of $O(1)$ in Ref.~\cite{Pappadopulo:2014qza}. This picture may be altered for the stronger couplings.}.
Therefore, the mixing is ignored in this analysis.
%% The vector $J^{a}_{F}$ represents the current of the left-handed SM fermions:
%% \begin{equation}
%% J_{F}^{a\mu} \equiv \sum_{f}\bar{f}_{L}\gamma^{\mu}\tau^{a}f_{L}.
%% \end{equation}


%% In the SILH basis, interactions of the Higgs boson with the vector bosons are described with the terms in Lagrangian:
%% \begin{align}
%% {\cal L}_{hVV} = \frac{h}{v}&\left[
%% (1+\delta c_{w})\frac{g^2v^2}{2}W_{\mu}^{+}W^{-\mu}
%% +(1+\delta c_{z})\frac{(g^2+g'^{2})_v^2}{2}Z_{\mu}Z^{\mu} \right.\nonumber\\
%% &+c_{ww}\frac{g^2}{2}W_{\mu\nu}^{+}W^{-\mu\nu}
%% +\widetilde{c}_{ww}\frac{g^2}{2}W_{\mu\nu}^{+}\widetilde{W}^{-\mu\nu}
%% +c_{w\Box}g^2\left(W_{\mu}^{-}\partial_{\nu}W^{+\mu\nu}+\text{h.c.}\right)\nonumber\\
%% &+c_{gg}\frac{g_s^2}{4}G_{\mu\nu}^{a}G^{a\mu\nu}
%% +c_{\gamma\gamma}\frac{e^2}{4}A_{\mu\nu}A^{\mu\nu}
%% +c_{z\gamma}\frac{e\sqrt{g^2+g'^{2}}}{2}Z_{\mu\nu}A^{\mu\nu}
%% +c_{zz}\frac{g^2+g'^{2}}{2}Z_{\mu\nu}Z^{\mu\nu}\nonumber\\
%% &+c_{z\Box}g^2Z_{\mu}\partial_{\nu}Z^{\mu\nu}
%% +c_{\gamma\Box}gg'Z_{\mu}\partial_{\nu}A^{\mu\nu}\nonumber\\
%% &\left.+\widetilde{c}_{gg}\frac{g_s^2}{4}G_{\mu\nu}^{a}\widetilde{G}^{a\mu\nu}
%% +\widetilde{c}_{\gamma\gamma}\frac{e^2}{4}A_{\mu\nu}\widetilde{A}^{\mu\nu}
%% +\widetilde{c}_{z\gamma}\frac{e\sqrt{g^2+g'^{2}}}{2}Z_{\mu\nu}\widetilde{A}^{\mu\nu}
%% +\widetilde{c}_{zz}\frac{g^2+g'^{2}}{2}Z_{\mu\nu}\widetilde{Z}^{\mu\nu}\right],
%% \label{eq:intro:SILH_HVV}
%% \end{align}
%% where the anti-symmetric tensors are defined as $V_{\mu\nu} = \partial_{\mu}V_{\nu}-\partial_{\nu}V_{\mu}$ and $\widetilde{V}_{\mu\nu} = \epsilon_{\mu\nu\alpha\beta}V^{\alpha\beta}$.
%% The Yukawa and gauge couplings of the fermions are described as
%% \begin{align}
%% {\cal L}_{hvff} = \sqrt{2}&g\frac{h}{v}W_{\mu}^{+}\left(
%% \bar{u}_{L}\gamma^{\mu}\delta g_{L}^{hWq}d_L+\bar{u}_{R}\gamma^{\mu}\delta g_{R}^{hWq}d_R+\bar{\nu}_{L}\gamma^{\mu}\delta g_{R}^{hW\ell}e_L\right)+\text{h.c.}
%% \nonumber\\
%% &+2\frac{h}{v}\sqrt{g^2+g'^2}Z_{\mu}\left[
%% \sum_{f=u,d,e,\nu}\bar{f}_L\gamma^{\mu}\delta g_L^{hZf}f_L
%% +\sum_{f=u,d,e}\bar{f}_R\gamma^{\mu}\delta g_R^{hZf}f_R\right].
%% \label{eq:intro:SILH_hvff}
%% \end{align}
The HVT model introduces additional contribution to the \VH production through the diagrams in Figure \ref{fig:intro:HVT_corr_VH} that affect Wilson coefficients.
In order to evaluate the contribution to the \VH production,
\begin{align}
ig_{V}c_{H}V_{\mu}^{a}(H^{\dagger}\tau^{a}\overleftrightarrow{D}^{\mu}H)
 = ig_{V}c_{H}V_{\mu}^{a}&\left[
(\tau^{a}H)^{\dagger}\partial_{\mu}H-(\partial_{\mu}H)^{\dagger}\tau^{a}H\right.\nonumber\\
&\left.-i\frac{g}{2}W_{\mu}^{a}H^{\dagger}H-ig'B_{\mu}H^{\dagger}\tau^{a}H\right].
\end{align}
In the second line, $\{\tau^a,\tau^{b}\}=\frac{1}{2}\delta^{ab}$ is used.
The second line is further transformed as:
\begin{align}
ig_{V}c_{H}V_{\mu}^{a}\left[-i\frac{g}{2}W_{\mu}^{a}H^{\dagger}H-ig'B_{\mu}H^{\dagger}\tau^{a}H\right]
&= g_{V}c_{H}\frac{(\vev+h)^2}{2}\left[\frac{g}{2}V_{\mu}^{a}W^{a\mu}+\frac{g'}{2}V_{\mu}^{3}B^{\mu}\right]\nonumber\\
%&= gg_{V}c_{H}\frac{(v+h)^2}{2}\left[\frac{g}{2}\left(V_{\mu}^{+}W^{-\mu}+V_{\mu}^{-}W^{+\mu}+V_{\mu}^{3}W^{3\mu}\right)+\frac{g'}{2}V_{\mu}^{3}B^{\mu}\right]\nonumber\\
&= g_{V}c_{H}\frac{(\vev+h)^2}{2}\left[\frac{g}{2}V_{\mu}^{+}W^{-\mu}+\frac{g}{2}V_{\mu}^{-}W^{+\mu}+\frac{\sqrt{g^2+g'^{2}}}{2}V_{\mu}^{3}Z^{\mu}\right],
\label{eq:theory:HVT_VWH}
\end{align}
where $V^{\pm}=(V^{1}\mp iV^{2})/\sqrt{2}$. In the first line, the terms including the Goldstone bosons are dropped.
For the interaction between the quarks and the heavy vector fields,
\begin{align}
\frac{g^2}{g_V}c_{F}\bar{q}_{L}\gamma^{\mu}\tau^{a}q_{L}V_{\mu}^{a}
&=\frac{g^2}{g_V}c_{F}\bar{q}_{L}\gamma^{\mu}\left(
\sqrt{2}\tau^{+}V_{\mu}^{+}
+\sqrt{2}\tau^{-}V_{\mu}^{-}
+\tau^{3}V_{\mu}^{3}
\right)q_{L}\nonumber\\
&=\frac{g^2}{g_V}c_{F}\left[
\frac{1}{\sqrt{2}}\bar{d}_{L}\gamma^{\mu}u_{L}V_{\mu}^{-}
+\frac{1}{\sqrt{2}}\bar{u}_{L}\gamma^{\mu}d_{L}V_{\mu}^{+}
+\frac{1}{2}\left(\bar{u}_{L}\gamma^{\mu}u_{L}-\bar{d}_{L}\gamma^{\mu}d_{L}\right)V_{\mu}^{3}
\right].
\label{eq:theory:HVT_qqV}
\end{align}
By combining Eqs.~\ref{eq:theory:HVT_VWH} and \ref{eq:theory:HVT_qqV}, Feynman diagrams on the left of Figure \ref{fig:intro:HVT_corr_VH} are obtained.
The scattering amplitudes represented by these Feynman diagrams are approximated by the amplitudes indicated by the Feynman diagrams on the right of the figure when the momentum $p$ is much lower than the mass of the heavy resonance.


In the Warsaw basis, interactions which include two quarks, the gauge boson, and the Higgs boson are parametrized as:
\begin{align}
\mathcal{L}
&= \frac{c_{Hq}^{(3)}}{\Lambda^2}(H^{\dagger}i\tau^{a}\overleftrightarrow{D}_{\mu}H)(\bar{q}_L\tau^{a}\gamma^{\mu}q_L)
+\frac{c_{Hq}^{(1)}}{\Lambda^2}(H^{\dagger}i\overleftrightarrow{D}_{\mu}H)(\bar{q}_L\gamma^{\mu}q_L)\nonumber\\
&+\frac{c_{Hu}}{\Lambda^2}(H^{\dagger}i\overleftrightarrow{D}_{\mu}H)(\bar{u}_R\gamma^{\mu}u_R)
+\frac{c_{Hd}}{\Lambda^2}(H^{\dagger}i\overleftrightarrow{D}_{\mu}H)(\bar{d}_R\gamma^{\mu}d_R)
+\cdots.
\end{align}
To match the HVT model to the EFT description, the following must be satisfied:
\begin{align}
&\frac{c_{Hq}^{(3)}}{\Lambda^2} = -\frac{g^2c_{H}c_{F}}{m_V^2} = -\frac{4g_{H}g_{q}}{m_V^2},\\
&c_{Hq}^{(1)} = c_{Hu} = c_{Hd} = 0.
\end{align}
Here, $g_H\equiv g_Vc_{H}/2$ and $g_q\equiv g^2c_{F}/2g_{V}$.
This can also be written as:
\begin{align}
\mathcal{L}
=-\frac{g_{H}g_{q}}{2m_V^2}
&\left\{
2\sqrt{2}\vev hg\left[\bar{d}_{L}i\gamma^{\mu}u_{L}W_{\mu}^{-}
+\bar{u}_{L}i\gamma^{\mu}d_{L}W_{\mu}^{+}\right]\right.\nonumber\\
&\left.+2\vev h\sqrt{g^2+g'^2}\left[\bar{u}_{L}i\gamma^{\mu}u_{L}-\bar{d}_{L}i\gamma^{\mu}d_{L}\right]Z_{\mu}
\right\}.
\end{align}
This agrees with the result in Ref.~\cite{deFlorian:2016spz}.


The following combinations of the parameters reduce to the same SMEFT and show the same phenomena at low energy as in the next paragraph.
\begin{itemize}
\item[-] Strongly coupled: $m_V=7\TeV$, $g_{H}=-g_q=1.75$
\item[-] Moderately coupled: $m_V=2\TeV$, $g_{H}=-g_q=0.5$
\item[-] Weakly coupled: $m_V=1\TeV$, $g_{H}=-g_q=0.25$
\end{itemize}
Thus, a measurement of anomalous interactions has sensitivities to various ranges of parameters and is interpreted as an \textit{indirect search} for BSM.

%% \begin{figure}[!bt]
%% \centering
%% \begin{fmfgraph*}(100,100)
%% \fmfleft{i}
%% \fmfright{o}
%% \fmflabel{$W,Z$}{i}
%% \fmflabel{$W,Z$}{o}
%% \fmf{photon,tension=3}{i,v1}
%% \fmf{photon,left=1,label=$V$}{v1,v2}
%% \fmf{dashes,left=1,label=$H$}{v2,v1}
%% \fmf{photon,tension=3}{v2,o}
%% \end{fmfgraph*}
%% \hspace{20mm}
%% \begin{fmfgraph*}(100,100)
%% \fmfleft{i}
%% \fmfright{o}
%% \fmflabel{$W,Z$}{i}
%% \fmflabel{$W,Z$}{o}
%% \fmf{photon,tension=3}{i,v1}
%% \fmf{photon,left=1,label=$V$}{v1,v2}
%% \fmf{photon,left=1,label=$W,,Z$}{v2,v1}
%% \fmf{photon,tension=3}{v2,o}
%% \end{fmfgraph*}
%% \caption[
%% Radiative corrections to the propagator of a vector boson $W$, $Z$ in the HVT model.
%% ]{\label{fig:intro:HVT_corr_Z_propagator}
%% Radiative correction to the propagator of a vector boson $W$, $Z$ in the HVT model.
%% While $W$, $Z$ represents the gauge boson of the Standard Model, $V$ represents the heavy vector field.}
%% \end{figure}

% Symmetric phase
%% \begin{figure}[!bt]
%% \centering
%% \vspace{5mm}
%% \begin{fmfgraph*}(100,60)
%% \fmfleft{i1,i2}
%% \fmfright{o1,o2}
%% \fmflabel{$q$}{i1}
%% \fmflabel{$\bar{q}'$}{i2}
%% \fmflabel{$H$}{o1}
%% \fmflabel{$W^{a}$}{o2}
%% \fmf{fermion}{i1,v1,i2}
%% \fmf{photon,label=$V^{a}$}{v1,v2}
%% \fmf{dashes}{v2,o1}
%% \fmf{photon}{v2,o2}
%% \end{fmfgraph*}
%% \hspace{20mm}
%% \begin{fmfgraph*}(1,60)
%% \fmfleft{i}
%% \fmflabel{$\longrightarrow$}{i}
%% \end{fmfgraph*}
%% \hspace{10mm}
%% \begin{fmfgraph*}(80,60)
%% \fmfleft{i1,i2}
%% \fmfright{o1,o2}
%% \fmflabel{$q$}{i1}
%% \fmflabel{$\bar{q}'$}{i2}
%% \fmflabel{$H$}{o1}
%% \fmflabel{$W^{a}$}{o2}
%% \fmf{fermion}{i1,v,i2}
%% \fmf{dashes}{v,o1}
%% \fmf{photon}{v,o2}
%% \fmfv{decor.shape=circle,decor.filled=full,decor.size=7}{v}
%% \end{fmfgraph*}
%% \vspace{10mm}\\
%% \begin{fmfgraph*}(100,60)
%% \fmfleft{i1,i2}
%% \fmfright{o1,o2}
%% \fmflabel{$q$}{i1}
%% \fmflabel{$\bar{q}'$}{i2}
%% \fmflabel{$H$}{o1}
%% \fmflabel{$B$}{o2}
%% \fmf{fermion}{i1,v1,i2}
%% \fmf{photon,label=$V^{3}$}{v1,v2}
%% \fmf{dashes}{v2,o1}
%% \fmf{photon}{v2,o2}
%% \end{fmfgraph*}
%% \hspace{20mm}
%% \begin{fmfgraph*}(1,60)
%% \fmfleft{i}
%% \fmflabel{$\longrightarrow$}{i}
%% \end{fmfgraph*}
%% \hspace{10mm}
%% \begin{fmfgraph*}(80,60)
%% \fmfleft{i1,i2}
%% \fmfright{o1,o2}
%% \fmflabel{$q$}{i1}
%% \fmflabel{$\bar{q}'$}{i2}
%% \fmflabel{$H$}{o1}
%% \fmflabel{$B$}{o2}
%% \fmf{fermion}{i1,v,i2}
%% \fmf{dashes}{v,o1}
%% \fmf{photon}{v,o2}
%% \fmfv{decor.shape=circle,decor.filled=full,decor.size=7}{v}
%% \end{fmfgraph*}
%% \vspace{5mm}
%% \caption[
%% Contribution from the HVT model to the \WH and \ZH production.
%% ]{\label{fig:intro:HVT_corr_VH}
%% Contribution from the HVT model to the \WH and \ZH production.
%% While $W$, $Z$ represents the gauge boson of the Standard Model, $V$ represents the heavy vector field.}
%% \end{figure}

% Broken phase
\begin{figure}[!bt]
\centering
\vspace{5mm}
\begin{fmfgraph*}(100,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$u$}{i1}
\fmflabel{$\bar{d}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$W^{+}$}{o2}
\fmf{fermion}{i1,v1,i2}
\fmf{photon,label=$V^{+}$,label.side=left}{v1,v2}
\fmf{dashes}{v2,o1}
\fmf{photon}{v2,o2}
\fmf{phantom,label=$\frac{-ig_{\mu\nu}}{p^2-m_{V}^{2}}$,label.side=right,tension=0}{v1,v2}
\fmflabel{$i\frac{g^2c_{F}}{\sqrt{2}g_{V}}\;\;\;$}{v1}
\fmflabel{$\;\;\;i\frac{\vev g}{2}g_{V}c_{H}$}{v2}
\end{fmfgraph*}
\hspace{30mm}
\begin{fmfgraph*}(1,60)
\fmfleft{i}
\fmflabel{$\longrightarrow$}{i}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(80,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$u$}{i1}
\fmflabel{$\bar{d}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$W^{+}$}{o2}
\fmf{fermion}{i1,v,i2}
\fmf{dashes}{v,o1}
\fmf{photon}{v,o2}
\fmfv{decor.shape=circle,decor.filled=full,decor.size=7,label=$\;\;\;-i\frac{\vev g^3c_{H}c_{F}}{2\sqrt{2}m_{V}^2}$}{v}
\end{fmfgraph*}
\vspace{10mm}\\
\begin{fmfgraph*}(100,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$d$}{i1}
\fmflabel{$\bar{u}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$W^{-}$}{o2}
\fmf{fermion}{i1,v1,i2}
\fmf{photon,label=$V^{-}$,label.side=left}{v1,v2}
\fmf{dashes}{v2,o1}
\fmf{photon}{v2,o2}
\fmf{phantom,label=$\frac{-ig_{\mu\nu}}{p^2-m_{V}^{2}}$,label.side=right,tension=0}{v1,v2}
\fmflabel{$i\frac{g^2c_{F}}{\sqrt{2}g_{V}}\;\;\;$}{v1}
\fmflabel{$\;\;\;i\frac{\vev g}{2}g_{V}c_{H}$}{v2}
\end{fmfgraph*}
\hspace{30mm}
\begin{fmfgraph*}(1,60)
\fmfleft{i}
\fmflabel{$\longrightarrow$}{i}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(80,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$d$}{i1}
\fmflabel{$\bar{u}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$W^{-}$}{o2}
\fmf{fermion}{i1,v,i2}
\fmf{dashes}{v,o1}
\fmf{photon}{v,o2}
\fmfv{decor.shape=circle,decor.filled=full,decor.size=7,label=$\;\;\;-i\frac{\vev g^3c_{H}c_{F}}{2\sqrt{2}m_{V}^2}$}{v}
\end{fmfgraph*}
\vspace{10mm}\\
\begin{fmfgraph*}(100,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$u,d$}{i1}
\fmflabel{$\bar{u},\bar{d}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$Z$}{o2}
\fmf{fermion}{i1,v1,i2}
\fmf{photon,label=$V^{3}$,label.side=left}{v1,v2}
\fmf{dashes}{v2,o1}
\fmf{photon}{v2,o2}
\fmf{phantom,label=$\frac{-ig_{\mu\nu}}{p^2-m_{V}^{2}}$,label.side=right,tension=0}{v1,v2}
\fmflabel{$i\frac{g^2c_{F}}{2g_{V}}\;\;\;$}{v1}
\fmflabel{$\;\;\;i\frac{\vev\sqrt{g^2+g'^2}}{2}g_{V}c_{H}$}{v2}
\end{fmfgraph*}
\hspace{30mm}
\begin{fmfgraph*}(1,60)
\fmfleft{i}
\fmflabel{$\longrightarrow$}{i}
\end{fmfgraph*}
\hspace{10mm}
\begin{fmfgraph*}(80,60)
\fmfleft{i1,i2}
\fmfright{o1,o2}
\fmflabel{$u,d$}{i1}
\fmflabel{$\bar{u},\bar{d}$}{i2}
\fmflabel{$H$}{o1}
\fmflabel{$Z$}{o2}
\fmf{fermion}{i1,v,i2}
\fmf{dashes}{v,o1}
\fmf{photon}{v,o2}
\fmfv{decor.shape=circle,decor.filled=full,decor.size=7,label=$\;\;\;-i\frac{\vev g^2\sqrt{g^2+g'^2}c_{H}c_{F}}{4m_{V}^2}$}{v}
\end{fmfgraph*}
\vspace{5mm}
\caption[
Contribution from the HVT model to the \WH and \ZH production.
]{\label{fig:intro:HVT_corr_VH}
Contribution from the HVT model to the \WH and \ZH production.
While $W$ and $Z$ represent the gauge bosons of the Standard Model, $V$ represents the heavy vector field.
The amplitudes are approximated by the ones obtained from the Feynman diagram on the right when $p\ll m_V$.}
\end{figure}


These additional terms affect the production of the Higgs boson with a vector boson (\VH) in the phase space with the high transverse momentum of $V$ (\ptv).
This is shown in Figure \ref{fig:intro:EFTSpectra}.
Thus, Wilson coefficients corresponding to these models are studied through the measurement of the differential cross-section of the \VH production as functions of \ptv.

Direct searches for the heavy resonance $V$ have been performed in final states with two SM weak bosons using the 139\ifb data of ATLAS, and have excluded these resonances below 3-5 TeV \cite{Aad:2019fbh,Aad:2020ddw}.
From the discussions above, the higher mass region can be searched if the coupling is sufficiently strong by the indirect search through the differential cross-section measurement of the \VH production.

\SimpleGraphics[100mm](figures/EFTSpectrum.pdf)[
Differential partonic cross-sections of the \WH production as a function of the center-of-mass energy of the initial partons predicted by the HVT models and corresponding effective field theories from Ref.~\cite{deFlorian:2016spz}.
]{
\label{fig:intro:EFTSpectra}
Differential partonic cross-sections of the \WH production as a function of the center-of-mass energy of the initial partons predicted by HVT models and corresponding effective field theories.
The black curves are the predictions from the HVT model with
$m_V=1\TeV$, $g_{H}=-g_q=0.25$ (dashed),
$m_V=2\TeV$, $g_{H}=-g_q=0.5$ (dotted) and
$m_V=7\TeV$, $g_{H}=-g_q=1.75$ (solid).
The red curve represents the EFT prediction when considering only the terms of the cross-section linear in Wilson coefficients.
The purple curve represents the same effective theory but considering the quadratic and linear terms.
The plot is taken from \cite{deFlorian:2016spz}.
}


\subsubsection{Composite Higgs model}


The HVT model can also be an effective description of another theory.

An example is the \textit{Minimum Composite Higgs Model (MCHM)}, which is shortly described below.
The MCHM has a $SO(5)$ global symmetry that is broken to the $SO(4)$ subgroup by a strongly interacting sector \cite{Contino:2011np}.
The lightest vector resonances in the MCHM are described by the HVT model \cite{Pappadopulo:2014qza}.
The four pseudo Nambu-Goldstone bosons (PNGB) corresponding to the preserving $SO(4)$ symmetry form a complex doublet of $SU(2)_L$.
A loop correction involving the top quark generates a potential for the PNGBs giving a VEV to these bosons.
Then, the EWSB takes place as $SO(4)\to SO(3)$, and one remaining PNGB is identified as the Higgs boson.
The mass of the Higgs boson is protected by the approximate $SO(5)$ symmetry from large radiative corrections \cite{Agashe:2004rs}.
Hence, the hierarchy problem of the Higgs boson is solved.

%% This model introduces additional gauge interaction and fermions that carry the charges of the new gauge group and $SU(2)_L$.
%% The lightest vector resonances in the MCHM are described by the HVT model \cite{Pappadopulo:2014qza}.
%% If the new gauge interaction has \textit{asymptotic freedom}, and the coupling constant is order 1 at the electroweak scale,
%% a pair of the new fermion condensates in the vacuum like in QCD.
%% Since the new fermion also has the $SU(2)_L$ charge, this vacuum breaks the electroweak symmetry.
%% That is an interesting possibility of the nature of the \textit{electroweak symmetry breaking}.
%% Since this model has the asymptotic freedom, the scalar is resolved into fermions.
%% The cut-off for the loop integral in Figure \ref{fig:intro:rad_corr_higgs_propagator} is set, hence the hierarchy problem of the Higgs boson is solved.

%% While the composite Higgs model generates the mass of the gauge bosons,
%% it is difficult to explain the Yukawa coupling with a renormalizable interaction,
%% and a mechanism is needed to overcome issues.
%% Since the nature of BSM is not known yet, it should be considered as a candidate of BSM.



\subsubsection{Extended gauge symmetry}

The gauge coupling of the HVT Lagrangian can also be weak.
Although it may be less attractive than the composite Higgs model, it is still a possible BSM scenario.
This effective model arises when a symmetry breaking pattern, $SU(2)_1\times SU(2)_2\times U(1)\to SU(2)_L\times U(1)_{Y}$, realizes \cite{Pappadopulo:2014qza,Barger:1980ix}.



%\subsection{Measurements of the couplings the Higgs boson}


\subsubsection{Validity of the Effective Field Theory}


In order to think about the validity of the effective description truncated at the dimension-6, dimension-8 contribution is discussed below following the argument in Ref.~\cite{deFlorian:2016spz}.
Here, the dimension-8 contribution from the following expansion of the propagator of the heavy resonance is taken as an example:
\begin{equation}
\frac{-ig^{\mu\nu}}{p^2-m_V^2} = \frac{ig^{\mu\nu}}{m_V^2}-\frac{ig^{\mu\nu}}{m_V^2}\frac{p^2}{m_V^2}+O\left(\left(\frac{p}{m_V}\right)^4\right).
\end{equation}
In the HVT model, the coefficient of the second terms is $g_{H}g_{q}$ similar to the dimension-6 term.
The amplitude of the SM contribution scales as $g_{\text{SM}}$ (=$g$ or $g'$)
whereas the amplitude by the dimension-6 operator scales as $g_{H}g_{q}\times p^2/\Lambda^2$.
When $g_{H}g_{q}$ is large compared to $g_{\text{SM}}^2$, equivalently $\Lambda$ is large,
there is an energy range that the \VH process is dominated by the dimension-6 operator while the EFT description is still valid:
\begin{equation}
g_{H}g_{q} p^2/\Lambda^2 > g_{\text{SM}}^2
\Longleftrightarrow
\Lambda\times g_{\text{SM}}^2/g_{H}g_{q}<p^2<\Lambda.
\end{equation}
In this kinematic region,
the dimension-6 term in the cross-section, $\abs{\mathcal{M}_{\text{dim-6}}}^2$, is relevant
even though this term includes the factor $\Lambda^{-4}$.
On the other hand, the dimension-8 contribution to the cross-section with $\Lambda^{-4}$ comes from the interference between the SM and dimension-8 operators, $2\mathrm{Re}\left[\mathcal{M}_{\text{SM}}\mathcal{M}_{\text{dim-8}}^*\right]$.
For the example above, this term scales as $g_{\text{SM}}^2g_{H}g_{q}\times p^4/\Lambda^4$ and is suppressed by a factor $g_{\text{SM}}^2/g_{H}g_{q}$ compared to $\abs{\mathcal{M}_{\text{dim-6}}}^2$.
Therefore, the approximation becomes good when the energy scale of the BSM is high even if the dim-6 term is the same.

