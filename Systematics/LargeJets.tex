
The uncertainties for large-$R$ jets from jet energy scale (JES), jet energy resolution (JER), jet mass scale (JMS), and jet mass resolution (JMR) uncertainties are considered.
The estimation of the uncertainties is described in Ref.\cite{JETM-2018-02}.

\paragraph*{Jet Energy Scale (JES)}
We use ``in-situ'' methods to correct residual difference between the data and MC samples, as described in Section~\ref{sec:reco:largeR_calib}.
In this method, three control samples, \Zjets, \gammajets, and multi-jet, are used to estimate the JES uncertainty depending on the \pt of the jet; \Zjets and \gammajets events are used for calibrating large-$R$ jets, which have $\pt \geq 200\GeV$ and $|\eta| < 0.8$, and multi-jets events are used to calibrate higher \pt large-$R$ jets ($\pt \geq 300\GeV$ and $|\eta| < 0.8$).
The uncertainties in the in-situ calibration are shown in Figure~\ref{fig:sys:large_jes}.
The differences of the calibration factors between MC samples using different generators and a combination of three measurements are assigned as systematic uncertainties.

An additional uncertainty depending on the flavor of large-$R$ jets is also considered.
This additional uncertainty is zero for gluon- and quark-initiated jets and non-zero for large-$R$ jets originating from $W/Z$ boson and top quarks.
The total uncertainties for the large-$R$ jets are shown in Figure~\ref{fig:sys:large_jer_sys}.
%Systematic uncertainties of flavor and topologies depend on the origin of large-$R$ jets.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[Gluon- and quark-initiated jets]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_jes_sys_qcd}}
\subfigure[Jets resulting from hadronically decaying $W/Z$ boson]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_jes_sys_WZ}}
\caption[Systematic uncertainties of the large-$R$ jet energy scale.]{
The uncertainty as a function of jet \pt of gluon- and quark-initiated jets (a) and jets resulting from hadronically decaying $W/Z$ boson (b).
Flavor uncertainties are propagated from small-$R$ jets.
``Relative \textit{in situ} JES'' are uncertainties from the $\eta$ inter-calibration, and ``Absolute \textit{in situ} JES'' includes all uncertainties of in-situ measurement in the central region.
Total uncertainties are determined as the quadruple sum of each component~\cite{JetETMissPublicResults}.
}
\label{fig:sys:large_jer_sys}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\paragraph*{Jet Energy Resolution (JER)}
%Uncertainties on the energy resolution of large-$R$ jets are evaluated in the same method as the study of the small-$R$ jets.
The jet energy resolution is measured in two $\eta$ regions; one in the central region ($|\eta| < 0.8$) and the other in the forward region ($0.8 < |\eta| < 2.0$).
An absolute 2\% uncertainty for jet energy resolution estimated by the differences of the width in the truth and reconstructed jet energy distributions is assigned.






\paragraph*{Jet Mass Scale (JMS) and Jet Mass Resolution (JMR)}
%Jet mass scale and resolution uncertainty is covered by boson tagging efficiency uncertainty.
%We need not consider them for $J_{qq}$.
%We consider it for only $J_{bb}$.

%http://cdsweb.cern.ch/record/2631339/files/ATL-PHYS-PUB-2018-015.pdf, https://indico.cern.ch/event/850543/contributions/3574974/attachments/1916016/3167676/JSS_-_September_26_2019.pdf,https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21Moriond2018LargeR}
For the jet mass resolution uncertainty, the jet mass response, defined as the ratio of the reconstructed mass to the true jet mass ($\mathcal{R} = m^{\textrm{reco}}/m^{\textrm{true}}$) is measured in the MC based study.
The measurement method is described in Ref.\cite{ATL-PHYS-PUB-2018-015}.
%A relative 20\% uncertainty of the jet mass as a Gaussian is assigned for the jet mass resolution uncertainty.
A relative 20\% uncertainty extracted from the jet mass distribution using a Gaussian width is assigned.

%One method is to measure the average calorimeter-to-tracker response defined as
%\begin{equation}
%R_{textrm{trk}}^{m}=\Braket{\frac{m^{\textrm{calo}}}{m^{\textrm{track}}}}
%\end{equation}
%where  the superscript of $m$ represents the detector to measure $m$, i.e. $m^{\textrm{calo}}$ is measured mass using topological clusters of calorimeter cells and $m^{\textrm{track}}$ is a track jet mass which matches to the same jet.
%This method is called as $R_{\textrm{trk}}$ method.
%We use dijet events in this study.
%And we consider generator difference and three different types of mis-modelling for tracking (efficiency, fake rates and $q/\pt$ bias) as sources of uncertainty.
%The results are shown in Figure~\ref{fig:sys:large_jms_Rtrk}.
%In higher \pt region, the double ratio of $R_{textrm{trk}}^{m}$ measure in data and MC (\PYTHIA 8) is small, and this is dominat source of uncertainty.
The jet mass scale is estimated in the $R_{\textrm{trk}}$ method, as described in Section~\ref{sec:reco:largeR_calib}.
In this method, three generators are used to estimate the theoretical uncertainties, and three types of mis-modeling for tracking (efficiency, fake rates, and $q/\pt$ bias) as sources of uncertainties are considered.
The systematic uncertainty evaluated in the $R_{\textrm{trk}}$ method is shown in Figure~\ref{fig:sys:large_jms_Rtrk_sys}.
In the higher \pt region, the double ratio of MC samples (\PYTHIA 8) to data in $R_{\textrm{trk}}^{m}$ is $\sim 0.96$.
The deviation from unity is assigned as a systematic uncertainty and is shown as a red line.
This uncertainty from the $R_{\textrm{trk}}$ method is a dominant source of the JMS uncertainty.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
%\subfigure[$R_{textrm{trk}}^{m}$]{\includegraphics[width=0.51\textwidth]{Systematics/figure/large_Rtrk_mass}}
%\subfigure[Jet mass scale uncertainty \label{fig:sys:large_jms_Rtrk_sys}]{\includegraphics[width=0.51\textwidth]{Systematics/figure/large_Rtrk_mass_sys}}
\includegraphics[width=0.48\textwidth]{Systematics/figure/large_Rtrk_mass_sys}
\caption[Jet mass scale uncertainty of large-$R$ jets.]{
%(a) Measurement of $R_{textrm{trk}}^{m}$ as a function of the large-R jet \pt with large-$R$ jet $m/\pt = 0.2$.
%Results using three different generator and three differebt tracking variations for the default generator \PYTHIA 8 (shown as a band around these points).
%(b) The total uncertainty in the relative jet mass scale as a function of \pt.
The uncertainty in the relative jet mass scale as a function of the large-$R$ jet \pt with large-$R$ jet $m/\pt = 0.2$.
The baseline uncertainty represents the deviation of the double ratio from unity for \PYTHIA 8.
The modeling uncertainty represents the largest deviation from the unity of alternative MC generators.
The tracking uncertainty represents the quadratic sum of the effect of three tracking variations~\cite{JETM-2018-02}.
}
\label{fig:sys:large_jms_Rtrk_sys}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


%Another method called as forward folding is to measure jet mass response determined by fits to the $W$ boson and top quark mass peaks in the large-$R$ jet invariant mass distribution using the hadronically decaying top quark candidate.
%In this study, we extract the ratio of large-$R$ jet mass scale and resolution from the distribution from the jet mass spectrum using the following function,
%\begin{equation}
%m^{\textrm{fold}} = s~m^{\textrm{reco}} + \left(m^{\textrm{reco}}-m^{\textrm{truth}}R_{m}(m^{\textrm{truth}},\pt^{\textrm{truth}}\right)(r-s)
%\end{equation}
%where $m^{\textrm{reco}}$ is the detector-level large-$R$ jet mass and $R_{m}$ is the large-R jet mass response determined by the simulation.
%We define two regions for $W$ and top mass measurement with different $b$-tagged jets selection in \ttbar events with $\mu+$jets final states.
%Figure~\ref{fig:sys:large_ff_dis} shows the mass distributions of calorimeter jet and track-assisted jet in $W$-enriched region.
%The dashed uncertainty band includes signal and detector modelling uncertainties.
%The final results are shwon in Figure~\ref{fig:sys:large_ff_summary}.
%Both calorimeter and track-assisted mass is good agreement between data and MC.
%The final results are shown in Figure~\ref{fig:reco:large_ff_summary}.
%The dominant sources of uncertainty is derived from the modelling of top quark pair production.
%They are estimated as the difference between different generators (\POWHEG + \HERWIGV{7} and \SHERPA) or different variations of the generator settings such as initial- and final-state radiation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%\begin{figure}[htb!p]
%\centering
%\subfigure[$R_{textrm{trk}}^{m}$]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_ff_W_calomass}}
%\subfigure[Jet mass scale uncertainty]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_ff_W_trkmass}}
%\caption{
%The mass distributions of large-$R$ jet in boosted $W$ bosons enriched region.
%The distributions of the calorimeter mass (a) and track-assisted jet mass (b) are shown~\cite{JETM-2018-02}.
%}
%\label{fig:sys:large_ff_dis}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%\begin{figure}[htb!p]
%\centering
%\includegraphics[width=0.48\textwidth]{Systematics/figure/large_ff_summary}
%\caption{
%Jet mass scale and resolution of calorimeter mass (closed circles) and track-assisted jet mass (open circules) as a function of large-$R$ jet \pt.
%The first two \pt bins are determined by $W$-enriched regions and the other by top-enriched regions~\cite{JETM-2018-02}.
%}
%\label{fig:sys:large_ff_summary}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%\begin{figure}[htb!p]
%\centering
%\subfigure[$50$~GeV $< m < 120$~GeV]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_jms_W_combine}}
%\subfigure[$1200$~GeV $< m < 300$~GeV]{\includegraphics[width=0.48\textwidth]{Systematics/figure/large_jms_top_combine}}
%\caption{
%Data-to-MC ratio of the average jet mass response as a function of the large-$R$ jet \pt in the W mass range (a) and top mass range (b).
%The gray points represents the result of $R_{\textrm{trk}}$ method.
%The balck points represents the result of forward folding method.
%Total (statistical) uncertainy is shown in green (blue) bands.
%}
%\label{fig:sys:large_jms_combine}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


The JMS/JMR uncertainties for $Z/h\ra bb$ tagging need to be estimated, and the differences between the data and MC samples are corrected.
For the irreducible backgrounds, they are evaluated on the relative yields, such as the ratio of the yields with the JMS/JMR systematic variations to the nominal MC samples.
The uncertainties for the reducible backgrounds are evaluated as systematic uncertainties on the transfer factor, such as the double ratio in the CRs and SRs with the JMS/JMR systematic variations to the nominal.
For the SUSY signals, the JMS/JMR systematic uncertainties on the acceptance of $m(J_{bb})$ selections are estimated.
The relative yields of each signal process in each SR are calculated with systematic uncertainties.
There is no significant dependency on the signal mass.
Thus, all signal samples are summed up for the estimation.
The relative yields of the JMS uncertainty are shown in Figure~\ref{fig:sys:sig_jms}.
For the JMR uncertainty, the relative yields are shown in Figure~\ref{fig:sys:sig_jmr}.
There is no problem while these uncertainties are large in not proper SRs for the SUSY signals due to the MC statistics.
The systematic uncertainties of the $Z/h\ra bb$ tagging are fully correlated with them on the normalization factors of kinematic extrapolations.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[\chinoonepm\ninotwo\ra $WZ$\ninoone\ninoone signal]{\includegraphics[width=0.48\textwidth]{Systematics/figure/C1N2_WZ_JMS}}
\subfigure[\chinoonepm\ninotwo\ra $Wh$\ninoone\ninoone signal]{\includegraphics[width=0.48\textwidth]{Systematics/figure/C1N2_Wh_JMS}}
\subfigure[\ninotwo\ninothree\ra $ZZ$\ninoone\ninoone signal]{\includegraphics[width=0.48\textwidth]{Systematics/figure/N2N3_ZZ_JMS}}
\subfigure[\ninotwo\ninothree\ra $Zh$\ninoone\ninoone signal]{\includegraphics[width=0.48\textwidth]{Systematics/figure/N2N3_Zh_JMS}}
\subfigure[\ninotwo\ninothree\ra $hh$\ninoone\ninoone signal]{\includegraphics[width=0.48\textwidth]{Systematics/figure/N2N3_hh_JMS}}
\caption[JMS uncertainty of $J_{bb}$ for SUSY signals.]{
The relative acceptance change in each signal and control region of the 0L-2B2Q category.
The differences are due to the JMS variations in Figure~\ref{fig:sys:large_jms_Rtrk_sys}.
All the statistics from each signal combined as no statistically significant mass dependence on the acceptance is found.
``Track Reco. Eff'' represents the track reconstruction efficiency and related uncertainty.
%Tracking1 represents the track reconstruction efficiency and related uncertainty.
%Tracking2 represents the tracking fake rate uncertainties, and Tracking3 represents the tracking $q/\pt$ bias uncertainties.
}
\label{fig:sys:sig_jms}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\begin{center}
\includegraphics[width=0.48\textwidth]{Systematics/figure/Signal_JMR}
\caption[JMR uncertainty of $J_{bb}$ for SUSY signals.]{The relative acceptance change in each signal and control region of the 0L-2B2Q category.
The differences are due to the JMR variation.
}
\label{fig:sys:sig_jmr}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


