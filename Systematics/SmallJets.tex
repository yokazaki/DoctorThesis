In this thesis, small-$R$ jets are used to calculate \met and \mindphimet.
The uncertainties on the jet energy scale (JES) and the jet energy resolution (JER) are considered.
Additional uncertainty from the pile-up rejection (Jet Vertex Tagger) is also taken into account.

\paragraph*{Jet Energy Scale (JES)}
The energy of small-$R$ jets is corrected in the multi-steps as explained in Section~\ref{sec:reco:small_calib}.
In-situ techniques measure the final jet energy correction, and the uncertainties are derived.
The in-situ techniques for the jet energy correction are described in Ref~\cite{JETM-2018-05}, the outline is introduced.
As described in Section~\ref{sec:reco:small_calib}, three control samples are used depending on the jet \pt to be calibrated, and the \pt balance of each jet relative to a reference object is measured using the data and MC samples.
\Zjets samples are used for the energy calibration of the jets with $17~\GeV < \pt^{j} < 1000~\GeV$ and $|\eta| < 0.8$.
In \Zjets events, the reference object is a $Z$ boson reconstructed by the opposite sign lepton pair.
$\gamma+$jets sample is used to calibrate jets, which have $25~\GeV < \pt^{j} < 1200~\GeV$ and $|\eta| < 0.8$, and photon is the reference object.
Additionally, multi-jets events are used to calibrate high \pT jets to extend the \pt range $\sim 2.4$~TeV.
In the multi-jets events, calibrated jets with the in-situ method using $Z/\gamma+$ jets are used as reference objects.
Since these methods are used to calibrate the central jets ($\arrowvert\eta\arrowvert < 0.8$), we need additional study for applying the calibration to the forward jets ($0.8 < \arrowvert\eta\arrowvert < 4.5$).
We use di-jets topology (use only two jets, unlike multi-jets topology) and measure the ratio of the two jets using data and MC samples.
Systematic uncertainties are evaluated as differences between data and MC samples after applying the correction of $\eta$ inter-calibration.
The results of the two methods are shown in Figure~\ref{fig:sys:smalljets_ratio}.
In the central region, the scale factors for jet \pt in data are about 3\%.
As shown in Figure~\ref{fig:sys:smalljets_ratio_eta_intercalib}, some spikes caused by the detector transition regions are seen in the forward region.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[In-situ method for the central jets]{\includegraphics[width=0.48\textwidth, height=0.24\textheight]{Systematics/figure/small_insitu}}
\subfigure[$\eta$ intercalibration \label{fig:sys:smalljets_ratio_eta_intercalib}]{\includegraphics[width=0.48\textwidth, height=0.24\textheight]{Systematics/figure/small_eta_intercalib}}
\caption[Systematic uncertainties of the small-$R$ jet energy scale in the in-situ techiniques and $\eta$ inter calibration.]{
(a) Ratio of small-$R$ jets response of data to that in MC samples as a function of the jet \pt in the measurement using \Zjets, $\gamma+$jets, and multi-jets events.
The response is defined as $R = \Braket{\pt^{j}/\pt^{\textrm{reference}}}$, where the bracket represents the average.
The black curve shows the corrected value, and total (statistical) uncertainty is shown in the blue (red) area.
The statistical uncertainty is too small to be visible in most regions.
(b) Relative response pf jets ($40~\GeV < \pt < 60~\GeV$) in data (black circles) and MC samples (red squares) as a function of $\eta$ in the detector.
This figure shows the result of only 2017.
The lower panel shows the ratio of the simulation to data.
The dashed lines provide reference points for the viewer~\cite{JETM-2018-05}.
}
\label{fig:sys:smalljets_ratio}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


The uncertainties in the jet energy scale calibration are account for:
\begin{itemize}
\item Uncertainties from the modeling of the physics process
\item Uncertainties of the reference objects
\item Uncertainties from the measurement, such as pile-up effects, flavor dependence, selection cuts, and topology dependence
\end{itemize}
They are summarized in Table~\ref{tab:sys:small_sys_comp} and the uncertainties are shown in Figure~\ref{fig:sys:small_sys}.
The spikes at $|\eta| \sim 2.5$ are caused by the uncertainty of the non-closure in the $\eta$ inter-calibration.
This uncertainty is derived from the modeling of the LAr pulse shape.
We resolve the correlations between them and redefine them as uncorrelated components.
These uncertainties are taken into account independently and used in statistical analysis.

\input{Systematics/tables/samll_sys_comp.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\subfigure[$\pt^{\textrm{jet}}$ at $\eta = 0$]{\includegraphics[width=0.48\textwidth]{Systematics/figure/small_sys_pt}}
\subfigure[$\eta$ at $\pt^{\textrm{jet}} = 60 $~GeV ]{\includegraphics[width=0.48\textwidth]{Systematics/figure/small_sys_eta}}
\caption[Total systematic uncertainties of the small-$R$ jet energy scale.]{
Total uncertainty is shown as a filled region with a function of jet \pt (a) and $\eta$ (b).
Each component is summarized in Table~\ref{tab:sys:small_sys_comp}.
Flavor uncertainties are assumed to be a dijet flavor composition.
``Relative \textit{in situ} JES'' are uncertainties from $\eta$ inter-calibration, and ``Absolute \textit{in situ} JES'' includes all uncertainties of in-situ measurement in the central region except for pile-up, flavor, punch-through.
Total uncertainties are determined as the quadruple sum of each component~\cite{JETM-2018-05}.
}
\label{fig:sys:small_sys}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\paragraph*{Jet Energy Resolution (JER)}
As described in Section~\ref{sec:reco:small_calib}, the uncertainties on the jet energy resolution are evaluated as the difference of \pt scalar balance between data and MC samples using di-jet control samples.
In the measurement, we consider systematic uncertainties of the jet energy scale, physics modeling, event selections, pile-up rejection, and non-closure uncertainty evaluated as the difference between the resolutions of the data and MC samples.
Additionally, in order to consider the contribution of electric noise, we measure the fluctuations in the energy deposits by pile-up effects using data samples collected by random unbiased triggers.
It is called the random cone method.
The difference between the energy deposits in the calorimeter summed inside $R=0.4$ circles are measured, and the estimated pile-up noise is determined by the central 68\% confidence interval of the distribution.
The uncertainties are shown in Figure~\ref{fig:sys:small_jer_sys}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\begin{figure}[t]
\centering
\includegraphics[width=0.48\textwidth]{Systematics/figure/small_jer_sys}
\caption[Total systematic uncertainties of the small-$R$ jet energy reso;ution.]{
Absolute uncertainty on the relative jet energy resolution as a function of jet \pt.
Uncertainties from the two in situ measurements using dijet events and random triggered events for the noise measurement~\cite{JETM-2018-05}.
}
\label{fig:sys:small_jer_sys}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\paragraph*{Jet Vertex Tagger (JVT)}
As described in Section~\ref{sec:reco:small}, Jet Vertex Tagger (JVT) is used to suppress pile-up effects.
The performance is measured using \Zmm+jets control samples, and the measurement method is described in Ref.~\cite{PERF-2014-03}.
The difference between the data and the MC samples is assigned as a simulation-to-data scale factor.
A systematic uncertainty is evaluated as the difference between two JVT working points.

